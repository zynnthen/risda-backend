package com.map2u.ipower.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FormUtil {

    private static final String YEAR_FORMAT = "yy";

    public static String generateNoSiri(String prefix, String ptcode, long currentRunningNumber) {
        DateFormat df = new SimpleDateFormat(YEAR_FORMAT); // Just the year, with 2 digits
        String year = df.format(Calendar.getInstance().getTime());

        return prefix + ptcode.substring(0, 4) + year + String.format("%04d", currentRunningNumber);
    }
}
