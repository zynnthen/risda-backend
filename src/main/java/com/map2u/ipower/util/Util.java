package com.map2u.ipower.util;

import com.map2u.ipower.config.dozer.BeanConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
public class Util {

    @Autowired
    static BeanConverter beanConverter;

    public static LocalDateTime getCurrentTime() {
        return LocalDateTime.now();
    }

    public static <T, U> List<U> convertList(Set<T> from, Function<T, U> func) {
        return from.stream().map(func).collect(Collectors.toList());
    }


    public static Long convertTimeToMillisecond(Date time) {
        if (time != null) {

            try {
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date reference = dateFormat.parse(time.toString());

                Calendar c = Calendar.getInstance();
                c.setTime(reference);
                c.set(Calendar.MILLISECOND, 0);
                Date newDate = c.getTime();

                return newDate.getTime();
            } catch (Exception e) {
                log.error("Failed to convert time to millisecond, {}", e.getMessage());
            }
        }

        return null;
    }

    public static Long convertTimeToMillisecond(String time) {
        if (time != null) {

            try {
                DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date reference = dateFormat.parse(time);
//            long seconds = time.getTime() - reference.getTime();
                return reference.getTime();
            } catch (Exception e) {
                log.error("Failed to convert time to mili seconds, {}", e.getMessage());
            }
        }

        return null;
    }

    public static Long convertDateTimeToMiliSeconds(LocalDateTime dateTime) {
        if (dateTime == null) {
            return null;
        }

        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    public static LocalDateTime convertMiliSecondsToDateTime(Long miliseconds) {
        if (miliseconds != null) {
            return LocalDateTime.ofInstant(Instant.ofEpochMilli(miliseconds), ZoneId.systemDefault());
        }
        return null;
    }

    public static long getMiliseconds(LocalDateTime time) {
        if (time != null) {
            return time.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        }

        return new Date().getTime();
    }

    public static long getSeconds(LocalDateTime time) {
        return Math.round(getMiliseconds(time) / 1000);
    }

}
