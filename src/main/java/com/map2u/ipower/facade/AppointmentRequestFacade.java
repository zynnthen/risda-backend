package com.map2u.ipower.facade;

import com.map2u.ipower.config.DocumentTypeID;
import com.map2u.ipower.config.SmartmapConfig;
import com.map2u.ipower.config.dozer.BeanConverter;
import com.map2u.ipower.exception.BadRequestException;
import com.map2u.ipower.exception.SourceNotFoundException;
import com.map2u.ipower.ipowerdb.entity.*;
import com.map2u.ipower.ipowerdb.repository.*;
import com.map2u.ipower.ipowerdb.specification.AppointmentSpecification;
import com.map2u.ipower.ipowerdb.specification.TS18ASpecification;
import com.map2u.ipower.ipowerdb.specification.TS18Specification;
import com.map2u.ipower.model.FarmBudgetModel;
import com.map2u.ipower.model.appointment.*;
import com.map2u.ipower.model.form.*;
import com.map2u.ipower.service.AuthService;
import com.map2u.ipower.service.FileStorageService;
import com.map2u.ipower.service.FormFieldsService;
import com.map2u.ipower.util.FormUtil;
import com.map2u.ipower.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AppointmentRequestFacade {

    @Autowired
    AppointmentRepository appointmentRepository;

    @Autowired
    LawatBayar18ARepository lawatBayar18ARepository;

    @Autowired
    LawatKebun18Repository lawatKebun18Repository;

    @Autowired
    LawatBayar18EMastRepository lawatBayar18EMastRepository;

    @Autowired
    LawatBayar18AAktivitiRepository lawatBayar18AAktivitiRepository;

    @Autowired
    DocumentTypeRepository documentTypeRepository;

    @Autowired
    TSApplicationRepository tsApplicationRepository;

    @Autowired
    LotOwnerRepository lotOwnerRepository;

    @Autowired
    DocumentSequenceRepository documentSequenceRepository;

    @Autowired
    LawatKebunDocumentRepository lawatKebunDocumentRepository;

    @Autowired
    KumpulanTSRepository kumpulanTSRepository;

    @Autowired
    FarmBudgetMastRepository farmBudgetMastRepository;

    @Autowired
    AppointmentParticipantRepository appointmentParticipantRepository;

    @Autowired
    AuthService authService;

    @Autowired
    DocumentTypeID documentTypeID;

    @Autowired
    SmartmapConfig smartMapConfig;

    @Autowired
    FileStorageService fileStorageService;

    @Autowired
    StokItemRepository stokItemRepository;

    @Autowired
    KodKerjaRepository kodKerjaRepository;

    @Autowired
    TSLejerMastRepository tsLejerMastRepository;

    @Autowired
    BeanConverter beanConverter;

    @Autowired
    FormFieldsService formFieldsService;

    public AppointmentModel getAppointmentByNoSiri(String noSiriTemujanji) {
        AppointmentModel appointment = appointmentRepository.findById(noSiriTemujanji)
                .map(t -> convertAppointmentEntityToModel(t))
                .orElseThrow(() -> new BadRequestException("Invalid no siri temujanji"));

        return appointment;
    }

    public List<AppointmentModel> listLatestAppointmentsByDays(AppointmentParam param, int numberOfDays) {

        List<Sort.Order> orders = new ArrayList<>();

        if (param.getTkhTJanji() != null) {
            Sort.Order order = new Sort.Order(param.getTkhTJanji(), "tkhTJanji");
            orders.add(order);
        }

        Sort sorts = Sort.by(orders);

        ZonedDateTime startDateTime = ZonedDateTime.now();
        ZonedDateTime endDateTime = startDateTime.truncatedTo(ChronoUnit.DAYS)
                .plusDays(numberOfDays)
                .withHour(23)
                .withMinute(59)
                .withSecond(59);

        param.setEnd(endDateTime.toEpochSecond());
        param.setStart(startDateTime.toEpochSecond());

        AppointmentSpecification specification = new AppointmentSpecification(param);

        List<AppointmentModel> appointments = appointmentRepository.findAll(specification, sorts)
                .stream()
                .map(a -> convertAppointmentEntityToModel(a))
                .collect(Collectors.toList());
        return appointments;
    }

    public List<AppointmentModel> listAppointment(int offset, int limit, AppointmentParam param) {
//    public List<Appointment> listAppointment(int offset, int limit, AppointmentParam param) {

        List<Sort.Order> orders = new ArrayList<>();

        if (param.getTkhTJanji() != null) {
            Sort.Order order = new Sort.Order(param.getTkhTJanji(), "tkhTJanji");
            orders.add(order);
        }

        PageRequest pageRequest = PageRequest.of(offset, limit, Sort.by(orders));

        AppointmentSpecification specification = new AppointmentSpecification(param);

        List<AppointmentModel> appointments = appointmentRepository.findAll(specification, pageRequest)
                .stream()
                .map(a -> convertAppointmentEntityToModel(a))
                .collect(Collectors.toList());
        return appointments;
    }

    public List<TSForm> listAllFormsPendingValidation(String ptCode, Long start, Long end, String noKpPpk, String tsMohonId, String jenisLk) {

        List<TSForm> forms = new ArrayList<>();

        boolean loadTS18A = true;
        boolean loadTS18 = true;

        if (jenisLk != null) {
            if (jenisLk.equalsIgnoreCase("TS18")) {
                loadTS18A = false;
            }

            if (jenisLk.equalsIgnoreCase("TS18A")) {
                loadTS18 = false;
            }
        }


        LocalDateTime startDate = null;
        LocalDateTime endDate = null;

        if (start != null) {
            startDate = Util.convertMiliSecondsToDateTime(start);
        }

        if (end != null) {
            endDate = Util.convertMiliSecondsToDateTime(end);
        }

        if (loadTS18A) {
            List<LawatBayar18AModel> ts18A = lawatBayar18ARepository.findFormsPendingValidation(ptCode, startDate, endDate, noKpPpk, tsMohonId)
                    .stream().map(f -> convertLawatBayar18AEntityToModel(f, true, false)).collect(Collectors.toList());
            forms.addAll(ts18A);
        }


        if (loadTS18) {
            List<LawatKebun18Model> ts18 = lawatKebun18Repository.findFormsPendingValidation(ptCode, startDate, endDate, noKpPpk, tsMohonId)
                    .stream().map(f -> convertLawatKebun18EntityToModel(f, true, false)).collect(Collectors.toList());
            forms.addAll(ts18);
        }

        forms.sort((o1, o2) -> Long.valueOf(o1.getTkhLawat()).compareTo(o2.getTkhLawat()));

        return forms;
    }

    public TSFormContainer listFormsPendingValidation(String ptCode, int offset, int limit) {

        Page<String> pages = lawatBayar18EMastRepository.findFormsIdPendingValidation(ptCode, PageRequest.of(offset, limit));

        List<String> formIds = pages.getContent();

//        log.info("Total elements:{}, Page:{}, Form IDS :{}", pages.getTotalElements(), pages.getTotalPages(), formIds);
        List<TSForm> forms = new ArrayList<>();
        List<LawatBayar18AModel> ts18A = lawatBayar18ARepository.findByNoSiriLBIn(formIds)
                .stream().map(f -> convertLawatBayar18AEntityToModel(f, true, false)).collect(Collectors.toList());

        List<LawatKebun18Model> ts18 = lawatKebun18Repository.findByNoSiriLKIn(formIds)
                .stream().map(f -> convertLawatKebun18EntityToModel(f, true, false)).collect(Collectors.toList());

        forms.addAll(ts18A);
        forms.addAll(ts18);

        forms.sort(new Comparator<TSForm>() {
            @Override
            public int compare(TSForm o1, TSForm o2) {
                return Long.valueOf(o1.getTkhLawat()).compareTo(o2.getTkhLawat());
            }
        });

        TSFormContainer container = new TSFormContainer();
        container.setCount(pages.getTotalElements());
        container.setForms(forms);

//
//        List<LawatBayar18AModel> ts18A = lawatBayar18ARepository.findFormsPendingValidation(ptCode)
//                .stream().map(f -> f.convertEntityToModel()).collect(Collectors.toList());
//
//        List<LawatKebun18Model> ts18 = lawatKebun18Repository.findFormsPendingValidation(ptCode)
//                .stream().map(f -> f.convertEntityToModel()).collect(Collectors.toList());
//
//        forms.addAll(ts18A);
//        forms.addAll(ts18);
//
//        forms.sort(new Comparator<TSForm>() {
//            @Override
//            public int compare(TSForm o1, TSForm o2) {
//                if (o1.getTkhLawat() != null && o2.getTkhLawat() != null) {
//                    return Long.valueOf(o1.getTkhLawat()).compareTo(o2.getTkhLawat());
//                }
//
//                if (o1.getTkhLawat() == null) {
//                    return -1;
//                }
//
//                if (o2.getTkhLawat() == null) {
//                    return 1;
//                }
//
//                return 0;
//            }
//        });
//
        return container;
    }

    public TSFormContainer listFormsForValidation(boolean isCompleted, String ptCode, Long start, Long end, String noKpPpk, String tsMohonId, String jenisLk, int offset, int limit) {
        Page<String> pages;


        LocalDateTime startDate = null;
        LocalDateTime endDate = null;

        if (start != null) {
            startDate = Util.convertMiliSecondsToDateTime(start);
        }

        if (end != null) {
            endDate = Util.convertMiliSecondsToDateTime(end);
        }

        if (isCompleted) {
            pages = lawatBayar18EMastRepository.findFormsIdCompleteValidation(ptCode, startDate, endDate, noKpPpk, tsMohonId, jenisLk, PageRequest.of(offset, limit));
        } else {
            pages = lawatBayar18EMastRepository.findFormsIdPendingValidation(ptCode, PageRequest.of(offset, limit));
        }


        List<String> formIds = pages.getContent();
        List<TSForm> forms = new ArrayList<>();

        List<LawatBayar18AModel> ts18A = lawatBayar18ARepository.findByNoSiriLBIn(formIds)
                .stream().map(f -> convertLawatBayar18AEntityToModel(f, true, true)).collect(Collectors.toList());

        List<LawatKebun18Model> ts18 = lawatKebun18Repository.findByNoSiriLKIn(formIds)
                .stream().map(f -> convertLawatKebun18EntityToModel(f, true, true)).collect(Collectors.toList());

        forms.addAll(ts18A);
        forms.addAll(ts18);

        forms.sort(new Comparator<TSForm>() {
            @Override
            public int compare(TSForm o1, TSForm o2) {
                return Long.valueOf(o1.getTkhLawat()).compareTo(o2.getTkhLawat());
            }
        });
        TSFormContainer container = new TSFormContainer();
        container.setCount(pages.getTotalElements());
        container.setForms(forms);

        return container;
    }

    public List<LawatBayar18AModel> findTS18AForms(LawatBayar18AParam param) {

        log.info("Param : {}", param);
        TS18ASpecification specification = new TS18ASpecification(param);

        List<LawatBayar18A> lawatBayar18AList = lawatBayar18ARepository.findAll(specification);

        List<LawatBayar18AModel> lawatBayar18AModelList = lawatBayar18AList
                .stream()
                .map(l -> convertLawatBayar18AEntityToModel(l, true, true))
                .collect(Collectors.toList());

//        LawatBayar18AModel lawatBayar18AModel = lawatBayar18AOpt.get().convertEntityToModel();

        return lawatBayar18AModelList;
    }

    public List<LawatKebun18Model> findTS18(LawatKebun18Param param) {
        log.info("Param : {}", param);
        TS18Specification specification = new TS18Specification(param);


        List<LawatKebun18Model> lawatKebun18List = lawatKebun18Repository.findAll(specification)
                .stream().map(ts18 -> convertLawatKebun18EntityToModel(ts18, true, true))
                .collect(Collectors.toList());

        return lawatKebun18List;
    }

    public LawatBayar18EModel findTS18E(String noSiriLE) {
        Optional<LawatBayar18EMast> lawatBayar18EMastOpt = lawatBayar18EMastRepository.findById(noSiriLE);

        lawatBayar18EMastOpt.orElseThrow(() -> new SourceNotFoundException(String.format("TS18E not found by ID %s.", noSiriLE)));

        LawatBayar18EModel lawatBayar18EModel = lawatBayar18EMastOpt.get().convertEntityToModel();

        List<LawatKebunDocumentModel> documents = lawatKebunDocumentRepository.findByNoSiri(lawatBayar18EModel.getNoSiriLE())
                .stream().map(d -> d.convertEntityToModel()).collect(Collectors.toList());

        lawatBayar18EModel.setDocuments(documents);
        return lawatBayar18EModel;
    }

    public synchronized String addNewTS18AForm(LawatBayar18AModel model) {
        if (model.getJenisLK().isEmpty() || model.getJenisLK().equalsIgnoreCase("kebun")) {
            throw new BadRequestException("Invalid Jenis LK");
        }

        if (model.getKumpulanId() != null) {
            boolean isKumpulanExists = kumpulanTSRepository.existsById(model.getKumpulanId());
            if (!isKumpulanExists)
                throw new BadRequestException(String.format("Invalid kumpulan ID %s.", model.getKumpulanId()));

//            Check peserta exists in appointment's participant
            boolean isPesertaExists = appointmentParticipantRepository.existsByNoSiriTemujanjiAndTsMohonIDAndKumpulanID(model.getNoSiriTemujanji(), model.getTsMohonId(), model.getKumpulanId());

            if (!isPesertaExists) {
                throw new BadRequestException(String.format("Peserta %s not found in temujanji %s and kumpulan %s", model.getTsMohonId(), model.getNoSiriTemujanji(), model.getKumpulanId()));
            }
        } else {
//            Check individual appointment exists
            boolean isAppointmentExists = appointmentRepository.existsByNoSiriTemujanjiAndTsMohonId(model.getNoSiriTemujanji(), model.getTsMohonId());
            if (!isAppointmentExists) {
                throw new BadRequestException(String.format("Invalid temujanji id %s or ts mohon id %s", model.getNoSiriTemujanji(), model.getTsMohonId()));
            }
        }


        boolean isFormExists = lawatBayar18ARepository.existsByNoSiriTemujanjiAndTsMohonId(model.getNoSiriTemujanji(), model.getTsMohonId());

        if (isFormExists) {
            throw new BadRequestException(String.format("Appt. %s, TS mohon ID %s (TS18A) already saved.", model.getNoSiriTemujanji(), model.getTsMohonId()));
        }

        long docTypeId = documentTypeID.getLB();

        String formId = getFormId(docTypeId, model.getPtCode());
        log.info("LB Form ID {}", formId);
        model.setNoSiriLB(formId);
        model.setJenisLK("TS18A");

        LawatBayar18A lawatBayar18A = model.convertModelToEntity();
        lawatBayar18A.setSmartmapLink(smartMapConfig.getLink().replace("{noSiri}", formId));
        lawatBayar18A.setJenisLK("TS18A");

        Integer bilLawatan = lawatBayar18ARepository.findBilLawatanByTsMohonId(model.getTsMohonId());
        lawatBayar18A.setBilLawatan(bilLawatan != null ? (bilLawatan + 1) : 1);

        LawatBayar18A inserted = lawatBayar18ARepository.save(lawatBayar18A);

        if (inserted != null) {
            increaseRunningNo(docTypeId, model.getPtCode());
            updatePesertaStatus(
                    inserted.getNoSiriTemujanji(), inserted.getTsMohonId(), inserted.getKumpulanId(), "Y");
            return inserted.getNoSiriLB();
        }

        throw new BadRequestException("Failed to create new form.");
    }

    public synchronized String addNewTS18Form(LawatKebun18Model model) {
        if (model.getJenisLK().isEmpty() || !model.getJenisLK().equalsIgnoreCase("kebun")) {
            throw new BadRequestException("Invalid Jenis LK");
        }

        boolean isAppointmentExists = appointmentRepository.existsByNoSiriTemujanjiAndTsMohonId(model.getNoSiriTemujanji(), model.getTsMohonId());
        if (!isAppointmentExists) {
            throw new BadRequestException(String.format("Invalid temujanji id %s or ts mohon id %s", model.getNoSiriTemujanji(), model.getTsMohonId()));
        }

        boolean isFormExists = lawatKebun18Repository.existsByNoSiriTemujanjiAndTsMohonId(model.getNoSiriTemujanji(), model.getTsMohonId());

        if (isFormExists) {
            throw new BadRequestException(String.format("Lawat Kebun 18 for no siri temujanji %s and ts mohon id %s already exists.", model.getNoSiriTemujanji(), model.getTsMohonId()));
        }

        long docTypeId = documentTypeID.getLK();

        String formId = getFormId(docTypeId, model.getPtCode());

        log.info("LK Form ID {}", formId);
        model.setNoSiriLK(formId);
        model.setJenisLK("TS18");

        LawatKebun18 lawatKebun18 = model.convertModelToEntity();
        lawatKebun18.setSmartmapLink(smartMapConfig.getLink().replace("{noSiri}", formId));
        lawatKebun18.setJenisLK("TS18");

        Integer bilLawatan = lawatKebun18Repository.findBilLawatanByTsMohonId(model.getTsMohonId());
        lawatKebun18.setBilLawatan(bilLawatan != null ? (bilLawatan + 1) : 1);

        LawatKebun18 inserted = lawatKebun18Repository.save(lawatKebun18);
        if (inserted != null) {
            increaseRunningNo(docTypeId, model.getPtCode());
            return inserted.getNoSiriLK();
        }

        throw new BadRequestException("Failed to create new form.");
    }

    public synchronized String addNewTS18EForm(LawatBayar18EModel model) {

        boolean isFormExisted = lawatKebun18Repository.existsById(model.getNoSiriLawatan());

        if (!isFormExisted) {
            isFormExisted = lawatBayar18ARepository.existsById(model.getNoSiriLawatan());
        }

        if (!isFormExisted) {
            throw new BadRequestException("Invalid No Siri Lawatan");
        }

        boolean isTS18ESubmitted = lawatBayar18EMastRepository.existsByNoSiriLawatan(model.getNoSiriLawatan());

        if (isTS18ESubmitted) {
            log.info("TS18E existed");
            throw new BadRequestException(String.format("Form %s already submitted TS18E", model.getNoSiriLawatan()));
        }

        validateTS18EFields(model);

        long docTypeId = documentTypeID.getLE();
        String formId = getFormId(docTypeId, model.getPtCode());

        log.info("LE Form ID {}", formId);

        model.setNoSiriLE(formId);

        LawatBayar18EMast lawatBayar18EMast = model.convertEntityToModel();
        lawatBayar18EMast.setSmartmapLink(smartMapConfig.getLink().replace("{noSiri}", formId));

        LawatBayar18EMast inserted = lawatBayar18EMastRepository.save(lawatBayar18EMast);

        if (inserted != null) {
            increaseRunningNo(docTypeId, model.getPtCode());
            return inserted.getNoSiriLE();
        }

        throw new BadRequestException("Failed to create new form.");
    }

    public synchronized void addNewTS18AAktiviti(LawatBayar18AAktivitiModel model) {
        LawatBayar18AAktiviti lawatBayar18AAktiviti = model.convertModelToEntity();

        lawatBayar18AAktivitiRepository.save(lawatBayar18AAktiviti);
    }

    public synchronized void updateAppointmentStatus(UpdateAppointmentStatusModel model) {
        Optional<Appointment> appointmentOpt = appointmentRepository.findById(model.getNoSiriTemujanji());
        if (!appointmentOpt.isPresent()) {
            throw new SourceNotFoundException(String.format("Appointment not found for ID %s", model.getNoSiriTemujanji()));
        }

        Appointment appointment = appointmentOpt.get();

        appointment.setStatusTJanji(model.getStatus());
        appointmentRepository.save(appointment);
    }

    public synchronized void updateTS18AForm(LawatBayar18AModel model) {
        if (model.getKumpulanId() != null) {
            boolean isKumpulanExists = kumpulanTSRepository.existsById(model.getKumpulanId());
            if (!isKumpulanExists)
                throw new BadRequestException(String.format("Invalid kumpulan ID %s.", model.getKumpulanId()));
        }


        LawatBayar18A entity = model.convertModelToEntity();

        boolean exists = lawatBayar18ARepository.existsById(model.getNoSiriLB());
        if (!exists) {
            throw new SourceNotFoundException(String.format("TS18A not found by ID %", model.getNoSiriLB()));
        }

        lawatBayar18ARepository.save(entity);
    }

    public synchronized void updateTS18Form(LawatKebun18Model model) {
        LawatKebun18 entity = model.convertModelToEntity();
        boolean exists = lawatKebun18Repository.existsById(model.getNoSiriLK());
        if (!exists) {
            throw new SourceNotFoundException(String.format("TS18 not found by ID %", model.getNoSiriLK()));
        }

        lawatKebun18Repository.save(entity);
    }

    public String storeFile(String formId, String docType, MultipartFile file) {

        boolean isFormExists = lawatBayar18ARepository.existsById(formId);

        if (!isFormExists) {
            isFormExists = lawatKebun18Repository.existsById(formId);
        }

        if (!isFormExists) {
            isFormExists = lawatBayar18EMastRepository.existsById(formId);
        }


        if (!isFormExists) {
            throw new BadRequestException("Invalid form ID");
        }

        LawatKebunDocument doc = new LawatKebunDocument();

        doc.setNoSiri(formId);
        doc.setDocName(file.getOriginalFilename());
        doc.setDocType(docType);

        String fileId = fileStorageService.storeFile(file);
        if (fileId != null) {
            doc.setId(fileId);
            lawatKebunDocumentRepository.save(doc);
            return fileId;
        }

        throw new BadRequestException("Failed to store file, please try again");
    }

    public Resource getFile(String fileId) {
        return fileStorageService.getFile(fileId);
    }

    public synchronized void updateFile(String fileId, String docType, MultipartFile file) {
        Optional<LawatKebunDocument> lawatKebunDocumentOpt = lawatKebunDocumentRepository.findById(fileId);

        if (!lawatKebunDocumentOpt.isPresent()) {
            throw new SourceNotFoundException(String.format("File not found for ID %s.", fileId));
        }

        LawatKebunDocument doc = lawatKebunDocumentOpt.get();
        doc.setDocType(docType);
        doc.setDocName(file.getOriginalFilename());

        boolean fileUpdated = fileStorageService.updateFile(fileId, file);

        if (fileUpdated) {
            lawatKebunDocumentRepository.save(doc);
        } else {
            throw new BadRequestException("Failed to update file");
        }

    }

    public synchronized void deleteFile(String fileId) {
        Optional<LawatKebunDocument> lawatKebunDocumentOpt = lawatKebunDocumentRepository.findById(fileId);

        if (!lawatKebunDocumentOpt.isPresent()) {
            throw new SourceNotFoundException(String.format("File not found for ID %s.", fileId));
        }

        boolean fileDeleted = fileStorageService.deleteFile(fileId);

        if (fileDeleted) {
            lawatKebunDocumentRepository.delete(lawatKebunDocumentOpt.get());
        } else {
            throw new BadRequestException("Failed to delete file");
        }
    }

    public LawatKebunDocument getFileDetailsById(String fileId) {
        Optional<LawatKebunDocument> lawatKebunDocumentOpt = lawatKebunDocumentRepository.findById(fileId);
        if (lawatKebunDocumentOpt.isPresent()) {
            return lawatKebunDocumentOpt.get();
        }

        throw new SourceNotFoundException(String.format("File not found for ID %s .", fileId));
    }

    public List<FarmBudgetItem> getFarmBudgetItems(String noSiriFBM, String ptCode, String kumpulanId, String ansuranNo) {

        Optional<FarmBudgetMast> farmBudgetMastOpt;
        if (noSiriFBM != null) {
            farmBudgetMastOpt = farmBudgetMastRepository.findById(noSiriFBM);
//        } else if (kumpulanId != null && ptCode != null) {
        } else {
            farmBudgetMastOpt = farmBudgetMastRepository.findByNoTSBRPKAndPtCode(kumpulanId, ptCode);
        }
//        else {
//            farmBudgetMastOpt = farmBudgetMastRepository.findByNoTSBRPKOrPtCode(kumpulanId, ptCode);
//        }

        if (!farmBudgetMastOpt.isPresent()) {
            return null;
        }

        List<FarmBudgetChild> farmBudgetChild = farmBudgetMastOpt.get().getChild();

        if (ansuranNo != null) {
            farmBudgetChild = farmBudgetChild.stream()
                    .filter(f -> f.getAnsuranNo().equalsIgnoreCase(ansuranNo))
                    .collect(Collectors.toList());
        }


        Set<String> stokItemId = farmBudgetChild.stream()
                .filter(f -> f.getKatKerja().equalsIgnoreCase("Input Pertanian")
                        && f.getStokItem() != null && !f.getStokItem().isEmpty())
                .map(f -> f.getStokItem())
                .collect(Collectors.toSet());

        Set<String> kodKerjaId = farmBudgetChild.stream()
                .filter(f -> f.getKatKerja().equalsIgnoreCase("Kerja Utama")
                        && f.getKerjaStokKod() != null && !f.getKerjaStokKod().isEmpty())
                .map(f -> f.getKerjaStokKod())
                .collect(Collectors.toSet());

        Map<String, StokItem> stokItemMap = stokItemRepository.findByKodStokItemIn(stokItemId)
                .stream().collect(Collectors.toMap(s -> s.getKodStokItem().trim(), s -> s));

        Map<String, KodKerja> kodKerjaMap = kodKerjaRepository.findByKodKerjaIdIn(kodKerjaId)
                .stream().collect(Collectors.toMap(k -> k.getKodKerjaId().trim(), k -> k));

        List<FarmBudgetItem> items = farmBudgetChild.stream().map(f -> {
            FarmBudgetItem item = new FarmBudgetItem();
            KodKerja kodKerja = kodKerjaMap.get(f.getKerjaStokKod() != null ? f.getKerjaStokKod().trim() : null);
            StokItem stokItem = stokItemMap.get(f.getStokItem() != null ? f.getStokItem().trim() : null);

            if (stokItem != null) {
                item.setUkuran(stokItem.getUkuran());
                item.setPerihalStokItem(stokItem.getPerihalStokItem());
                item.setJenisStok(stokItem.getJenisStok());
            }

            if (kodKerja != null) {
                item.setKodKerjaDesc(kodKerja.getKodKerjaDesc());
                item.setKodKerjaId(kodKerja.getKodKerjaId());
            }


            item.setAnsuranNo(f.getAnsuranNo());
            item.setKatKerja(f.getKatKerja());
            item.setPtCode(f.getPtCode());
            item.setStokItem(f.getStokItem());
            item.setKerjaStokKod(f.getKerjaStokKod());

            return item;
        }).collect(Collectors.toList());

        return items;


//                .orElseThrow(() -> new BadRequestException("Invalid noSiriFBM"));
    }

    public FarmBudgetModel getFarmBudget(String noSiriFBM) {
        FarmBudgetModel farmBudget = farmBudgetMastRepository.findById(noSiriFBM)
                .map(FarmBudgetMast::convertEntityToModel)
                .orElseThrow(() -> new BadRequestException("Invalid noSiriFBM"));

        return farmBudget;
    }

    public List<TSLejerModel> getTSLejerByTsMohonId(String tsMohonId) {
        List<TSLejerModel> tsLejerModels = tsLejerMastRepository.findByTsMohonId(tsMohonId)
                .stream().map(t -> t.convertEntityToModel())
                .collect(Collectors.toList());

        return tsLejerModels;
    }

    private LawatBayar18AModel convertLawatBayar18AEntityToModel(LawatBayar18A entity, boolean loadAttachments, boolean loadTS18E) {
        LawatBayar18AModel model = entity.convertEntityToModel();

        if (loadAttachments) {
            List<LawatKebunDocumentModel> lawatKebunDocuments = lawatKebunDocumentRepository
                    .findByNoSiri(model.getNoSiriLB()).stream().map(m -> m.convertEntityToModel())
                    .collect(Collectors.toList());

            model.setDocuments(lawatKebunDocuments);
        }

        if (loadTS18E) {

            LawatBayar18EModel ts18e = lawatBayar18EMastRepository.findByNoSiriLawatan(
                    model.getNoSiriLB()).map(d -> {
                LawatBayar18EModel ts18eModel = d.convertEntityToModel();

                List<LawatKebunDocumentModel> ts18eDocuments = lawatKebunDocumentRepository
                        .findByNoSiri(ts18eModel.getNoSiriLE()).stream().map(m -> m.convertEntityToModel())
                        .collect(Collectors.toList());

                ts18eModel.setDocuments(ts18eDocuments);

                return ts18eModel;
            }).orElse(null);

            model.setTs18e(ts18e);
        }

        return model;
    }

    private LawatKebun18Model convertLawatKebun18EntityToModel(LawatKebun18 entity, boolean loadAttachments, boolean loadTS18E) {
        LawatKebun18Model model = entity.convertEntityToModel();

        if (loadAttachments) {
            List<LawatKebunDocumentModel> lawatKebunDocuments = lawatKebunDocumentRepository.findByNoSiri(model.getNoSiriLK())
                    .stream().map(m -> m.convertEntityToModel())
                    .collect(Collectors.toList());

            model.setDocuments(lawatKebunDocuments);
        }

        if (loadTS18E) {

            LawatBayar18EModel ts18e = lawatBayar18EMastRepository.findByNoSiriLawatan(
                    entity.getNoSiriLK()).map(f -> {
                LawatBayar18EModel ts18eModel = f.convertEntityToModel();

                List<LawatKebunDocumentModel> ts18eDocuments = lawatKebunDocumentRepository
                        .findByNoSiri(ts18eModel.getNoSiriLE()).stream().map(m -> m.convertEntityToModel())
                        .collect(Collectors.toList());

                ts18eModel.setDocuments(ts18eDocuments);

                return ts18eModel;
            }).orElse(null);

            model.setTs18e(ts18e);
        }


        return model;
    }

    private AppointmentModel convertAppointmentEntityToModel(Appointment a) {
        AppointmentModel model = a.convertEntityToModel();

        if (a.getJenisLK().equalsIgnoreCase("Kebun") || a.getJenisLK().equalsIgnoreCase("ts18")) {

            List<LawatKebun18Model> lawatKebun18List = lawatKebun18Repository.findByNoSiriTemujanjiAndTsMohonId(a.getNoSiriTemujanji(), a.getTsMohonId())
                    .stream().map(ts18 -> convertLawatKebun18EntityToModel(ts18, true, true))
                    .collect(Collectors.toList());

            List<TSForm> tsForms = new ArrayList<>();
            tsForms.addAll(lawatKebun18List);
            model.setForms(tsForms);
        } else {
            LawatBayar18AParam lawatBayar18AParam = new LawatBayar18AParam();
            lawatBayar18AParam.setKumpulanId(a.getKumpulanId());
            lawatBayar18AParam.setNoSiriTemujanji(a.getNoSiriTemujanji());
            lawatBayar18AParam.setTsMohonId(a.getTsMohonId());


            TS18ASpecification ts18ASpecification = new TS18ASpecification(lawatBayar18AParam);

            List<LawatBayar18AModel> lawatBayar18AList = lawatBayar18ARepository.findAll(ts18ASpecification)
                    .stream().map(f -> convertLawatBayar18AEntityToModel(f, true, true))
                    .collect(Collectors.toList());

            List<TSForm> tsForms = new ArrayList<>();
            tsForms.addAll(lawatBayar18AList);
            model.setForms(tsForms);
        }

        List<FarmBudgetItem> farmBudgetItems = getFarmBudgetItems(
                null, model.getPtCode(), model.getKumpulanId(), model.getAnsuranNo());

        model.setFarmBudgetItems(farmBudgetItems);
        return model;
    }

    private void increaseRunningNo(long docTypeId, String ptCode) {
        Optional<DocumentSequence> documentSequenceOpt = documentSequenceRepository.findFirstByDocTypeAndPtCodeOrderByCutOffDateDesc(docTypeId, ptCode);

        if (!documentSequenceOpt.isPresent()) {
            throw new SourceNotFoundException(String.format("Document sequence not found for doc type %s and pt code %s.", docTypeId, ptCode));
        }

        DocumentSequence docSequence = documentSequenceOpt.get();
        docSequence.setCurrentRunningNo(docSequence.getCurrentRunningNo() + 1);
        documentSequenceRepository.save(docSequence);
    }

    private String getFormId(long docTypeId, String ptCode) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy");

        Optional<DocumentSequence> documentSequenceOpt = documentSequenceRepository.findByDocTypeAndYearAndPtCode(
                docTypeId,
                format.format(new Date()),
                ptCode);

        if (!documentSequenceOpt.isPresent()) {
            throw new SourceNotFoundException(String.format("Document sequence not found for doc type %s and pt code %s.", docTypeId, ptCode));
        }

        DocumentSequence docSequence = documentSequenceOpt.get();

        String idPrefix = "";

        if (docSequence.getDocumentType() != null) {
            idPrefix = docSequence.getDocumentType().getPrefix().trim();
        }

        String formId = FormUtil.generateNoSiri(idPrefix, ptCode, docSequence.getCurrentRunningNo());

        return formId;
    }

    private void validateTS18EItemsAgainstTS18A(List<LawatBayar18EChildModel> ts18EChilds) {

        List<String> ts18AFields = formFieldsService.getTs18AFields();

        for (LawatBayar18EChildModel item : ts18EChilds) {
            if (!ts18AFields.contains(item.getItem())) {
                throw new BadRequestException(String.format("Invalid item '%s' in Lawat Bayar 18A (TS18A).", item.getItem()));
            }
//            log.info("{} :  {}", item.getItem(), ts18Fields.contains(item.getItem()));
        }

    }

    private void validateTS18EItemsAgainstTS18(List<LawatBayar18EChildModel> ts18EChilds) {
        List<String> ts18Fields = formFieldsService.getTs18Fields();

        for (LawatBayar18EChildModel item : ts18EChilds) {
            if (!ts18Fields.contains(item.getItem())) {
                throw new BadRequestException(String.format("Invalid item '%s' in Lawat Kebun 18 (TS18).", item.getItem()));
            }
//            log.info("{} :  {}", item.getItem(), ts18Fields.contains(item.getItem()));

        }

    }

    // TEST
    public DocumentSequence getDocSequenceTest(String ptCode, int docType) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        Optional<DocumentSequence> documentSequenceOpt = documentSequenceRepository.findByDocTypeAndYearAndPtCode(
                docType,
                format.format(new Date()),
                ptCode);

        documentSequenceOpt.orElseThrow(() -> new SourceNotFoundException(String.format("Document sequence not found for doc type %s and pt code %s.", docType, ptCode)));

        return documentSequenceOpt.get();
    }

    public Integer getTS18ABilLawatanByTsMohonId(String tsMohonId) {
        Integer bil = lawatBayar18ARepository.findBilLawatanByTsMohonId(tsMohonId);
        return bil;
    }

    public Integer getTS18BilLawatanByTsMohonId(String tsMohonId) {
        Integer bil = lawatKebun18Repository.findBilLawatanByTsMohonId(tsMohonId);
        return bil;
    }

    public LawatBayar18AAktivitiModel getLawatBayar18AAktivitiById(long id) {
        LawatBayar18AAktivitiModel model = lawatBayar18AAktivitiRepository.findById(id).map(a -> a.convertEntityToModel())
                .orElseThrow(() -> new BadRequestException("Invalid Id"));

        return model;
    }

    public void updatePesertaStatus(String noSiriTemujanji, String tsMohonId, String kumpulanId, String value) {

        Optional<AppointmentParticipant> participantOpt = appointmentParticipantRepository
                .findByNoSiriTemujanjiAndTsMohonIDAndKumpulanID(noSiriTemujanji, tsMohonId, kumpulanId);

        if (participantOpt.isPresent()) {
            log.info("Update participant status, {} {} {} {}", noSiriTemujanji, tsMohonId, kumpulanId, value);
            AppointmentParticipant participant = participantOpt.get();
            participant.setStatus(value.toUpperCase());
            appointmentParticipantRepository.save(participant);
        } else {
            log.info("No Participant to update status, {} {} {}", noSiriTemujanji, tsMohonId, kumpulanId);
        }

    }

    public void validateTS18EFields(LawatBayar18EModel model) {
        if (model.getJenisLE().equalsIgnoreCase("18") && model.getNoSiriLawatan().startsWith("LK")) {
            validateTS18EItemsAgainstTS18(model.getItems());
        } else if (model.getJenisLE().equalsIgnoreCase("18A") && model.getNoSiriLawatan().startsWith("LB")) {
            validateTS18EItemsAgainstTS18A(model.getItems());
        } else {
            throw new BadRequestException(String.format("Incompatible jenis LE %s and no siri lawatan %s", model.getJenisLE(), model.getNoSiriLawatan()));
        }
    }

    public TSApplicationModel getTSApplication(String tsMohonId) {
        TSApplication tsApplication = tsApplicationRepository.findById(tsMohonId)
                .orElseThrow(() -> new SourceNotFoundException(String.format("TS Application not found by ID %s", tsMohonId)));

        TSApplicationModel model = tsApplication.convertEntityToModel();
        return model;
    }

    public LotOwnerModel getLotOwner(String lotOwnerId) {
        LotOwner lotOwner = lotOwnerRepository.findById(lotOwnerId)
                .orElseThrow(() -> new SourceNotFoundException(String.format("Lot owner not found by ID %s.", lotOwnerId)));

        return lotOwner.convertEntityToModel();
    }
}
