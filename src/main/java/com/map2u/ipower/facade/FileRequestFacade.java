package com.map2u.ipower.facade;

import com.map2u.ipower.exception.SourceNotFoundException;
import com.map2u.ipower.ipowerdb.entity.LawatKebunDocument;
import com.map2u.ipower.ipowerdb.repository.LawatKebunDocumentRepository;
import com.map2u.ipower.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FileRequestFacade {

    @Autowired
    LawatKebunDocumentRepository lawatKebunDocumentRepository;

    @Autowired
    FileStorageService fileStorageService;

    public LawatKebunDocument getFileDetailsById(String fileId) {
        Optional<LawatKebunDocument> lawatKebunDocumentOpt = lawatKebunDocumentRepository.findById(fileId);
        if (lawatKebunDocumentOpt.isPresent()) {
            return lawatKebunDocumentOpt.get();
        }

        throw new SourceNotFoundException(String.format("File not found for ID %s .", fileId));
    }

    public Resource getFile(String fileId) {
        return fileStorageService.getFile(fileId);
    }

}
