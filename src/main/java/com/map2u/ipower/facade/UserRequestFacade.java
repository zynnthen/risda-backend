package com.map2u.ipower.facade;

import com.map2u.ipower.config.dozer.BeanConverter;
import com.map2u.ipower.exception.UnauthorizedException;
import com.map2u.ipower.model.user.*;
import com.map2u.ipower.risdadb.entity.User;
import com.map2u.ipower.risdadb.repository.UserRepository;
import com.map2u.ipower.service.JWTService;
import com.map2u.ipower.service.PasswordService;
import com.nimbusds.jwt.JWTClaimsSet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserRequestFacade {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordService passwordService;

    @Autowired
    JWTService jwtService;

    @Autowired
    BeanConverter beanConverter;

    public AuthenticatedUserModel authenticateUser(AuthenticationModel authenticationModel) throws Exception {
        Optional<User> userOpt = userRepository.findByIdentity(authenticationModel.getUsername());
        if (!userOpt.isPresent()) {
            throw new UnauthorizedException("Invalid username or password");
        }

        User user = userOpt.get();


//        log.info("Found user {}", user.getName());
        boolean isMatched = passwordService.matchHashPassword(authenticationModel.getPassword(), user.getPassword());
//        log.info("Is password matched? {}", isMatch);

        if (!isMatched) {
            log.info("Unauthorized user {} with password {}", authenticationModel.getUsername(), authenticationModel.getPassword());
            throw new UnauthorizedException("Invalid username or password");
        }

        log.info("User {} authenticated, generating Token", authenticationModel.getUsername());

//        UserDetailsModel userDetailsModel = user.convertEntityToModel();
        UserDetailsModel userDetailsModel = beanConverter.convert(user, UserDetailsModel.class);
        userDetailsModel.setPassword(null);
        String idToken = jwtService.generateIdToken(userDetailsModel);
        String refreshToken = jwtService.generateRefreshToken(userDetailsModel);

        TokenModel token = new TokenModel(idToken, refreshToken);

        return new AuthenticatedUserModel(userDetailsModel, token);
    }

    public AuthenticatedUserModel refreshToken(RefreshTokenModel refreshTokenModel) throws Exception {
        JWTClaimsSet claimsSet = jwtService.verifyRefreshToken(refreshTokenModel.getRefreshToken());

        log.info("Refresh token expiry: {}", claimsSet.getExpirationTime());

        UserDetailsModel userDetailsModel = jwtService.getUserDetailsFromJWTClaimSet(claimsSet);

        String idToken = jwtService.generateIdToken(userDetailsModel);

        TokenModel token = new TokenModel(idToken);

        return new AuthenticatedUserModel(userDetailsModel, token);
    }
}
