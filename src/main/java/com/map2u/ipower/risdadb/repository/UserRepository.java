package com.map2u.ipower.risdadb.repository;

import com.map2u.ipower.risdadb.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByIdentity(String identity);
}
