package com.map2u.ipower.risdadb.entity;

import com.map2u.ipower.model.user.UserDetailsModel;
import lombok.Data;

import javax.persistence.*;
import java.time.ZonedDateTime;


@Entity
@Table(name = "tbl_pengguna")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Column(name = "No_KP")
    String identity;

    @Column(name = "No_Staff")
    String staffNo;

    @Column(name = "KataLaluan")
    String password;

    @Column(name = "Nama")
    String name;

    @Column(name = "Email")
    String email;

    @Column(name = "Kategori_Pengguna")
    Integer category;

    @Column(name = "Kod_PT")
    String ptCode;

    @Column(name = "Kod_PT_Baru")
    String ptCodeNew;

    @Column(name = "Gelaran_Jawatan")
    String designation;

    @Column(name = "Status")
    String status;

    @Column(name = "No_Telefon")
    String contact;

    @Column(name = "Tarikh")
    ZonedDateTime date;

    @Column(name = "InsertBy")
    String insertBy;

    @Column(name = "InsertDate")
    ZonedDateTime insertDate;

    @Column(name = "UpdatedBy")
    String updatedBy;

    @Column(name = "UpdatedOn")
    ZonedDateTime updatedOn;

    @Column(name = "Token")
    String token;

    @Column(name = "Exp_Token")
    ZonedDateTime expToken;

    @Column(name = "Attempt_Count")
    Integer attemptCount;

    @Column(name = "Penalty")
    ZonedDateTime penalty;

    @OneToOne
    @JoinColumn(name = "Peranan_Pengguna")
    UserRole userRole;

    public UserDetailsModel convertEntityToModel() {
        UserDetailsModel userDetailsModel = new UserDetailsModel();

        userDetailsModel.setCategory(this.category);
        userDetailsModel.setContact(this.contact);
        userDetailsModel.setDesignation(this.designation);
        userDetailsModel.setEmail(this.getEmail());
        userDetailsModel.setId(this.id);
        userDetailsModel.setIdentity(this.identity);
        userDetailsModel.setName(this.name);
        userDetailsModel.setPtCode(this.ptCode);
        userDetailsModel.setPtCodeNew(this.ptCodeNew);
        userDetailsModel.setStaffNo(this.staffNo);

        return userDetailsModel;
    }
}
