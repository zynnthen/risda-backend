package com.map2u.ipower.risdadb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_peranan")
@Data
public class UserRole {

    @Id
    long id;

    @Column(name = "Peranan")
    String role;

    @Column(name = "Kategori_ID")
    long categoryId;


}
