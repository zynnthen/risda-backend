package com.map2u.ipower.controller;


import com.map2u.ipower.facade.UserRequestFacade;
import com.map2u.ipower.model.response.RestResponse;
import com.map2u.ipower.model.user.AuthenticatedUserModel;
import com.map2u.ipower.model.user.AuthenticationModel;
import com.map2u.ipower.model.user.RefreshTokenModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@Slf4j
@Validated
public class AuthController {

    @Autowired
    UserRequestFacade userRequestFacade;

    @PostMapping("/login")
    public RestResponse<AuthenticatedUserModel> login(@RequestBody AuthenticationModel authenticationModel) throws Exception {

        AuthenticatedUserModel authenticatedUserModel = userRequestFacade.authenticateUser(authenticationModel);

        return new RestResponse<>(RestResponse.STATUS_OK, authenticatedUserModel);
    }

    @PostMapping("/refresh")
    public RestResponse<AuthenticatedUserModel> refreshToken(@Valid @RequestBody RefreshTokenModel refreshTokenModel) throws Exception {
        AuthenticatedUserModel authenticatedUserModel = userRequestFacade.refreshToken(refreshTokenModel);
        return new RestResponse<>(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL, authenticatedUserModel);
    }
}
