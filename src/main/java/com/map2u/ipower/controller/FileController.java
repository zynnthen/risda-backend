package com.map2u.ipower.controller;

import com.map2u.ipower.facade.FileRequestFacade;
import com.map2u.ipower.ipowerdb.entity.LawatKebunDocument;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/file")
@ApiIgnore
public class FileController {

    @Autowired
    FileRequestFacade fileRequestFacade;

    @GetMapping("/view/{id}")
    public ResponseEntity getFile(
            @PathVariable("id") String id,
            HttpServletRequest request
    ) {

        LawatKebunDocument doc = fileRequestFacade.getFileDetailsById(id);

        Resource resource = fileRequestFacade.getFile(id);


        String contentType = request.getServletContext().getMimeType(doc.getDocName());

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "filename=\"" + doc.getDocName() + "\"")
                .body(resource);
    }

    @GetMapping("/download/{id}")
    public ResponseEntity downloadFile(
            @PathVariable("id") String id,
            HttpServletRequest request
    ) {

        LawatKebunDocument doc = fileRequestFacade.getFileDetailsById(id);

        Resource resource = fileRequestFacade.getFile(id);


        String contentType = request.getServletContext().getMimeType(doc.getDocName());

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + doc.getDocName() + "\"")
                .body(resource);
    }
}


