package com.map2u.ipower.controller;

import com.map2u.ipower.facade.AppointmentRequestFacade;
import com.map2u.ipower.ipowerdb.entity.DocumentSequence;
import com.map2u.ipower.model.FarmBudgetModel;
import com.map2u.ipower.model.appointment.LotOwnerModel;
import com.map2u.ipower.model.appointment.TSApplicationModel;
import com.map2u.ipower.model.form.LawatBayar18AAktivitiModel;
import com.map2u.ipower.model.form.LawatBayar18EModel;
import com.map2u.ipower.model.response.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/test")
@Validated
@ConditionalOnExpression("${rest.api.module.test.enable:false}")
public class TestController {

    @Autowired
    AppointmentRequestFacade appointmentRequestFacade;

    @GetMapping("/docSequence/{docTypeNum}/{ptCode}")
//    @PreAuthorize("hasAuthority('ROLE_'+@userRoleConfig.getUserRoleCode('ketuaStesen'))")
    @PreAuthorize("hasAuthority('ROLE_'+getUserRole('ketuaStesen'))")
    public RestResponse<DocumentSequence> getDocumentSequenceTest(
            @PathVariable("docTypeNum") int docTypeNum,
            @PathVariable("ptCode") String ptCode
    ) {
        DocumentSequence docSeq = appointmentRequestFacade.getDocSequenceTest(ptCode, docTypeNum);

        return new RestResponse<>(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL, docSeq);
    }

    @GetMapping("/farmbudget/{noSiriFBM}")
    public RestResponse<FarmBudgetModel> getFarmBudget(
            @PathVariable("noSiriFBM") String noSiriFBM
    ) {
        FarmBudgetModel farmBudget = appointmentRequestFacade.getFarmBudget(noSiriFBM);
        return new RestResponse<>(RestResponse.STATUS_OK, farmBudget);
    }


    @GetMapping("/peserta/updatestatus/{noSiriTemujanji}")
    public RestResponse updatePesertaStatus(
            @PathVariable("noSiriTemujanji") String noSiriTemujanji,
            @RequestParam(value = "tsMohonId", required = false) String tsMohonId,
            @RequestParam(value = "kumpulanId", required = false) String kumpulanId
    ) {

        appointmentRequestFacade.updatePesertaStatus(noSiriTemujanji, tsMohonId, kumpulanId, "Y");
        return new RestResponse(RestResponse.STATUS_OK);
    }

    @PostMapping("/validate/ts18e")
    public RestResponse validateTs18E(
            @RequestBody LawatBayar18EModel ts18e
    ) {
        appointmentRequestFacade.validateTS18EFields(ts18e);
        return new RestResponse(RestResponse.STATUS_OK);
    }

    @PostMapping("/ts18a/aktiviti")
    public RestResponse saveTS18AAktiviti(
            @RequestBody @Valid LawatBayar18AAktivitiModel ts18aAktiviti
    ) {
        appointmentRequestFacade.addNewTS18AAktiviti(ts18aAktiviti);

        return new RestResponse(RestResponse.STATUS_OK);

    }

    @GetMapping("/ts18a/aktiviti/{id}")
    public RestResponse<LawatBayar18AAktivitiModel> getLawatBayar18AAktivitiById(
            @PathVariable("id") long id
    ) {
        LawatBayar18AAktivitiModel model = appointmentRequestFacade.getLawatBayar18AAktivitiById(id);
        return new RestResponse<>(RestResponse.STATUS_OK, model);
    }

    @GetMapping("/tsApplication")
    public RestResponse<TSApplicationModel> getTSApplication(
            @RequestParam("tsMohonId") String tsMohonId
    ) {
        TSApplicationModel model = appointmentRequestFacade.getTSApplication(tsMohonId);

        return new RestResponse<>(RestResponse.STATUS_OK, model);
    }

    @GetMapping("/lotOwner")
    public RestResponse<LotOwnerModel> getLotOwner(
            @RequestParam("lotOwnerId") String lotOwnerId
    ) {
        LotOwnerModel lotOwnerModel = appointmentRequestFacade.getLotOwner(lotOwnerId);

        return new RestResponse<>(RestResponse.STATUS_OK, lotOwnerModel);
    }

    @GetMapping("/ts18a/bilLawatan")
    public RestResponse<Integer> getTS18ABilLawatanByTsMohonId(
            @RequestParam(value = "tsMohonId") String tsMohonId
    ) {
        Integer bil = appointmentRequestFacade.getTS18ABilLawatanByTsMohonId(tsMohonId);

        return new RestResponse<>(RestResponse.STATUS_OK, bil);
    }

    @GetMapping("/ts18/bilLawatan")
    public RestResponse<Integer> getTS18BilLawatanByTsMohonId(
            @RequestParam(value = "tsMohonId") String tsMohonId
    ) {
        Integer bil = appointmentRequestFacade.getTS18BilLawatanByTsMohonId(tsMohonId);

        return new RestResponse<>(RestResponse.STATUS_OK, bil);
    }

}
