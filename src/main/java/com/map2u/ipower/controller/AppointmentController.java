package com.map2u.ipower.controller;

import com.map2u.ipower.facade.AppointmentRequestFacade;
import com.map2u.ipower.ipowerdb.entity.*;
import com.map2u.ipower.model.FarmBudgetModel;
import com.map2u.ipower.model.appointment.*;
import com.map2u.ipower.model.form.*;
import com.map2u.ipower.model.response.RestResponse;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/api/appointment")
@Validated
@PreAuthorize("hasAnyAuthority('ROLE_'+getUserRole('pegawaiPemeriksa'),'ROLE_'+getUserRole('ketuaStesen'))")
public class AppointmentController {

    @Autowired
    AppointmentRequestFacade appointmentRequestFacade;

    @PutMapping("/status/update")
    public RestResponse updateAppointmentStatus(
            @Valid @RequestBody UpdateAppointmentStatusModel updateAppointmentStatusModel
    ) {
        appointmentRequestFacade.updateAppointmentStatus(updateAppointmentStatusModel);
        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL);
    }

    @GetMapping("/{noSiriTemujanji}")
    public RestResponse<AppointmentModel> getAppointmentByNoSiri(@PathVariable("noSiriTemujanji") String noSiriTemujanji) {
        AppointmentModel appointment = appointmentRequestFacade.getAppointmentByNoSiri(noSiriTemujanji);
        return new RestResponse<>(RestResponse.STATUS_OK, appointment);
    }

    @GetMapping("/list/{offset}/{limit}")
    public RestResponse<List<AppointmentModel>> getListOfAppointments(
//    public RestResponse getListOfAppointments(
            @PathVariable(name = "offset") int offset,
            @Valid
            @Min(value = 1)
            @Max(value = 25) @PathVariable(name = "limit") int limit,
            @ApiParam @Valid AppointmentParam appointmentParam
    ) {
        List<AppointmentModel> appointmentList = appointmentRequestFacade.listAppointment(offset, limit, appointmentParam);
//        List<Appointment> appointmentList = appointmentRequestFacade.listAppointment(offset, limit, appointmentParam);
        return new RestResponse(RestResponse.STATUS_OK, appointmentList);
    }

    @GetMapping("/list/latest/days/{days}")
    public RestResponse<List<AppointmentModel>> getLatestAppointmentsByNumberOfDays(
            @Min(value = 0)
            @Max(value = 10)
            @PathVariable(name = "days") int days,
            @ApiParam @Valid AppointmentParam appointmentParam
    ) {
        List<AppointmentModel> appointmentModelList = appointmentRequestFacade.listLatestAppointmentsByDays(appointmentParam, days);
        return new RestResponse<>(RestResponse.STATUS_OK, appointmentModelList);
    }


    @GetMapping("/list/forms/pending/validation")
    @PreAuthorize("hasAuthority('ROLE_'+getUserRole('ketuaStesen'))")
    public RestResponse<List<TSForm>> getAllFormsPendingValidation(
            @RequestParam("ptCode") String ptCode,
            @RequestParam(value = "start", required = false) Long start,
            @RequestParam(value = "end", required = false) Long end,
            @RequestParam(value = "noKpPpk", required = false) String noKpPPK,
            @RequestParam(value = "tsMohonId", required = false) String tsMohonId,
            @RequestParam(value = "jenisLK", required = false) String jenisLK

    ) {
        List<TSForm> forms = appointmentRequestFacade.listAllFormsPendingValidation(ptCode, start, end, noKpPPK, tsMohonId, jenisLK);
//         appointmentRequestFacade.listFormsPendingValidation(ptCode);

        return new RestResponse<>(RestResponse.STATUS_OK, forms);
    }

    @GetMapping("/list/forms/pending/validation/{offset}/{limit}")
    @PreAuthorize("hasAuthority('ROLE_'+getUserRole('ketuaStesen'))")
    public RestResponse<TSFormContainer> getFormsPendingValidation(
            @PathVariable("offset") int offset,
            @Valid
            @Min(value = 1)
            @Max(value = 25) @PathVariable(name = "limit") int limit,
            @RequestParam("ptCode") String ptCode,
            @RequestParam(value = "start", required = false) Long start,
            @RequestParam(value = "end", required = false) Long end,
            @RequestParam(value = "noKpPpk", required = false) String noKpPPK,
            @RequestParam(value = "tsMohonId", required = false) String tsMohonId,
            @RequestParam(value = "jenisLK", required = false) String jenisLK
    ) {
        TSFormContainer container = appointmentRequestFacade.listFormsForValidation(false, ptCode, start, end, noKpPPK, tsMohonId, jenisLK, offset, limit);
//         appointmentRequestFacade.listFormsPendingValidation(ptCode);

        return new RestResponse<>(RestResponse.STATUS_OK, container);
    }

    @GetMapping("/list/forms/complete/validation/{offset}/{limit}")
    @PreAuthorize("hasAuthority('ROLE_'+getUserRole('ketuaStesen'))")
    public RestResponse<TSFormContainer> getFormsCompleteValidation(
            @PathVariable("offset") int offset,
            @Valid
            @Min(value = 1)
            @Max(value = 25) @PathVariable(name = "limit") int limit,
            @RequestParam("ptCode") String ptCode,
            @RequestParam(value = "start", required = false) Long start,
            @RequestParam(value = "end", required = false) Long end,
            @RequestParam(value = "noKpPpk", required = false) String noKpPPK,
            @RequestParam(value = "tsMohonId", required = false) String tsMohonId,
            @RequestParam(value = "jenisLK", required = false) String jenisLK

    ) {
        TSFormContainer container = appointmentRequestFacade.listFormsForValidation(true, ptCode, start, end, noKpPPK, tsMohonId, jenisLK, offset, limit);
        return new RestResponse<>(RestResponse.STATUS_OK, container);
    }

    @GetMapping("/ts18a")
    public RestResponse<List<LawatBayar18AModel>> getTS18AForms(
            @ApiParam @Valid LawatBayar18AParam lawatBayar18AParam
    ) {
        List<LawatBayar18AModel> lawatBayar18AList = appointmentRequestFacade.findTS18AForms(lawatBayar18AParam);

        return new RestResponse<>(RestResponse.STATUS_OK, lawatBayar18AList);
    }

    @GetMapping("/ts18")
    public RestResponse<List<LawatKebun18Model>> getTS18Forms(
            @ApiParam
            @Valid LawatKebun18Param lawatKebun18Param
    ) {

        List<LawatKebun18Model> lawatKebun18Models = appointmentRequestFacade.findTS18(lawatKebun18Param);
        return new RestResponse<>(RestResponse.STATUS_OK, lawatKebun18Models);
    }

    @GetMapping("/ts18e")
    @PreAuthorize("hasAuthority('ROLE_'+getUserRole('ketuaStesen'))")
    public RestResponse<LawatBayar18EModel> getTS18E(
            @RequestParam("noSiriLE") String noSiriLE
    ) {
        LawatBayar18EModel lawatBayar18EModel = appointmentRequestFacade.findTS18E(noSiriLE);
        return new RestResponse<>(RestResponse.STATUS_OK, lawatBayar18EModel);
    }

    @GetMapping("/farmBudget")
    public RestResponse<List<FarmBudgetItem>> getFarmBudget(
            @RequestParam(value = "noSiriFBM", required = false) String noSiriFBM,
            @RequestParam(value = "ptCode", required = false) String ptCode,
            @RequestParam(value = "kumpulanId", required = false) String kumpulanId,
            @RequestParam(value = "ansuranNo", required = false) String ansuranNo
    ) {
        List<FarmBudgetItem> farmBudget = appointmentRequestFacade.getFarmBudgetItems(noSiriFBM, ptCode, kumpulanId, ansuranNo);

        return new RestResponse<>(RestResponse.STATUS_OK, farmBudget);
    }

    @PostMapping("/ts18a/save")
    public RestResponse addTS18AForm(
            @Valid @RequestBody LawatBayar18AModel ts18a
    ) {
        String formId = appointmentRequestFacade.addNewTS18AForm(ts18a);
        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL, formId);
    }

    @PostMapping("/ts18/save")
    public RestResponse addTS18Form(
            @Valid @RequestBody LawatKebun18Model ts18
    ) {
        String formId = appointmentRequestFacade.addNewTS18Form(ts18);
        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL, formId);
    }

    @PostMapping("/ts18e/save")
    @PreAuthorize("hasAuthority('ROLE_'+getUserRole('ketuaStesen'))")
    public RestResponse addTS18EForm(
            @Valid @RequestBody LawatBayar18EModel ts18e
    ) {
        String formId = appointmentRequestFacade.addNewTS18EForm(ts18e);
        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL, formId);
    }

    @PutMapping("/ts18a/update")
    public RestResponse updateTS18AForm(
            @Valid @RequestBody LawatBayar18AModel model
    ) {
        appointmentRequestFacade.updateTS18AForm(model);
        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL);
    }

    @PutMapping("/ts18/update")
    public RestResponse updateTS18Form(
            @Valid @RequestBody LawatKebun18Model model
    ) {
        appointmentRequestFacade.updateTS18Form(model);

        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL);
    }


    @PostMapping("/file/{formId}/{docType}")
    public RestResponse uploadFile(@PathVariable("formId") String formId, @PathVariable("docType") String docType, @RequestPart("file") MultipartFile file) {

        String fileId = appointmentRequestFacade.storeFile(formId, docType, file);
        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL, fileId);
    }

    @GetMapping("/file/{fileId}")
    public ResponseEntity getFile(
            @PathVariable("fileId") String fileId, HttpServletRequest request) {

        LawatKebunDocument doc = appointmentRequestFacade.getFileDetailsById(fileId);

        Resource resource = appointmentRequestFacade.getFile(fileId);


//        String contentType = null;
//        try {
//            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        String contentType = request.getServletContext().getMimeType(doc.getDocName());
//        } catch (IOException ex) {
//            System.out.println("Could not determine file type.");
//        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + doc.getDocName() + "\"")
                .body(resource);
    }

    @GetMapping("/lejer")
    public RestResponse<List<TSLejerModel>> getLejerByTsMohonId(
            @RequestParam("tsMohonId") String tsMohonId
    ) {

        List<TSLejerModel> lejers = appointmentRequestFacade.getTSLejerByTsMohonId(tsMohonId);

        return new RestResponse<>(RestResponse.STATUS_OK, lejers);
    }

    @DeleteMapping("/file/delete/{fileId}")
    public RestResponse deleteFile(
            @PathVariable("fileId") String fileId
    ) {
        appointmentRequestFacade.deleteFile(fileId);

        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL);
    }

    @PutMapping("/file/update/{fileId}/{docType}")
    public RestResponse updateFile(
            @PathVariable("fileId") String fileId,
            @PathVariable("docType") String docType,
            @RequestPart("file") MultipartFile file
    ) {
        appointmentRequestFacade.updateFile(fileId, docType, file);

        return new RestResponse(RestResponse.STATUS_OK, RestResponse.MESSAGE_SUCCESSFUL);
    }


}
