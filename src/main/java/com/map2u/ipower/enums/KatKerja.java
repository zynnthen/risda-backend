package com.map2u.ipower.enums;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.util.HashMap;
import java.util.Map;

public enum KatKerja {


    KERJA_UTAMA("Kerja Utama"),
    INPUT_PERTANIAN("Input Pertanian");

    private String value;

    KatKerja(String value) {
        this.value = value;
        KatKerjaMap.map.put(value, this);
    }

    private static class KatKerjaMap {
        private static Map<String, KatKerja> map = new HashMap<>();
    }


    @JsonCreator
    public static KatKerja findByValue(String value) {
        return KatKerjaMap.map.get(value);
    }

    @Override
    public String toString() {
        return this.value;
    }
}
