package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.DocumentType;
import org.springframework.data.repository.CrudRepository;

public interface DocumentTypeRepository extends CrudRepository<DocumentType, Long> {
}
