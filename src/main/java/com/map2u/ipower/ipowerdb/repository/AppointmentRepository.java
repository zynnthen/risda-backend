package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.Appointment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AppointmentRepository extends CrudRepository<Appointment,String> {

    List<Appointment> findAll(Specification<Appointment> specs);

    List<Appointment> findAll(Specification<Appointment> specs, Pageable pageable);

    List<Appointment> findAll(Specification<Appointment> specs, Sort sort);

    boolean existsByNoSiriTemujanjiAndTsMohonId(String noSiriTemujanji, String tsMohonId);
}
