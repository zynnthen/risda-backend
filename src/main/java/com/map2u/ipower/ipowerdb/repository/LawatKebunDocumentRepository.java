package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.LawatKebunDocument;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LawatKebunDocumentRepository extends CrudRepository<LawatKebunDocument,String> {

    List<LawatKebunDocument> findByNoSiri(String noSiri);
}
