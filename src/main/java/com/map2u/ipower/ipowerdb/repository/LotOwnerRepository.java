package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.LotOwner;
import org.springframework.data.repository.CrudRepository;

public interface LotOwnerRepository extends CrudRepository<LotOwner,String> {
}
