package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.FarmBudgetChild;
import org.springframework.data.repository.CrudRepository;

public interface FarmBudgetChildRepository extends CrudRepository<FarmBudgetChild, Long> {
}
