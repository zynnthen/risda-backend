package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.AppointmentParticipant;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AppointmentParticipantRepository extends CrudRepository<AppointmentParticipant, Long> {

    boolean existsByNoSiriTemujanjiAndTsMohonIDAndKumpulanID(String noSiriTemujanji, String tsMohonId, String kumpulanId);

    Optional<AppointmentParticipant> findByNoSiriTemujanjiAndTsMohonIDAndKumpulanID(String noSiriTemujanji, String tsMohonId, String kumpulanId);

}
