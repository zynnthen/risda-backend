package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18EMast;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface LawatBayar18EMastRepository extends CrudRepository<LawatBayar18EMast, String> {

    Optional<LawatBayar18EMast> findByNoSiriLawatan(String noSiriLawatan);

    boolean existsByNoSiriLawatan(String noSiriLawatan);

    @Query(value = "SELECT a.formId FROM (" +
            "SELECT a.NoSiri_LB as formId, " +
            "a.Tkh_Lawat as tkhLawat " +
            "FROM TanamSemula.LawatBayar18A a " +
            "LEFT JOIN TanamSemula.LawatBayar18E_Mast e ON a.NoSiri_LB = e.NoSiri_Lawatan " +
            "WHERE e.NoSiri_Lawatan IS NULL " +
            "AND a.PTcode = :ptCode " +
            "AND a.Tkh_Lawat IS NOT NULL " +
            "UNION ALL " +
            "SELECT a.NoSiri_LK as formId, " +
            "a.Tkh_Lawat as tkhLawat " +
            "FROM TanamSemula.LawatKebun18 a " +
            "LEFT JOIN TanamSemula.LawatBayar18E_Mast e ON a.NoSiri_LK = e.NoSiri_Lawatan " +
            "WHERE e.NoSiri_Lawatan IS NULL " +
            "AND a.PTcode = :ptCode " +
            "AND a.Tkh_Lawat IS NOT NULL "
            + ") a " +
            "ORDER BY a.tkhLawat", nativeQuery = true,
            countQuery = "SELECT count(a.formId) FROM (" +
                    "SELECT a.NoSiri_LB as formId " +
                    "FROM TanamSemula.LawatBayar18A a " +
                    "LEFT JOIN TanamSemula.LawatBayar18E_Mast e ON a.NoSiri_LB = e.NoSiri_Lawatan " +
                    "WHERE e.NoSiri_Lawatan IS NULL " +
                    "AND a.PTcode = :ptCode " +
                    "AND a.Tkh_Lawat IS NOT NULL " +
                    "UNION ALL " +
                    "SELECT a.NoSiri_LK as formId " +
                    "FROM TanamSemula.LawatKebun18 a " +
                    "LEFT JOIN TanamSemula.LawatBayar18E_Mast e ON a.NoSiri_LK = e.NoSiri_Lawatan " +
                    "WHERE e.NoSiri_Lawatan IS NULL " +
                    "AND a.PTcode = :ptCode " +
                    "AND a.Tkh_Lawat IS NOT NULL "
                    + ") a ")
    Page<String> findFormsIdPendingValidation(String ptCode, Pageable pageable);

    @Query(value = "SELECT a.formId FROM (" +
            "SELECT a.NoSiri_LB as formId, " +
            "a.Tkh_Lawat as tkhLawat, " +
            "e.Jenis_LE as jenisLE " +
            "FROM TanamSemula.LawatBayar18A a " +
            "LEFT JOIN TanamSemula.LawatBayar18E_Mast e ON a.NoSiri_LB = e.NoSiri_Lawatan " +
            "WHERE e.NoSiri_Lawatan IS NOT NULL " +
            "AND a.PTcode = :ptCode " +
            "AND a.Tkh_Lawat IS NOT NULL " +
            "AND (:start IS NULL OR (:start IS NOT NULL AND a.Tkh_Lawat >= :start)) " +
            "AND (:end IS NULL OR (:end IS NOT NULL AND a.Tkh_Lawat <= :end)) " +
            "AND (:noKpPpk IS NULL OR (:noKpPpk IS NOT NULL AND a.No_KP_PPK = :noKpPpk)) " +
            "AND (:tsMohonId IS NULL OR (:tsMohonId IS NOT NULL AND a.TSmohon_ID = :tsMohonId)) " +
            "UNION ALL " +
            "SELECT a.NoSiri_LK as formId, " +
            "a.Tkh_Lawat as tkhLawat, " +
            "e.Jenis_LE as jenisLE " +
            "FROM TanamSemula.LawatKebun18 a " +
            "LEFT JOIN TanamSemula.LawatBayar18E_Mast e ON a.NoSiri_LK = e.NoSiri_Lawatan " +
            "WHERE e.NoSiri_Lawatan IS NOT NULL " +
            "AND a.PTcode = :ptCode " +
            "AND a.Tkh_Lawat IS NOT NULL " +
            "AND (:start IS NULL OR (:start IS NOT NULL AND a.Tkh_Lawat >= :start)) " +
            "AND (:end IS NULL OR (:end IS NOT NULL AND a.Tkh_Lawat <= :end)) " +
            "AND (:noKpPpk IS NULL OR (:noKpPpk IS NOT NULL AND a.No_KP_PPK = :noKpPpk)) " +
            "AND (:tsMohonId IS NULL OR (:tsMohonId IS NOT NULL AND a.TSmohon_ID = :tsMohonId)) "
            + ") a "
            + "WHERE (:jenisLE IS NULL OR (:jenisLE IS NOT NULL AND a.jenisLE = :jenisLE)) "
            + "ORDER BY a.tkhLawat",
            nativeQuery = true
            ,
            countQuery = "SELECT count(a.formId) FROM (" +
                    "SELECT a.NoSiri_LB as formId, " +
                    "e.Jenis_LE as jenisLE " +
                    "FROM TanamSemula.LawatBayar18A a " +
                    "LEFT JOIN TanamSemula.LawatBayar18E_Mast e ON a.NoSiri_LB = e.NoSiri_Lawatan " +
                    "WHERE e.NoSiri_Lawatan IS NOT NULL " +
                    "AND a.PTcode = :ptCode " +
                    "AND a.Tkh_Lawat IS NOT NULL " +
                    "AND (:start IS NULL OR (:start IS NOT NULL AND a.Tkh_Lawat >= :start)) " +
                    "AND (:end IS NULL OR (:end IS NOT NULL AND a.Tkh_Lawat <= :end)) " +
                    "AND (:noKpPpk IS NULL OR (:noKpPpk IS NOT NULL AND a.No_KP_PPK = :noKpPpk)) " +
                    "AND (:tsMohonId IS NULL OR (:tsMohonId IS NOT NULL AND a.TSmohon_ID = :tsMohonId)) " +
                    "UNION ALL " +
                    "SELECT a.NoSiri_LK as formId, " +
                    "e.Jenis_LE as jenisLE " +
                    "FROM TanamSemula.LawatKebun18 a " +
                    "LEFT JOIN TanamSemula.LawatBayar18E_Mast e ON a.NoSiri_LK = e.NoSiri_Lawatan " +
                    "WHERE e.NoSiri_Lawatan IS NOT NULL " +
                    "AND a.PTcode = :ptCode " +
                    "AND a.Tkh_Lawat IS NOT NULL " +
                    "AND (:start IS NULL OR (:start IS NOT NULL AND a.Tkh_Lawat >= :start)) " +
                    "AND (:end IS NULL OR (:end IS NOT NULL AND a.Tkh_Lawat <= :end)) " +
                    "AND (:noKpPpk IS NULL OR (:noKpPpk IS NOT NULL AND a.No_KP_PPK = :noKpPpk)) " +
                    "AND (:tsMohonId IS NULL OR (:tsMohonId IS NOT NULL AND a.TSmohon_ID = :tsMohonId)) "
                    + ") a "
                    + "WHERE (:jenisLE IS NULL OR (:jenisLE IS NOT NULL AND a.jenisLE = :jenisLE)) "
    )
    Page<String> findFormsIdCompleteValidation(
            @Param("ptCode") String ptCode,
            @Param("start") LocalDateTime start,
            @Param("end") LocalDateTime end,
            @Param("noKpPpk") String noKpPpk,
            @Param("tsMohonId") String tsMohonId,
            @Param("jenisLE") String jenisLE,
            Pageable pageable);

}
