package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.TSLejerMast;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TSLejerMastRepository extends CrudRepository<TSLejerMast, String> {

    List<TSLejerMast> findByTsMohonId(String tsMohonId);
}
