package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18A;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface LawatBayar18ARepository extends CrudRepository<LawatBayar18A, String> {

    boolean existsByNoSiriTemujanjiAndTsMohonIdAndKumpulanId(String noSiriTemujanji, String tsMohonId, String kumpulanId);

    boolean existsByNoSiriTemujanjiAndTsMohonId(String noSiriTemujanji, String tsMohonId);

    @Query(value = "SELECT top 1 Bil_Lawatan FROM TanamSemula.LawatBayar18A " +
            "WHERE TSmohon_ID = :tsMohonId " +
            "ORDER BY Bil_Lawatan DESC ",nativeQuery = true)
    Integer findBilLawatanByTsMohonId(@Param("tsMohonId") String tsMohonId);

    List<LawatBayar18A> findByNoSiriTemujanjiAndTsMohonIdOrKumpulanId(String noSiriTemujanji, String tsMohonId, String kumpulanId);

    List<LawatBayar18A> findAll(Specification<LawatBayar18A> specification);

    @Query(value = "SELECT a FROM LawatBayar18A a " +
            "LEFT JOIN LawatBayar18EMast e ON a.noSiriLB = e.noSiriLawatan " +
            "WHERE e.noSiriLawatan IS NULL " +
            "AND a.ptCode = :ptCode " +
            "AND (:start IS NULL OR (:start IS NOT NULL AND a.tkhLawat >= :start)) " +
            "AND (:end IS NULL OR (:end IS NOT NULL AND a.tkhLawat <= :end)) " +
            "AND (:noKpPpk IS NULL OR (:noKpPpk IS NOT NULL AND a.noKpPpk = :noKpPpk)) " +
            "AND (:tsMohonId IS NULL OR (:tsMohonId IS NOT NULL AND a.tsMohonId = :tsMohonId)) " +
            "AND a.tkhLawat IS NOT NULL " +
            "AND a.kumpulanId != '' " +
            "ORDER BY a.tkhLawat")
    List<LawatBayar18A> findFormsPendingValidation(
            @Param("ptCode") String ptCode,
            @Param("start") LocalDateTime start,
            @Param("end") LocalDateTime end,
            @Param("noKpPpk") String noKpPpk,
            @Param("tsMohonId") String tsMohonId
    );

    List<LawatBayar18A> findByNoSiriLBIn(List<String> noSiri);
}
