package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.FarmBudgetMast;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface FarmBudgetMastRepository extends CrudRepository<FarmBudgetMast, String> {

    Optional<FarmBudgetMast> findByNoTSBRPKAndPtCode(String noTSBRPK, String ptCode);

    Optional<FarmBudgetMast> findByNoTSBRPKOrPtCode(String noTSBRPK, String ptCode);
}
