package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.KodKerja;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface KodKerjaRepository extends CrudRepository<KodKerja, String> {

    List<KodKerja> findByKodKerjaIdIn(Set<String> kodKerjaIds);
}
