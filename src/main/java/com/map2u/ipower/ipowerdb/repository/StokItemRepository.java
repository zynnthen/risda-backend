package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.StokItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Set;

public interface StokItemRepository extends CrudRepository<StokItem, String> {

    List<StokItem> findByKodStokItemIn(Set<String> kodStokItemIds);
}
