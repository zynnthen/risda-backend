package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18AAktiviti;
import org.springframework.data.repository.CrudRepository;

public interface LawatBayar18AAktivitiRepository extends CrudRepository<LawatBayar18AAktiviti, Long> {
}
