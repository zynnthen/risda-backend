package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.TSApplication;
import org.springframework.data.repository.CrudRepository;

public interface TSApplicationRepository extends CrudRepository<TSApplication,String> {
}
