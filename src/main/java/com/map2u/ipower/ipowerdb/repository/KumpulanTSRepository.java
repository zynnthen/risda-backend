package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.KumpulanTS;
import org.springframework.data.repository.CrudRepository;

public interface KumpulanTSRepository extends CrudRepository<KumpulanTS, String> {
}
