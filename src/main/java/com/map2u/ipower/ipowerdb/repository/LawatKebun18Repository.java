package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18A;
import com.map2u.ipower.ipowerdb.entity.LawatKebun18;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface LawatKebun18Repository extends CrudRepository<LawatKebun18, String> {

//    Optional<LawatKebun18> findByNoSiriTemujanjiAndTsMohonId(String noSiriTemujanji, String tsMohonId);

    boolean existsByNoSiriTemujanjiAndTsMohonId(String noSiriTemujanji, String tsMohonId);

    List<LawatKebun18> findAll(Specification<LawatKebun18> specification);

    List<LawatKebun18> findByNoSiriTemujanjiAndTsMohonId(String noSiriTemujanji, String tsMohonId);

    @Query(value = "SELECT top 1 Bil_Lawatan FROM TanamSemula.LawatKebun18 " +
            "WHERE TSmohon_ID = :tsMohonId " +
            "ORDER BY Bil_Lawatan DESC ",nativeQuery = true)
    Integer findBilLawatanByTsMohonId(@Param("tsMohonId") String tsMohonId);

    @Query(value = "SELECT a FROM LawatKebun18 a " +
            "LEFT JOIN LawatBayar18EMast e ON a.noSiriLK = e.noSiriLawatan " +
            "WHERE e.noSiriLawatan IS NULL " +
            "AND a.ptCode = :ptCode " +
            "AND (:start IS NULL OR (:start IS NOT NULL AND a.tkhLawat >= :start)) " +
            "AND (:end IS NULL OR (:end IS NOT NULL AND a.tkhLawat <= :end)) " +
            "AND (:noKpPpk IS NULL OR (:noKpPpk IS NOT NULL AND a.noKpPpk = :noKpPpk)) " +
            "AND (:tsMohonId IS NULL OR (:tsMohonId IS NOT NULL AND a.tsMohonId = :tsMohonId)) " +
            "AND a.tkhLawat IS NOT NULL " +
            "ORDER BY a.tkhLawat")
    List<LawatKebun18> findFormsPendingValidation(
            @Param("ptCode") String ptCode,
            @Param("start") LocalDateTime start,
            @Param("end") LocalDateTime end,
            @Param("noKpPpk") String noKpPpk,
            @Param("tsMohonId") String tsMohonId
    );


    List<LawatKebun18> findByNoSiriLKIn(List<String> noSiri);
}
