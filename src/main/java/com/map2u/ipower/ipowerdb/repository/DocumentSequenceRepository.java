package com.map2u.ipower.ipowerdb.repository;

import com.map2u.ipower.ipowerdb.entity.DocumentSequence;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface DocumentSequenceRepository extends CrudRepository<DocumentSequence, Long> {

    Optional<DocumentSequence> findFirstByDocTypeAndPtCodeOrderByCutOffDateDesc(long docType, String ptCode);

    Optional<DocumentSequence> findByDocTypeAndYearAndPtCode(long docType,String year, String ptCode);
}
