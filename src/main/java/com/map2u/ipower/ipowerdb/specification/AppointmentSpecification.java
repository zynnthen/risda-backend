package com.map2u.ipower.ipowerdb.specification;

import com.map2u.ipower.ipowerdb.entity.Appointment;
import com.map2u.ipower.ipowerdb.entity.LotOwner;
import com.map2u.ipower.ipowerdb.entity.TSApplication;
import com.map2u.ipower.model.appointment.AppointmentParam;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AppointmentSpecification implements Specification<Appointment> {

    private AppointmentParam params;

    public AppointmentSpecification() {
    }

    public AppointmentSpecification(AppointmentParam params) {
        this.params = params;
    }

    @Override
    public Predicate toPredicate(Root<Appointment> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> filters = new ArrayList<>();

        Join<Appointment, TSApplication> tsApplicationJoin = root.join("tsApplication", JoinType.LEFT);
        Join<TSApplication, LotOwner> lotOwnerJoin = tsApplicationJoin.join("lotOwners", JoinType.LEFT);

        if (this.params.getNamaPemohon() != null) {
            filters.add(
                    criteriaBuilder.like(
                            criteriaBuilder.upper(tsApplicationJoin.get("namaPemohon")),
                            "%" + this.params.getNamaPemohon().toUpperCase() + "%"
                    )
            );
        }

        if (this.params.getPegKemaskini() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            root.get("pegKemaskini"),
                            this.params.getPegKemaskini()
                    )
            );
        }

        if (this.params.getNoKpPemohon() != null) {
            filters.add(
                    criteriaBuilder.like(
                            criteriaBuilder.upper(tsApplicationJoin.get("icNoPemohon")),
                            "%" + this.params.getNoKpPemohon().toUpperCase() + "%"
                    )
            );
        }

        if (this.params.getTsMohonId() != null) {
            filters.add(
                    criteriaBuilder.like(
                            criteriaBuilder.upper(root.get("tsMohonId")),
                            "%" + this.params.getTsMohonId().toUpperCase() + "%"
                    )
            );
        }


        if (this.params.getDate() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            root.get("tkhTJanji"),
                            this.params.getDate()
                    )
            );
        } else {
            if (this.params.getStart() != null) {
                filters.add(
                        criteriaBuilder.greaterThanOrEqualTo(
                                root.get("tkhTJanji"),
                                new Date(this.params.getStart() * 1000)
                        )
                );
            }

            if (this.params.getEnd() != null) {
                filters.add(
                        criteriaBuilder.lessThanOrEqualTo(
                                root.get("tkhTJanji"),
                                new Date(this.params.getEnd() * 1000)
                        )
                );
            }
        }


        if (this.params.getNoGeran() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            lotOwnerJoin.get("noGeran"),
                            this.params.getNoGeran()
                    )
            );
        }

        if (this.params.getNoLot() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            lotOwnerJoin.get("noLot"),
                            this.params.getNoLot()
                    )
            );
        }

        if (this.params.getStatusTJanji() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            criteriaBuilder.upper(root.get("statusTJanji")),
                            this.params.getStatusTJanji().toUpperCase()
                    )
            );
        }

        if (this.params.getPtCode() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            criteriaBuilder.upper(root.get("ptCode")),
                            this.params.getPtCode().toUpperCase()
                    )
            );
        }


        return criteriaBuilder.and(filters.toArray(new Predicate[filters.size()]));
    }
}
