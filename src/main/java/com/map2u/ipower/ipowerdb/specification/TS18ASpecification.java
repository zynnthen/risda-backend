package com.map2u.ipower.ipowerdb.specification;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18A;
import com.map2u.ipower.model.form.FormParam;
import com.map2u.ipower.model.form.LawatBayar18AParam;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TS18ASpecification implements Specification<LawatBayar18A> {

    LawatBayar18AParam param;

    public TS18ASpecification(LawatBayar18AParam param) {
        this.param = param;
    }

    public TS18ASpecification(FormParam param) {
        this.param = new LawatBayar18AParam();
        this.param.setNoSiriTemujanji(param.getNoSiriTemujanji());
        this.param.setPtCode(param.getPtCode());
        this.param.setTkhLawat(param.getTkhLawat());
        this.param.setTsMohonId(param.getTsMohonId());

    }

    @Override
    public Predicate toPredicate(Root<LawatBayar18A> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> filters = new ArrayList<>();

        if (this.param.getNoSiriLB() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            root.get("noSiriLB"),
                            this.param.getNoSiriLB()
                    )
            );
        } else {

            if (this.param.getKumpulanId() != null) {
                filters.add(
                        criteriaBuilder.equal(
                                root.get("kumpulanId"),
                                this.param.getKumpulanId()
                        )
                );
            }

            if (this.param.getTsMohonId() != null) {
                filters.add(
                        criteriaBuilder.equal(
                                root.get("tsMohonId"),
                                this.param.getTsMohonId()
                        )
                );
            }

            if (this.param.getNoSiriTemujanji() != null) {
                filters.add(
                        criteriaBuilder.equal(
                                root.get("noSiriTemujanji"),
                                this.param.getNoSiriTemujanji()
                        )
                );
            }

            if (this.param.getTkhLawat() != null) {
                filters.add(
                        criteriaBuilder.greaterThanOrEqualTo(
                                root.get("tkhLawat"),
                                new Date(this.param.getTkhLawat())
                        )
                );
            }

            if (this.param.getPtCode() != null) {
                filters.add(
                        criteriaBuilder.equal(
                                root.get("ptCode"),
                                this.param.getPtCode()
                        )
                );
            }
        }
        return criteriaBuilder.and(filters.toArray(new Predicate[filters.size()]));
    }
}
