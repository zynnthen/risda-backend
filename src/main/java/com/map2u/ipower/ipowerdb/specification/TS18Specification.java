package com.map2u.ipower.ipowerdb.specification;

import com.map2u.ipower.ipowerdb.entity.LawatKebun18;
import com.map2u.ipower.model.form.FormParam;
import com.map2u.ipower.model.form.LawatKebun18Param;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TS18Specification implements Specification<LawatKebun18> {

    private LawatKebun18Param param;

    public TS18Specification(LawatKebun18Param param) {
        this.param = param;
    }

    public TS18Specification(FormParam param) {
        this.param = new LawatKebun18Param();
        this.param.setNoSiriTemujanji(param.getNoSiriTemujanji());
        this.param.setPtCode(param.getPtCode());
        this.param.setTkhLawat(param.getTkhLawat());
        this.param.setTsMohonId(param.getTsMohonId());
    }

    @Override
    public Predicate toPredicate(Root<LawatKebun18> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> filters = new ArrayList<>();

        if (this.param.getNoSiriLK() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            root.get("noSiriLK"),
                            this.param.getNoSiriLK()
                    )
            );
        } else {
            if (this.param.getTsMohonId() != null) {
                filters.add(
                        criteriaBuilder.equal(
                                root.get("tsMohonId"),
                                this.param.getTsMohonId()
                        )
                );
            }

            if (this.param.getNoSiriTemujanji() != null) {
                filters.add(
                        criteriaBuilder.equal(
                                root.get("noSiriTemujanji"),
                                this.param.getNoSiriTemujanji()
                        )
                );
            }

            if (this.param.getTkhLawat() != null) {
                filters.add(
                        criteriaBuilder.greaterThanOrEqualTo(
                                root.get("tkhLawat"),
                                new Date(this.param.getTkhLawat())
                        )
                );
            }

            if (this.param.getPtCode() != null) {
                filters.add(
                        criteriaBuilder.equal(
                                root.get("ptCode"),
                                this.param.getPtCode()
                        )
                );
            }
        }

        return criteriaBuilder.and(filters.toArray(new Predicate[filters.size()]));
    }
}
