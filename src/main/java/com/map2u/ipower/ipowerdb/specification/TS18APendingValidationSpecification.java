package com.map2u.ipower.ipowerdb.specification;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18A;
import com.map2u.ipower.ipowerdb.entity.LawatBayar18EMast;
import com.map2u.ipower.model.form.FormParam;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

public class TS18APendingValidationSpecification implements Specification<LawatBayar18A> {

    private FormParam param;

    public TS18APendingValidationSpecification(FormParam param) {
        this.param = param;
    }

    @Override
    public Predicate toPredicate(Root<LawatBayar18A> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        List<Predicate> filters = new ArrayList<>();


        Join<LawatBayar18A, LawatBayar18EMast> join = root.join("ts18E", JoinType.LEFT);

//        join = join.on(criteriaBuilder.equal(
//                root.get("noSiriLB"),
//                join.get("noSiriLawatan")
//        ));

        filters.add(
                criteriaBuilder.equal(root.get("noSiriLB"), join.get("noSiriLawatan"))
        );

        filters.add(
                criteriaBuilder.isNull(join.get("noSiriLawatan"))
        );

        filters.add(
                criteriaBuilder.notEqual(root.get("kumpulanId"), "")
        );

        if (this.param.getPtCode() != null) {
            filters.add(
                    criteriaBuilder.equal(
                            root.get("ptCode"),
                            this.param.getPtCode()
                    )
            );
        }


        return criteriaBuilder.and(filters.toArray(new Predicate[filters.size()]));
    }
}
