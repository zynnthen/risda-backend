package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.form.LawatKebunDocumentModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "LawatKebunDocument", schema = "TanamSemula")
@Data
public class LawatKebunDocument {

    @Id
    protected String id;

    @Column(name = "no_siri")
    private String noSiri;

    @Column(name = "doc_type")
    private String docType;

    @Column(name = "doc_name")
    private String docName;

    public LawatKebunDocumentModel convertEntityToModel() {
        LawatKebunDocumentModel model = new LawatKebunDocumentModel();

        model.setId(this.id);
        model.setDocName(this.docName);
        model.setDocType(this.docType);
        model.setNoSiri(this.noSiri);

        return model;
    }

}
