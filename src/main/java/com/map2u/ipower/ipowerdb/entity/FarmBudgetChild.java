package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.FarmBudgetChildModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "FarmBudget_Child", schema = "TanamSemula")
@Data
public class FarmBudgetChild {

    @Column(name = "PTCode")
    String ptCode;

    @Id
    @Column(name = "NoSiri_FBC")
    long noSiriFBC;

    @Column(name = "NoSiri_FBM")
    String noSiriFBM;

    @Column(name = "Ansuran_No")
    String ansuranNo;

    @Column(name = "KatKerja")
    String katKerja;

    @Column(name = "KerjaStok_Kod")
    String kerjaStokKod;

    @Column(name = "Stok_Item")
    String stokItem;

    @Column(name = "Jumlah_Unit")
    Integer jumlahUnit;

    @Column(name = "Harga_Unit")
    Double hargaUnit;

    @Column(name = "Unit_Hektar")
    Integer unitHektar;

    @Column(name = "Harga_Kos")
    Double hargaKos;

    @Column(name = "Status_Byr")
    String statusByr;

    @OneToOne
    @JoinColumn(name = "NoSiri_FBM", insertable = false, updatable = false)
    FarmBudgetMast farmBudgetMast;

//    @OneToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "Stok_Item", insertable = false, updatable = false)
//    StokItem stok;
//
//    @OneToOne
//    @JoinColumn(name = "KerjaStok_Kod", insertable = false, updatable = false)
//    KodKerja kerja;

    public FarmBudgetChildModel convertEntityToModel() {
        FarmBudgetChildModel model = new FarmBudgetChildModel();

        model.setAnsuranNo(this.ansuranNo);
        model.setHargaKos(this.hargaKos);
        model.setHargaUnit(this.hargaUnit);
        model.setJumlahUnit(this.jumlahUnit);
        model.setKatKerja(this.katKerja);
        model.setKerjaStokKod(this.kerjaStokKod);
        model.setNoSiriFBC(this.noSiriFBC);
        model.setNoSiriFBM(this.noSiriFBM);
        model.setPtCode(this.ptCode);
        model.setStatusByr(this.statusByr);
        model.setStokItem(this.stokItem);
        model.setUnitHektar(this.unitHektar);

//        if (this.stok != null) {
//            model.setPerihalStokItem(this.stok.getPerihalStokItem());
//        }

        return model;
    }
}
