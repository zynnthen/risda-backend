package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SP_Parlimen", schema = "UtilityDict")
@Data
public class SpParlimen implements Serializable {

    @Id
    @Column(name = "U_Parlimen_ID")
    String parlimenId;

    @Column(name = "Parlimen")
    String parlimen;

    @Column(name = "Kod_Parlimen")
    String kodParlimen;
}
