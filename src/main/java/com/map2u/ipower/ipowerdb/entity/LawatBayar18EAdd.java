package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "LawatBayar18E_Add", schema = "TanamSemula")
@Data
public class LawatBayar18EAdd {

    @Id
    @Column(name = "NoSiri_LE")
    String noSiriLE;

    @Lob
    @Column(name = "sign_pegawai")
    String signPegawai;

    @Column(name = "latitude")
    Double latitude;

    @Column(name = "longitude")
    Double longitude;

    @OneToOne
    @JoinColumn(name = "NoSiri_LE", updatable = false, insertable = false)
    LawatBayar18EMast form;

}
