package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.form.LawatBayar18EChildModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "LawatBayar18E_Child", schema = "TanamSemula")
@Data
public class LawatBayar18EChild {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "NoSiri_LE")
    String noSiriLE;

    @Column(name = "Item")
    String item;

    @Column(name = "Catatan")
    String catatan;

    @OneToOne
    @JoinColumn(name = "NoSiri_LE", insertable = false, updatable = false)
    LawatBayar18EMast lawatBayar18EMast;

    public LawatBayar18EChildModel convertEntityToModel() {
        LawatBayar18EChildModel model = new LawatBayar18EChildModel();
        model.setId(this.id);
        model.setCatatan(this.catatan);
        model.setItem(this.item);
        model.setNoSiriLE(this.noSiriLE);
        return model;
    }
}
