package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.appointment.TSLejerChildModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "TSLejer_Child", schema = "TanamSemula")
@Data
public class TSLejerChild {

    @Column(name = "PTcode")
    String ptCode;

    @Id
    @Column(name = "NoLejerChild")
    long noLejerChild;

    @Column(name = "NoLejer")
    String noLejer;

    @Column(name = "NoBaris")
    Long noBaris;

    @Column(name = "Transaksi_Desc")
    String transaksiDesc;

    @Column(name = "Ansuran_No")
    String ansuranNo;

    @Column(name = "KerjaStok_Kod")
    String kerjaStokKod;

    @Column(name = "KerjaStok_Desc")
    String kerjaStokDesc;

    @Column(name = "Luas_Bayar")
    Double luasBayar;

    @Column(name = "Jenis_Stok")
    String jenisStok;

    @Column(name = "Kuantiti_Stok")
    Integer kuantitiStok;

    @Column(name = "KerjaStok_Hrg")
    Double kerjaStokHrg;

    @Column(name = "KerjaStok_Amt")
    Double kerjaStokAmt;

    @OneToOne
    @JoinColumn(name = "NoLejer", insertable = false, updatable = false)
    TSLejerMast tsLejerMast;

    public TSLejerChildModel convertEntityToModel() {
        TSLejerChildModel model = new TSLejerChildModel();
        model.setAnsuranNo(this.ansuranNo);
        model.setJenisStok(this.jenisStok);
        model.setKerjaStokAmt(this.kerjaStokAmt);
        model.setKerjaStokDesc(this.kerjaStokDesc);
        model.setKerjaStokHrg(this.kerjaStokHrg);
        model.setKerjaStokKod(this.kerjaStokKod);
        model.setKuantitiStok(this.kuantitiStok);
        model.setLuasBayar(this.luasBayar);
        model.setNoBaris(this.noBaris);
        model.setNoLejer(this.noLejer);
        model.setNoLejerChild(this.noLejerChild);
        model.setPtCode(this.ptCode);
        model.setTransaksiDesc(this.transaksiDesc);
        return model;
    }
}
