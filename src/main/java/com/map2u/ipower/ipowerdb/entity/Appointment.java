package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.appointment.AppointmentModel;
import com.map2u.ipower.model.appointment.ParticipantAppointmentModel;
import com.map2u.ipower.util.Util;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "Temujanji", schema = "TanamSemula")
@Data
public class Appointment {

    @Id
    @Column(name = "NoSiri_Temujanji")
    String noSiriTemujanji;

    @Column(name = "PTcode")
    String ptCode;

    @Column(name = "ModuleOwner")
    String moduleOwner;

    @Column(name = "TSPendekatan")
    String tsPendekatan;

    @Column(name = "Jenis_LK")
    String jenisLK;

    @Column(name = "TSmohon_ID")
    String tsMohonId;

    @Column(name = "Kumpulan_ID")
    String kumpulanId;

    @Column(name = "Kumpulan_Claim")
    String kumpulanClaim;

    @Column(name = "Tkh_TJanji")
    @Temporal(TemporalType.DATE)
    Date tkhTJanji;

    @Column(name = "Masa_TJanji")
    @Temporal(TemporalType.TIME)
    Date masaTJanji;

    @Column(name = "Tmpt_TJanji")
    String tempatTJanji;

    @Column(name = "Status_TJanji")
    String statusTJanji;

    @Column(name = "Peg_Kemaskini")
    String pegKemaskini;

    @Column(name = "Tkh_Kemaskini")
    @Temporal(TemporalType.TIMESTAMP)
    Date tkhKemaskini;

    @Column(name = "Ansuran_No")
    String ansuranNo;

    @OneToMany(mappedBy = "appointment", fetch = FetchType.EAGER)
    List<AppointmentParticipant> participants;

    @ManyToOne
    @JoinColumn(name = "TSmohon_ID", insertable = false, updatable = false)
    TSApplication tsApplication;


    public AppointmentModel convertEntityToModel() {
        AppointmentModel model = new AppointmentModel();

        model.setAnsuranNo(this.ansuranNo);
        model.setJenisLK(this.jenisLK);
        model.setKumpulanClaim(this.kumpulanClaim);
        model.setKumpulanId(this.kumpulanId);
        if (this.tkhTJanji != null) {
            model.setTkhTJanji(this.tkhTJanji.getTime());
        }
        model.setMasaTJanji(Util.convertTimeToMillisecond(this.masaTJanji));
        model.setModuleOwner(this.moduleOwner);
        model.setNoSiriTemujanji(this.noSiriTemujanji);
        model.setPegKemaskini(this.pegKemaskini);
        model.setPtCode(this.ptCode);
        model.setStatusTJanji(this.statusTJanji);
        model.setTempatTJanji(this.tempatTJanji);
        model.setTkhKemaskini(this.tkhKemaskini.getTime());
        model.setTsMohonId(this.tsMohonId);
        model.setTsPendekatan(this.tsPendekatan);

        if (this.participants != null) {

            List<ParticipantAppointmentModel> participants = this.participants.stream()
                    .map(p -> p.convertEntityToModel(this.ansuranNo))
                    .collect(Collectors.toList());
            model.setParticipants(participants);
        }

        if (this.tsApplication != null) {
            model.setTsApplication(this.tsApplication.convertEntityToModel());
        }


        return model;
    }
}
