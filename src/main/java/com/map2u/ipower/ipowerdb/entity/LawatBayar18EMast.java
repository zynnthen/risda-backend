package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.form.LawatBayar18EChildModel;
import com.map2u.ipower.model.form.LawatBayar18EModel;
import com.map2u.ipower.util.Util;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "LawatBayar18E_Mast", schema = "TanamSemula")
@Data
public class LawatBayar18EMast {

    @Id
    @Column(name = "NoSiri_LE")
    String noSiriLE;

    @Column(name = "PTcode")
    String ptCode;

    @Column(name = "NoSiri_Lawatan")
    String noSiriLawatan;

    @Column(name = "Jenis_LE")
    String jenisLE;

    @Column(name = "TSmohon_ID")
    String tsMohonId;

    @Column(name = "Catatan")
    String catatan;

    @Column(name = "Luas_Lulus")
    Double luasLulus;

    @Column(name = "Status_Lulus")
    String statusLulus;

    @Column(name = "Peg_Lulus")
    String pegLulus;

    @Column(name = "Tkh_Lulus")
    LocalDateTime tkhLulus;

    @Column(name = "Tkh_Lawat")
    LocalDateTime tkhLawat;

    @Column(name = "MLawat_Mula")
    @Temporal(TemporalType.TIME)
    Date mLawatMula;

    @Column(name = "MLawat_Tamat")
    @Temporal(TemporalType.TIME)
    Date mLawatTamat;

    @Column(name = "Img_Plan")
    String imgPlan;

    @Column(name="Smartmap_link")
    String smartmapLink;

    @OneToMany(mappedBy = "lawatBayar18EMast", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    List<LawatBayar18EChild> items;

    @OneToOne(mappedBy = "form", cascade = CascadeType.ALL)
    LawatBayar18EAdd additional;

    public LawatBayar18EModel convertEntityToModel() {
        LawatBayar18EModel model = new LawatBayar18EModel();

        model.setCatatan(this.catatan);
        model.setImgPlan(this.imgPlan);
        model.setJenisLE(this.jenisLE);
        model.setLuasLulus(this.luasLulus);
        model.setMLawatMula(Util.convertTimeToMillisecond(this.mLawatMula));
        model.setMLawatTamat(Util.convertTimeToMillisecond(this.mLawatTamat));
        model.setNoSiriLawatan(this.noSiriLawatan);
        model.setNoSiriLE(this.noSiriLE);
        model.setPegLulus(this.pegLulus);
        model.setPtCode(this.ptCode);
        model.setStatusLulus(this.statusLulus);
        model.setTkhLawat(Util.convertDateTimeToMiliSeconds(this.tkhLawat));
        model.setTkhLulus(Util.convertDateTimeToMiliSeconds(this.tkhLulus));
        model.setTsMohonId(this.tsMohonId);
        model.setSmartmapLink(this.smartmapLink);

        if (this.additional != null) {
            model.setSignPegawai(this.additional.getSignPegawai());
            model.setLatitude(this.additional.getLatitude());
            model.setLongitude(this.additional.getLongitude());
        }


        List<LawatBayar18EChildModel> items = this.items.stream().map(i -> i.convertEntityToModel()).collect(Collectors.toList());
        model.setItems(items);

        return model;
    }
}
