package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.form.LawatKebun18Model;
import com.map2u.ipower.util.Util;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "LawatKebun18", schema = "TanamSemula")
@Data
public class LawatKebun18 {

    @Column(name = "PTcode")
    String ptCode;

    @Id
    @Column(name = "NoSiri_LK")
    String noSiriLK;

    @Column(name = "NoSiri_Temujanji")
    String noSiriTemujanji;

    @Column(name = "Jenis_LK")
    String jenisLK;

    @Column(name = "TSmohon_ID")
    String tsMohonId;

    @Column(name = "Bantuan_Lain")
    String bantuanLain;

    @Column(name = "BL_Agensi")
    String blAgensi;

    @Column(name = "BL_Crop")
    String blCrop;

    @Column(name = "BL_NoTS")
    String blNoTs;

    @Column(name = "BL_LuasLulus")
    Double blLuasLulus;

    @Column(name = "BL_LuasHabisByr")
    Double blLuasHabisByr;

    @Column(name = "Luas_GetahTua")
    Double luasGetahTua;

    @Column(name = "Tunggul_Getah")
    String tunggulGetah;

    @Column(name = "Lesen_Getah")
    String lesenGetah;

    @Column(name = "Buku_Lesen")
    String bukuLesen;

    @Column(name = "Surat_Agensi")
    String suratAgensi;

    @Column(name = "Gambar_Satelit")
    String gambarSatelit;

    @Column(name = "Bukti_lot")
    String buktiLot;

    @Column(name = "Umur_Pokok")
    String umurPokok;

    @Column(name = "Getah_Tua")
    String getahTua;

    @Column(name = "Pokok_K300")
    String pokokK300;

    @Column(name = "Hasil_K500")
    String hasilK500;

    @Column(name = "Kulit_Torehan")
    String kulitTorehan;

    @Column(name = "Pelan_Tanah")
    String pelanTanah;

    @Column(name = "PK_Hadir")
    String pkHadir;

    @Column(name = "Catatan")
    String catatan;

    @Column(name = "Peg_Pemeriksa")
    String pegPemeriksa;

    @Column(name = "No_KP_PPK")
    String noKpPpk;

    @Column(name = "Jaw_Pemeriksa")
    String jawPemeriksa;

    @Column(name = "Tkh_Lawat")
    LocalDateTime tkhLawat;

    @Column(name = "MLawat_Mula")
    String mLawatMula;

    @Column(name = "MLawat_Tamat")
    String mLawatTamat;

    @Column(name = "Peg_Kemaskini")
    String pegKemaskini;

    @Column(name = "Tkh_Kemaskini")
    LocalDateTime tkhKemaskini;

    @Column(name = "Status_Lulus")
    String statusLulus;

    @Column(name = "Alasan_Lulus")
    String alasanLulus;

    @Column(name = "Alasan_No")
    Integer alasanNo;

    @Column(name = "Catatan_Lulus")
    String catatanLulus;

    @Column(name = "Luas_Lulus")
    Double luasLulus;

    @Column(name = "Peg_Lulus")
    String pegLulus;

    @Column(name = "Jaw_Peg_Lulus")
    String jawPegLulus;

    @Column(name = "Tkh_Lulus")
    LocalDateTime tkhLulus;

    @Column(name = "Bil_Lawatan")
    Integer bilLawatan;

    @Column(name = "Luas_Lain")
    Double luasLain;

    @Column(name = "Bukti_Lot_TS")
    String buktiLotTs;

    @Column(name = "Sebab_Lain_PRN")
    String sebabLainPRN;

    @Column(name = "Luas_TanamanLain")
    Double luasTanamanLain;

    @Column(name = "Luas_Hutan")
    Double luasHutan;

    @Column(name = "Luas_Rumah")
    Double luasRumah;

    @Column(name = "Lain_Lain")
    String lainLain;

    @Column(name = "Luas_LainLain")
    Double luasLainLain;

    @Column(name="Smartmap_link")
    String smartmapLink;

    @OneToOne(mappedBy = "form", cascade = CascadeType.ALL)
    LawatKebun18Add additional;

    @ManyToOne
    @JoinColumn(name = "TSmohon_ID", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    TSApplication tsApplication;


    @PrePersist
    @PreUpdate
    public void updateTkhKemaskini() {
        this.tkhKemaskini = Util.getCurrentTime();
    }

    public LawatKebun18Model convertEntityToModel() {
        LawatKebun18Model model = new LawatKebun18Model();

        model.setAlasanLulus(this.alasanLulus);
        model.setAlasanNo(this.alasanNo);
        model.setBantuanLain(this.bantuanLain);
        model.setBilLawatan(this.bilLawatan);
        model.setBlAgensi(this.blAgensi);
        model.setBlCrop(this.blCrop);
        model.setBlLuasHabisByr(this.blLuasHabisByr);
        model.setBlLuasLulus(this.blLuasLulus);
        model.setBlNoTs(this.blNoTs);
        model.setBuktiLot(this.buktiLot);
        model.setBuktiLotTs(this.buktiLotTs);
        model.setBukuLesen(this.bukuLesen);
        model.setCatatan(this.catatan);
        model.setCatatanLulus(this.catatanLulus);
        model.setGambarSatelit(this.gambarSatelit);
        model.setGetahTua(this.getahTua);
        model.setHasilK500(this.hasilK500);
        model.setJawPegLulus(this.jawPegLulus);
        model.setJawPemeriksa(this.jawPemeriksa);
        model.setJenisLK(this.jenisLK);
        model.setKulitTorehan(this.kulitTorehan);
        model.setLainLain(this.lainLain);
        model.setLesenGetah(this.lesenGetah);
        model.setLuasGetahTua(this.luasGetahTua);
        model.setLuasHutan(this.luasHutan);
        model.setLuasLain(this.luasLain);
        model.setLuasLainLain(this.luasLainLain);
        model.setLuasLulus(this.luasLulus);
        model.setLuasRumah(this.luasRumah);
        model.setLuasTanamanLain(this.luasTanamanLain);
        model.setMLawatMula(this.mLawatMula);
        model.setMLawatTamat(this.mLawatTamat);
        model.setNoKpPpk(this.noKpPpk);
        model.setNoSiriLK(this.noSiriLK);
        model.setNoSiriTemujanji(this.noSiriTemujanji);
        model.setPegKemaskini(this.pegKemaskini);
        model.setPegLulus(this.pegLulus);
        model.setPegPemeriksa(this.pegPemeriksa);
        model.setPelanTanah(this.pelanTanah);
        model.setPkHadir(this.pkHadir);
        model.setPokokK300(this.pokokK300);
        model.setPtCode(this.ptCode);
        model.setSebabLainPRN(this.sebabLainPRN);
        model.setStatusLulus(this.statusLulus);
        model.setSuratAgensi(this.suratAgensi);
        model.setTkhKemaskini(Util.convertDateTimeToMiliSeconds(this.tkhKemaskini));
        model.setTkhLawat(Util.convertDateTimeToMiliSeconds(this.tkhLawat));
        model.setTkhLulus(Util.convertDateTimeToMiliSeconds(this.tkhLulus));
        model.setTsMohonId(this.tsMohonId);
        model.setTunggulGetah(this.tunggulGetah);
        model.setUmurPokok(this.umurPokok);
        model.setTsApplication(this.tsApplication.convertEntityToModel());
        model.setSmartmapLink(this.smartmapLink);

        if (this.additional != null) {
            model.setSignPegawai(this.additional.getSignPegawai());
            model.setSignPerakuan(this.additional.getSignPerakuan());
            model.setNamaPerakuan(this.additional.getNamaPerakuan());
            model.setNoKpPerakuan(this.additional.getNoKpPerakuan());
            model.setHadir(this.additional.getHadir());
            model.setLongitude(this.additional.getLongitude());
            model.setLatitude(this.additional.getLatitude());
//            System.out.println("No Siri LB:" + this.additional.getNoSiriLB());
        }

        return model;
    }
}
