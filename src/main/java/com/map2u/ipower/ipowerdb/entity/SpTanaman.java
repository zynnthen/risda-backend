package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SP_Tanaman", schema = "UtilityDict")
@Data
public class SpTanaman {

    @Id
    @Column(name = "U_LJTanaman_ID")
    String id;

    @Column(name = "Tanaman")
    String tanaman;
}
