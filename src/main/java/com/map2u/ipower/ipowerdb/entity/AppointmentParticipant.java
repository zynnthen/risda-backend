package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.appointment.ParticipantAppointmentModel;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "Temujanji_Peserta", schema = "TanamSemula")
@Data
public class AppointmentParticipant implements Serializable {

    @Id
    @Column(name = "Temujanji_P_ID")
    long temujanjiPID;

    @Column(name = "NoSiri_Temujanji")
    String noSiriTemujanji;

    @Column(name = "TSmohon_ID")
    String tsMohonID;

    @Column(name = "Kumpulan_ID")
    String kumpulanID;

    @Column(name = "Status")
    String status;

    @OneToOne
    @JoinColumn(name = "TSmohon_ID", insertable = false, updatable = false)
    TSApplication tsApplication;

//    @OneToMany(fetch = FetchType.EAGER)
//    @JoinColumn(name = "TSmohon_ID", referencedColumnName = "TSmohon_ID")
//    List<TSLejerMast> tsLejers;

    @OneToOne
    @JoinColumn(name = "Kumpulan_ID", insertable = false, updatable = false)
    KumpulanTS kumpulanTS;

    @OneToOne
    @JoinColumn(name = "NoSiri_Temujanji", insertable = false, updatable = false)
    Appointment appointment;

    public ParticipantAppointmentModel convertEntityToModel() {
        ParticipantAppointmentModel model = new ParticipantAppointmentModel();

        model.setTemujanjiPID(this.temujanjiPID);
        model.setKumpulanID(this.kumpulanID);
        model.setNoSiriTemujanji(this.noSiriTemujanji);
        model.setStatus(this.status);
        model.setTSmohonID(this.tsMohonID);

        if (this.kumpulanTS != null) {
            model.setKumpulanTS(this.kumpulanTS.convertEntityToModel());
        }

        if (this.tsApplication != null) {
            model.setTsApplication(this.tsApplication.convertEntityToModel());
        }

        return model;
    }

    public ParticipantAppointmentModel convertEntityToModel(String ansuranNo) {
        ParticipantAppointmentModel model = new ParticipantAppointmentModel();

        model.setTemujanjiPID(this.temujanjiPID);
        model.setKumpulanID(this.kumpulanID);
        model.setNoSiriTemujanji(this.noSiriTemujanji);
        model.setStatus(this.status);
        model.setTSmohonID(this.tsMohonID);

        if (this.kumpulanTS != null) {
            model.setKumpulanTS(this.kumpulanTS.convertEntityToModel());
        }

        if (this.tsApplication != null) {
            model.setTsApplication(this.tsApplication.convertEntityToModel());
        }

        List<String> tslejers = this.tsApplication.getTsLejers()
                .stream()
                .flatMap(t -> t.getChild().stream())
                .filter(c -> c.ansuranNo != null ? c.ansuranNo.equalsIgnoreCase(ansuranNo) : true)
                .map(f -> f.getKerjaStokKod().trim())
                .collect(Collectors.toList());

        model.setLejer(tslejers);

        return model;
    }

}
