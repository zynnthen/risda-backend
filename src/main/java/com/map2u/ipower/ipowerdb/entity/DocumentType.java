package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "DocumentType", schema = "users")
public class DocumentType {

    @Id
    @Column(name = "DocType")
    long docType;

    @Column(name = "DocTypeDesc")
    String docTypeDesc;

    @Column(name = "Doc_PT")
    String docPT;

    @Column(name = "AutoReset")
    String autoReset;

    @Column(name = "NumericDigits")
    Integer numericDigits;

    @Column(name = "Prefix")
    String prefix;
}
