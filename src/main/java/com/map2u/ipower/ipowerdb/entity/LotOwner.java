package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.appointment.LotOwnerModel;
import com.map2u.ipower.util.Util;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Entity
@Table(name = "Lot_diMiliki", schema = "TanamSemula")
@Data
public class LotOwner {

    @Id
    @Column(name = "LotMilik_ID")
    String lotMilikId;

    @Column(name = "PTcode")
    String ptCode;

    @Column(name = "TSmohon_ID")
    String tsMohonId;

    @Column(name = "ICNo_Pemilik")
    String icNoMilik;

    @Column(name = "NoLithosheet")
    String noLithosheet;

    @Column(name = "NoGeran")
    String noGeran;

    @Column(name = "NoGeran_Lama")
    String noGeranLama;

    @Column(name = "NoLot")
    String noLot;

    @Column(name = "NoLot_Lama")
    String noLotLama;

    @Column(name = "Bhgn_Milikan")
    Float bhgnMilikan;

    @Column(name = "Luas_Diambil", columnDefinition = "Decimal(10,2)")
    Double luasDiambil;

    @Column(name = "Tkh_Diambil")
    LocalDateTime tkhDiambil;

    @Column(name = "Syarat_Khas")
    String syaratKhas;

    @Column(name = "ButirPajak")
    String butirPajak;

    @Column(name = "Butir_Kaveat")
    String butirKaveat;

    @Column(name = "Catatan")
    String catatan;

    @Column(name = "Negeri")
    String negeri;

    @Column(name = "Daerah")
    String daerah;

    @Column(name = "Mukim")
    String mukim;

    @Column(name = "Seksyen")
    String seksyen;

    @Column(name = "Kampung")
    String kampung;

    @Column(name = "Dun")
    String dun;

    @Column(name = "Parlimen")
    String parlimen;

    @Column(name = "RePlant")
    String replant;

    @Column(name = "Syarat_Nyata")
    String syaratNyata;

    @Column(name = "TSCrop")
    String tsCrop;

    @Column(name = "TSCrop_Sub")
    String tsCropSub;

    @Column(name = "TSBulan")
    String tsBulan;

    @Column(name = "TSTahun")
    String tsTahun;

    @Column(name = "Luas_Lot")
    Double luasLot;

    @Column(name = "Luas_TS")
    Double luasTs;

    @Column(name = "Peg_Kemaskini")
    String pegKemaskini;

    @Column(name = "Tkh_KKemaskini")
    LocalDateTime tkhKKemaskini;

    @Column(name = "Carian_Rasmi")
    String carianRasmi;

    @Column(name = "Tarikh_Pecahan")
    LocalDateTime tarikhPecahan;

    @Column(name = "Sumber")
    String sumber;

    @Column(name = "Lot_UPI")
    String logUpi;

    @Column(name = "Kod_UPI")
    String kodUpi;

    @Column(name = "Taraf_Tanah")
    Long tarafTanah;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "daerah", insertable = false, updatable = false)
    UpiDaerah upiDaerah;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Parlimen", referencedColumnName = "Kod_Parlimen", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    SpParlimen spParlimen;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TSCrop", insertable = false, updatable = false)
    SpTanaman spTanaman;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns(value = {
            @JoinColumn(name = "Parlimen", referencedColumnName = "Kod_Parlimen", insertable = false, updatable = false),
            @JoinColumn(name = "Dun", referencedColumnName = "Kod_Dun2", insertable = false, updatable = false)
    })
    @NotFound(action = NotFoundAction.IGNORE)
    SpDun spDun;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Mukim", insertable = false, updatable = false)
    UpiMukim upiMukim;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Kampung", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    UpiKampung upiKampung;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Negeri", insertable = false, updatable = false)
    UpiNegeri upiNegeri;

    public LotOwnerModel convertEntityToModel() {
        LotOwnerModel model = new LotOwnerModel();

        model.setBhgnMilikan(this.bhgnMilikan);
        model.setButirKaveat(this.butirKaveat);
        model.setButirPajak(this.butirPajak);
        model.setCarianRasmi(this.carianRasmi);
        model.setCatatan(this.catatan);
        model.setIcNoMilik(this.icNoMilik);
        model.setKodUpi(this.kodUpi);
        model.setLogUpi(this.logUpi);
        model.setLotMilikId(this.lotMilikId);
        model.setLuasDiambil(this.luasDiambil);
        model.setLuasLot(this.luasLot);
        model.setLuasTs(this.luasTs);
//        model.setDun(this.dun);
//        model.setParlimen(this.parlimen);
//        model.setDaerah(this.daerah);
//        model.setKampung(this.kampung);
//        model.setMukim(this.mukim);
//        model.setNegeri(this.negeri);
//        model.setTsCrop(this.tsCrop);
        model.setNoGeran(this.noGeran);
        model.setNoGeranLama(this.noGeranLama);
        model.setNoLithosheet(this.noLithosheet);
        model.setNoLot(this.noLot);
        model.setNoLotLama(this.noLotLama);
        model.setPegKemaskini(this.pegKemaskini);
        model.setPtCode(this.ptCode);
        model.setReplant(this.replant);
        model.setSeksyen(this.seksyen);
        model.setSumber(this.sumber);
        model.setSyaratKhas(this.syaratKhas);
        model.setSyaratNyata(this.syaratNyata);
        model.setTarafTanah(this.tarafTanah);
        model.setTarikhPecahan(Util.convertDateTimeToMiliSeconds(this.tarikhPecahan));
        model.setTkhDiambil(Util.convertDateTimeToMiliSeconds(this.tkhDiambil));
        model.setTkhKKemaskini(Util.convertDateTimeToMiliSeconds(this.tkhKKemaskini));
        model.setTsBulan(this.tsBulan);
        model.setTsCropSub(this.tsCropSub);
        model.setTsMohonId(this.tsMohonId);
        model.setTsTahun(this.tsTahun);

        if (this.upiDaerah != null) {
            model.setDaerah(this.upiDaerah.getDaerah());
        }

        if (this.spParlimen != null) {
            model.setParlimen(this.spParlimen.getParlimen());
        }

        if (this.spDun != null) {
            model.setDun(this.spDun.getDun());
        }

        if (this.upiMukim != null) {
            model.setMukim(this.upiMukim.getMukim());
        }

        if (this.upiKampung != null) {
            model.setKampung(this.upiKampung.getKampung());
        }

        if (this.upiNegeri != null) {
            model.setNegeri(this.upiNegeri.getNegeri());
        }

        if (this.spTanaman != null) {
            model.setTsCrop(this.spTanaman.getTanaman());
        }

        return model;
    }
}
