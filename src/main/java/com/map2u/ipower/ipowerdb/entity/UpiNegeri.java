package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "upi_Negeri", schema = "UtilityDict")
@Data
public class UpiNegeri {

    @Id
    @Column(name = "U_Negeri_ID")
    String negeriId;

    @Column(name = "Negeri")
    String negeri;
}
