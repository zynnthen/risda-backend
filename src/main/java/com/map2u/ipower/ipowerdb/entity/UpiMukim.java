package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "upi_Mukim", schema = "UtilityDict")
@Data
public class UpiMukim {

    @Id
    @Column(name = "U_Mukim_ID")
    String mukimId;

    @Column(name = "Mukim")
    String mukim;
}
