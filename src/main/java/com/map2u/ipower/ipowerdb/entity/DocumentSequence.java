package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "DocumentSequenceNo", schema = "users")
public class DocumentSequence {

    @Id
    @Column(name = "DocTypeID")
    long docTypeId;

    @Column(name = "CurrentRunningNo")
    long currentRunningNo;

    @Column(name = "kod_pt")
    String ptCode;

    @Column(name = "year")
    String year;

    @Column(name = "cutOff_Date")
    LocalDateTime cutOffDate;

    @Column(name = "DocType")
    long docType;

    @OneToOne
    @JoinColumn(name = "DocType",insertable = false,updatable = false)
    DocumentType documentType;
}
