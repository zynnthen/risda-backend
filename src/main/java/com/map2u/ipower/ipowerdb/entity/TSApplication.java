package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.appointment.LotOwnerModel;
import com.map2u.ipower.model.appointment.TSApplicationModel;
import com.map2u.ipower.util.Util;
import lombok.Data;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "PermohonanTS", schema = "TanamSemula")
@Data
public class TSApplication {

    @Column(name = "PTcode")
    String ptCode;

    @Column(name = "TSmohon_No")
    String tsMohonNo;

    @Column(name = "TSJenis")
    String tsJenis;

    @Column(name = "Pemilik_Lot")
    String pemilikLot;

    @Column(name = "ID_eSPek")
    String idESpek;

    @Column(name = "ICNo_Pmohon")
    String icNoPemohon;

    @Column(name = "Nama_Pmohon")
    String namaPemohon;

    @Column(name = "DOB_Pmohon")
    LocalDateTime DOB_Pmohon;

    @Column(name = "SEX_Pmohon")
    String sexPemohon;

    @Column(name = "RACE_Pmohon")
    String racePemohon;

    @Column(name = "Address1")
    String address1;

    @Column(name = "Address2")
    String address2;

    @Column(name = "Address3")
    String address3;

    @Column(name = "Poskod")
    String poskod;

    @Column(name = "Bandar")
    String bandar;

    @Column(name = "Negeri")
    String negeri;

    @Column(name = "Daerah")
    String daerah;

    @Column(name = "Mukim")
    String mukim;

    @Column(name = "Seksyen")
    String seksyen;

    @Column(name = "Kampung")
    String kampung;

    @Column(name = "Dun")
    String dun;

    @Column(name = "Parlimen")
    String parlimen;

    @Column(name = "NoPhone_Mobile")
    String noPhoneMobile;

    @Column(name = "NoPhone_House")
    String noPhoneHouse;

    @Column(name = "NoPhone_Office")
    String noPhoneOffice;

    @Column(name = "Email")
    String email;

    @Column(name = "Bank_AccNo")
    String bankAccNo;

    @Column(name = "Bank_Name")
    String bankName;

    @Column(name = "Bank_Scode")
    String bankSCode;

    @Column(name = "TSPendekatan")
    String tsPendekatan;

    @Column(name = "TSAgensi")
    String tsAgensi;

    @Column(name = "TSAktiviti")
    String tsAktiviti;

    @Column(name = "TSProgram")
    String tsProgram;

    @Column(name = "TS_RMK")
    String tsRMK;

    @Column(name = "Asal_Pmohon")
    String asalPemohon;

    @Column(name = "No_SMB_TSO")
    String noSmbTso;

    @Column(name = "Tkh_Terima")
    LocalDateTime tkhTerima;

    @Column(name = "Tkh_Lulus")
    LocalDateTime tkhLulus;

    @Column(name = "Peg_lulus")
    String pegLulus;

    @Column(name = "Ptrima_Bantuan")
    String ptrimaBantuan;

    @Column(name = "Status_Pmohon")
    String statusPmohon;

    @Column(name = "Status_Alasan")
    String statusAlasan;

    @Column(name = "Status_Aktif")
    String statusAktif;

    @Column(name = "Status_PerluTS18")
    String statusPerluTS18;

    @Column(name = "KodPA_Semasa")
    String kodPASemasa;

    @Column(name = "KodObj_STunai")
    String kodObjSTunai;

    @Column(name = "KodObj_SStok")
    String kodObjSStok;

    @Column(name = "KodPA_Berturut")
    String kodPaBerturut;

    @Column(name = "KodObj_BTunai")
    String kodObjBTunai;

    @Column(name = "KodObj_BStok")
    String kodObjBStok;

    @Column(name = "Peg_Kemaskini")
    String pegKemaskini;

    @Column(name = "Tkh_Kemaskini")
    LocalDateTime tkhKemaskini;

    @Column(name = "NoSiriTS")
    long noSiriTs;

    @Column(name = "DtSiriTS")
    String dtSiriTs;

    @Column(name = "Warganegara")
    String warganegara;

    @Column(name = "BayarKepada")
    String bayarKepada;

    @Column(name = "SWK_Status")
    String swkStatus;

    @Column(name = "SWK_Kod")
    String swkKod;

    @Column(name = "TSKategori")
    String tsKategori;

    @Column(name = "Kumpulan_ID")
    String kumpulanId;

    @Id
    @Column(name = "TSmohon_ID")
    String tsMohonId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "daerah", insertable = false, updatable = false)
    UpiDaerah upiDaerah;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Parlimen", referencedColumnName = "Kod_Parlimen", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    SpParlimen spParlimen;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns(value = {
            @JoinColumn(name = "Parlimen", referencedColumnName = "Kod_Parlimen", insertable = false, updatable = false),
            @JoinColumn(name = "Dun", referencedColumnName = "Kod_Dun2", insertable = false, updatable = false)
    })
    SpDun spDun;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Mukim", insertable = false, updatable = false)
    UpiMukim upiMukim;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Kampung", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    UpiKampung upiKampung;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "Negeri", insertable = false, updatable = false)
    UpiNegeri upiNegeri;

    @OneToMany(mappedBy = "tsMohonId", fetch = FetchType.EAGER)
    Set<LotOwner> lotOwners;

    @OneToMany(mappedBy = "tsMohonId")
    @LazyCollection(LazyCollectionOption.FALSE)
    List<TSLejerMast> tsLejers;

    public TSApplicationModel convertEntityToModel() {
        TSApplicationModel model = new TSApplicationModel();

        model.setAddress1(this.address1);
        model.setAddress2(this.address2);
        model.setAddress3(this.address3);
        model.setAsalPemohon(this.asalPemohon);
        model.setBandar(this.bandar);
        model.setBankAccNo(this.bankAccNo);
        model.setBankName(this.bankName);
        model.setBankSCode(this.bankSCode);
        model.setBayarKepada(this.bayarKepada);
        model.setDOB_Pmohon(Util.convertDateTimeToMiliSeconds(this.DOB_Pmohon));

        model.setDtSiriTs(this.dtSiriTs);
        model.setEmail(this.email);
        model.setIcNoPemohon(this.icNoPemohon);
        model.setIdESpek(this.idESpek);
        model.setKodObjBStok(this.kodObjBStok);
        model.setKodObjBTunai(this.kodObjBTunai);
        model.setKodObjSStok(this.kodObjSStok);
        model.setKodObjSTunai(this.kodObjSTunai);
        model.setKodPaBerturut(this.kodPaBerturut);
        model.setKodPASemasa(this.kodPASemasa);
        model.setKumpulanId(this.kumpulanId);
        model.setNamaPemohon(this.namaPemohon);
        model.setDun(this.dun);
        model.setParlimen(this.parlimen);
        model.setDaerah(this.daerah);
        model.setKampung(this.kampung);
        model.setNegeri(this.negeri);
        model.setMukim(this.mukim);
        model.setNoPhoneHouse(this.noPhoneHouse);
        model.setNoPhoneMobile(this.noPhoneMobile);
        model.setNoPhoneOffice(this.noPhoneOffice);
        model.setNoSiriTs(this.noSiriTs);
        model.setNoSmbTso(this.noSmbTso);
        model.setPegKemaskini(this.pegKemaskini);
        model.setPegLulus(this.pegLulus);
        model.setPemilikLot(this.pemilikLot);
        model.setPoskod(this.poskod);
        model.setPtCode(this.ptCode);
        model.setPtrimaBantuan(this.ptrimaBantuan);
        model.setRacePemohon(this.racePemohon);
        model.setSeksyen(this.seksyen);
        model.setSexPemohon(this.sexPemohon);
        model.setStatusAktif(this.statusAktif);
        model.setStatusAlasan(this.statusAlasan);
        model.setStatusPerluTS18(this.statusPerluTS18);
        model.setStatusPmohon(this.statusPmohon);
        model.setSwkKod(this.swkKod);
        model.setSwkStatus(this.swkStatus);
        model.setTkhKemaskini(Util.convertDateTimeToMiliSeconds(this.tkhKemaskini));
        model.setTkhLulus(Util.convertDateTimeToMiliSeconds(this.tkhLulus));
        model.setTkhTerima(Util.convertDateTimeToMiliSeconds(this.tkhTerima));

        model.setTsAgensi(this.tsAgensi);
        model.setTsAktiviti(this.tsAktiviti);
        model.setTsJenis(this.tsJenis);
        model.setTsKategori(this.tsKategori);
        model.setTsMohonId(this.tsMohonId);
        model.setTsMohonNo(this.tsMohonNo);
        model.setTsPendekatan(this.tsPendekatan);
        model.setTsProgram(this.tsProgram);
        model.setTsRMK(this.tsRMK);
        model.setWarganegara(this.warganegara);

        List<LotOwnerModel> lotOwnerModelList = this.lotOwners.stream().map(l -> l.convertEntityToModel()).collect(Collectors.toList());

        model.setLotDimiliki(lotOwnerModelList);

        if (this.upiDaerah != null) {
            model.setDaerah(this.upiDaerah.getDaerah());
        }

        if (this.spParlimen != null) {
            model.setParlimen(this.spParlimen.getParlimen());
        }

        if (this.spDun != null) {
            model.setDun(this.spDun.getDun());
        }

        if (this.upiMukim != null) {
            model.setMukim(this.upiMukim.getMukim());
        }

        if (this.upiKampung != null) {
            model.setKampung(this.upiKampung.getKampung());
        }

        if (this.upiNegeri != null) {
            model.setNegeri(this.upiNegeri.getNegeri());
        }

        return model;
    }
}
