package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.appointment.TSLejerChildModel;
import com.map2u.ipower.model.appointment.TSLejerModel;
import com.map2u.ipower.util.Util;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "TSLejer_Mast", schema = "TanamSemula")
@Data
public class TSLejerMast {

    @Column(name = "PTcode")
    String ptCode;

    @Id
    @Column(name = "NoLejer")
    String noLejer;

    @Column(name = "TkhLejer")
    LocalDateTime tkhLejer;

    @Column(name = "TSmohon_ID")
    String tsMohonId;

    @Column(name = "NoRisda7")
    String noRisda7;

    @Column(name = "Amaun_Jumlah")
    Double amaunJumlah;

    @Column(name = "Peg_Kemaskini")
    String pegKemaskini;

    @Column(name = "Tkh_Kemaskini")
    LocalDateTime tkhKemaskini;

    @Column(name = "Rujukan")
    String rujukan;

    @Column(name = "RPKmohon_ID")
    String rpkMohonId;

    @OneToMany(mappedBy = "tsLejerMast", fetch = FetchType.EAGER)
    List<TSLejerChild> child;

//    @OneToOne
//    @JoinColumn(name = "TSmohon_ID", insertable = false, updatable = false)
//    TSApplication tsApplication;

    public TSLejerModel convertEntityToModel() {
        TSLejerModel model = new TSLejerModel();

        model.setAmaunJumlah(this.amaunJumlah);
        model.setNoLejer(this.noLejer);
        model.setRujukan(this.rujukan);
        model.setNoRisda7(this.noRisda7);
        model.setPegKemaskini(this.pegKemaskini);
        model.setPtCode(this.ptCode);
        model.setTkhKemaskini(Util.convertDateTimeToMiliSeconds(this.tkhKemaskini));
        model.setTkhLejer(Util.convertDateTimeToMiliSeconds(this.tkhLejer));
        model.setRpkMohonId(this.rpkMohonId);

        if (this.child != null) {
            List<TSLejerChildModel> childModels = this.child.stream()
                    .map(t -> t.convertEntityToModel()).collect(Collectors.toList());
            model.setChild(childModels);
        }

        return model;
    }
}
