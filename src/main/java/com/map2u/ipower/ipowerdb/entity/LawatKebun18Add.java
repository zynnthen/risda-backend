package com.map2u.ipower.ipowerdb.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;


@Entity
@Table(name = "LawatKebun18_Add", schema = "TanamSemula")
@Data
public class LawatKebun18Add {

    @Id
    @Column(name = "NoSiri_LK")
    String noSiriLK;

    @Lob
    @Column(name = "sign_pegawai")
    String signPegawai;

    @Lob
    @Column(name = "sign_perakuan")
    String signPerakuan;

    @Column(name = "nama_perakuan")
    String namaPerakuan;

    @Column(name = "no_kp_perakuan")
    String noKpPerakuan;

    @Column(name = "hadir")
    String hadir;

    @Column(name = "latitude")
    Double latitude;

    @Column(name = "longitude")
    Double longitude;

    @OneToOne
    @JoinColumn(name = "NoSiri_LK", updatable = false, insertable = false)
    LawatKebun18 form;
}
