package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.FarmBudgetChildModel;
import com.map2u.ipower.model.FarmBudgetModel;
import com.map2u.ipower.model.appointment.FarmBudgetItem;
import com.map2u.ipower.util.Util;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "FarmBudget_Mast", schema = "TanamSemula")
@Data
public class FarmBudgetMast {

    @Id
    @Column(name = "NoSiri_FBM")
    String noSiriFBM;

    @Column(name = "PTCode")
    String ptCode;

    @Column(name = "Tujuan_FBM")
    String tujuanFBM;

    @Column(name = "NoTSB_RPK")
    String noTSBRPK;

    @Column(name = "Jumlah_Kos")
    Double jumlahKos;

    @Column(name = "Status")
    String status;

    @Column(name = "NoPindaan")
    String noPindaan;

    @Column(name = "Peg_Input")
    String pegInput;

    @Column(name = "Tkh_Input")
    LocalDateTime tkhInput;

    @Column(name = "Peg_Lulus")
    String pegLulus;

    @Column(name = "Tkh_Lulus")
    LocalDateTime tkhLulus;

    @OneToMany(mappedBy = "farmBudgetMast", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    List<FarmBudgetChild> child;


    public FarmBudgetModel convertEntityToModel() {
        FarmBudgetModel model = new FarmBudgetModel();

        model.setJumlahKos(this.jumlahKos);
        model.setNoPindaan(this.noPindaan);
        model.setNoSiriFBM(this.noSiriFBM);
        model.setNoTSBRPK(this.noTSBRPK);
        model.setPegInput(this.pegInput);
        model.setPegLulus(this.pegLulus);
        model.setPtCode(this.ptCode);
        model.setStatus(this.status);
        model.setTkhInput(Util.convertDateTimeToMiliSeconds(this.tkhInput));
        model.setTkhLulus(Util.convertDateTimeToMiliSeconds(this.tkhLulus));
        model.setTujuanFBM(this.tujuanFBM);

        if (this.child != null) {
            List<FarmBudgetChildModel> childModels = this.child.stream()
                    .map(e -> e.convertEntityToModel())
                    .collect(Collectors.toList());

            model.setChild(childModels);
        }

        return model;
    }

    public FarmBudgetModel convertEntityToModel(String ansuranNo) {
        if (ansuranNo == null) {
            return convertEntityToModel();
        }

        FarmBudgetModel model = new FarmBudgetModel();

        model.setJumlahKos(this.jumlahKos);
        model.setNoPindaan(this.noPindaan);
        model.setNoSiriFBM(this.noSiriFBM);
        model.setNoTSBRPK(this.noTSBRPK);
        model.setPegInput(this.pegInput);
        model.setPegLulus(this.pegLulus);
        model.setPtCode(this.ptCode);
        model.setStatus(this.status);
        model.setTkhInput(Util.convertDateTimeToMiliSeconds(this.tkhInput));
        model.setTkhLulus(Util.convertDateTimeToMiliSeconds(this.tkhLulus));
        model.setTujuanFBM(this.tujuanFBM);

        if (this.child != null) {
            List<FarmBudgetChildModel> childModels = this.child.stream()
                    .filter(e -> e.getAnsuranNo().equalsIgnoreCase(ansuranNo))
                    .map(e -> e.convertEntityToModel())
                    .collect(Collectors.toList());

            model.setChild(childModels);
        }

        return model;
    }

    public List<FarmBudgetItem> getFarmBudgetItems(String ansuranNo) {
        if (this.child != null) {
            List<FarmBudgetItem> farmBudgetItems = this.child.stream()
                    .filter(e -> ansuranNo != null ? e.getAnsuranNo().equalsIgnoreCase(ansuranNo) : true)
                    .map(f -> {
                        FarmBudgetItem item = new FarmBudgetItem();
                        item.setAnsuranNo(f.getAnsuranNo());
                        item.setKatKerja(f.getKatKerja());
                        item.setKerjaStokKod(f.getKerjaStokKod());
                        item.setPtCode(f.getPtCode());
                        item.setStokItem(f.getStokItem());
//                        if (f.getStok() != null) {
//                            item.setJenisStok(f.getStok().getJenisStok());
//                            item.setPerihalStokItem(f.getStok().getPerihalStokItem());
//                            item.setUkuran(f.getStok().getUkuran());
//                        }
//
//                        if (f.getKerja() != null) {
//                            item.setKodKerjaId(f.getKerja().getKodKerjaId());
//                            item.setKodKerjaDesc(f.getKerja().getKodKerjaDesc());
//                        }

                        return item;
                    })
                    .collect(Collectors.toList());

            return farmBudgetItems;
        }

        return null;
    }

}
