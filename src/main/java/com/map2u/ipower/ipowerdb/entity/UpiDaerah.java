package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "upi_Daerah", schema = "UtilityDict")
@Data
public class UpiDaerah {

    @Id
    @Column(name = "U_Daerah_ID")
    String daerahId;

    @Column(name = "Daerah")
    String daerah;

//    @Column(name = "Kod_Daerah")
//    Integer kodDaerah;
//
//    @Column(name = "U_Negeri_ID")
//    String negeriId;
//
//    @Column(name = "Kod_Negeri")
//    Integer kodNegeri;
//
//    @Column(name = "Negeri_SMB")
//    String negeriSMB;
//
//    @Column(name = "Daerah_SMB")
//    String daerahSMB;
//
//    @Column(name = "Daerah_Keterangan_SMB")
//    String daerahKeterangan;
//
//    @Column(name = "status")
//    Integer status;

}
