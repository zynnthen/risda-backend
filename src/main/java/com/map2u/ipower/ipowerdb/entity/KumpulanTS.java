package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.appointment.KumpulanTSModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "KumpulanTS", schema = "TanamSemula")
@Data
public class KumpulanTS {

    @Id
    @Column(name = "Kumpulan_ID")
    String kumpulanId;

    @Column(name = "Nama_Kumpulan")
    String namaKumpulan;

    public KumpulanTSModel convertEntityToModel() {
        KumpulanTSModel model = new KumpulanTSModel();

        model.setKumpulanId(this.kumpulanId);
        model.setNamaKumpulan(this.namaKumpulan);

        return model;
    }
}
