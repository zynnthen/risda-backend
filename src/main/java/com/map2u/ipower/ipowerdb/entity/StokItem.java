package com.map2u.ipower.ipowerdb.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "DC_StokItem", schema = "UtilityDict")
@Data
public class StokItem {

    @Id
    @Column(name = "KodStok_Item")
    String kodStokItem;

    @Column(name = "JenisStok")
    String jenisStok;

    @Column(name = "PerihalStok_Item")
    String perihalStokItem;

    @Column(name = "Ukuran")
    String ukuran;

    @Column(name = "TSCrop")
    String tsCrop;

    @Column(name = "SrtRujuk_Efektif")
    String srtRujukEfektif;

    @Column(name = "Tkh_Efektif")
    LocalDateTime tkhEfektif;

    @Column(name = "StatusStok")
    String statusStok;

}
