package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "LawatBayar18A_Add", schema = "TanamSemula")
@Data
public class LawatBayar18AAdd {

    @Id
    @Column(name = "NoSiri_LB")
    String noSiriLB;

    @Lob
    @Column(name = "sign_pegawai")
    String signPegawai;

    @Lob
    @Column(name = "sign_perakuan")
    String signPerakuan;

    @Column(name = "nama_perakuan")
    String namaPerakuan;

    @Column(name = "no_kp_perakuan")
    String noKpPerakuan;

    @Column(name = "hadir")
    String hadir;

    @Column(name = "latitude")
    Double latitude;

    @Column(name = "longitude")
    Double longitude;

    @OneToOne
    @JoinColumn(name = "NoSiri_LB", updatable = false, insertable = false)
    LawatBayar18A form;

}
