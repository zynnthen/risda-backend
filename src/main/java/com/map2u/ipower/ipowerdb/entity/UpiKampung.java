package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "upi_Kampung", schema = "UtilityDict")
@Data
public class UpiKampung {

    @Id
    @Column(name = "U_Kampung_ID")
    String kampungId;

    @Column(name = "Kampung")
    String kampung;
}
