package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.model.form.LawatBayar18AAktivitiModel;
import com.map2u.ipower.model.form.LawatBayar18AModel;
import com.map2u.ipower.util.Util;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "LawatBayar18A", schema = "TanamSemula")
@Data
public class LawatBayar18A {

    @Column(name = "PTcode")
    String ptCode;

    @Id
    @Column(name = "NoSiri_LB")
    String noSiriLB;

    @Column(name = "NoSiri_Temujanji")
    String noSiriTemujanji;

    @Column(name = "Jenis_LK")
    String jenisLK;

    @Column(name = "ModulOwner")
    String modulOwner;

    @Column(name = "TSmohon_ID")
    String tsMohonId;

    @Column(name = "TSmohon_LBSiri")
    String tsMohonLBSiri;

    @Column(name = "Ansuran_No")
    String ansuranNo;

    @Column(name = "Perimeter")
    String perimeter;

    @Column(name = "Tebang_Jentera1")
    Double tebangJentera1;

    @Column(name = "Tebang_Jentera2")
    Double tebangJentera2;

    @Column(name = "Jarak_Lubang1")
    Double jarakLubang1;

    @Column(name = "Jarak_Lubang2")
    Double jarakLubang2;

    @Column(name = "Jarak_Lubang3")
    Double jarakLubang3;

    @Column(name = "Luas_Tanam")
    Double luasTanam;

    @Column(name = "Tkh_Tanam")
    LocalDateTime tkhTanam;

    @Column(name = "Jenis_Benih")
    String jenisBenih;

    @Column(name = "Klon_Benih")
    String klonBenih;

    @Column(name = "Benih_Luar")
    String benihLuar;

    @Column(name = "Sah_Klon")
    String sahKlon;

    @Column(name = "GAP_Senggara")
    String gapSenggara;

    @Column(name = "GAP_Baja")
    String gapBaja;

    @Column(name = "GAP_Cantasan")
    String gapCantasan;

    @Column(name = "Jenis_Cantasan")
    String jenisCantasan;

    @Column(name = "GAP_GalakDahan")
    String gapGalakDahan;

    @Column(name = "Jenis_GalakDahan")
    String jenisGalakDahan;

    @Column(name = "KPB_Kontan")
    String kpbKontan;

    @Column(name = "KPB_Kontan_Selenggara")
    String kpbKontanSelenggara;

    @Column(name = "Getah_PkkHek")
    Integer getahPkkHek;

    @Column(name = "TL_PkkHidup")
    Integer tlPkkHidup;

    @Column(name = "Sulaman")
    Integer sulaman;

    @Column(name = "Ukur_Lilit")
    Double ukurLilit;

    @Column(name = "Kawal_Perosak")
    String kawalPerosak;

    @Column(name = "Rumpai_Senggara")
    Double rumpaiSenggara;

    @Column(name = "Rumpai_TdkSenggara")
    Double rumpaiTdkSenggara;

    @Column(name = "Tkh_Berhasil")
    LocalDateTime tkhBerhasil;

    @Column(name = "Kenyataan_Lain")
    String kenyataanLain;

    @Column(name = "Peg_Pemeriksa")
    String pegPemeriksa;

    @Column(name = "No_KP_PPK")
    String noKpPpk;

    @Column(name = "Jaw_Pemeriksa")
    String jawPemeriksa;

    @Column(name = "Tkh_Lawat")
    LocalDateTime tkhLawat;

    @Column(name = "MLawat_Mula")
    @Temporal(TemporalType.TIME)
    Date mLawatMula;


    @Column(name = "MLawat_Tamat")
    @Temporal(TemporalType.TIME)
    Date mLawatTamat;

    @Column(name = "Peg_Kemaskini")
    String pegKemaskini;

    @Column(name = "Tkh_Kemaskini")
    LocalDateTime tkhKemaskini;

    @Column(name = "Status_Lulus")
    String statusLulus;

    @Column(name = "Peg_Lulus")
    String pegLulus;

    @Column(name = "Tkh_Lulus")
    LocalDateTime tkhLulus;

    @Column(name = "Bil_Lawatan")
    Integer bilLawatan;

    @Column(name = "PlanTanah")
    String planTanah;

    @Column(name = "HadirSendiri")
    String hadirSendiri;

    @Column(name = "LuasBerjaya")
    Double luasBerjaya;

    @Column(name = "Status_TolakKIV")
    String statusTolakKIV;

    @Column(name = "Catatan")
    String catatan;

    @Column(name = "Kumpulan_ID")
    String kumpulanId;

    @Column(name = "Kump_NoClaim")
    String kumpNoClaim;

    @Column(name = "Jarak_Tanaman")
    String jarakTanaman;

    @Column(name = "Bekal_Awal")
    String bekalAwal;

    @Column(name = "Status_R7")
    Character statusR7;

    @Column(name = "Klon_Mutu")
    String klonMutu;

    @Column(name = "Resit_Benih")
    String resitBenih;

    @Column(name = "Tapak_Semaian")
    String tapakSemaian;

    @Column(name = "Benih_Sawit")
    String benihSawit;

    @Column(name = "Luas_Kontan")
    Double luasKontan;

    @Column(name="Smartmap_link")
    String smartmapLink;

    @OneToMany(mappedBy = "lawatBayar18A", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    List<LawatBayar18AAktiviti> activities;

    @ManyToOne
    @JoinColumn(name = "TSmohon_ID", insertable = false, updatable = false)
    TSApplication tsApplication;

    @ManyToOne
    @JoinColumn(name = "Kumpulan_ID", insertable = false, updatable = false)
    KumpulanTS kumpulanTS;

    @OneToOne(mappedBy = "form", cascade = CascadeType.ALL)
    LawatBayar18AAdd additional;

    @PrePersist
    @PreUpdate
    public void updateTkhKemaskini() {
        this.tkhKemaskini = Util.getCurrentTime();
    }

    public LawatBayar18AModel convertEntityToModel() {
        LawatBayar18AModel model = new LawatBayar18AModel();

        model.setAnsuranNo(this.ansuranNo);
        model.setBekalAwal(this.bekalAwal);
        model.setBenihLuar(this.benihLuar);
        model.setBenihSawit(this.benihSawit);
        model.setBilLawatan(this.bilLawatan);
        model.setCatatan(this.catatan);
        model.setGapBaja(this.gapBaja);
        model.setGapCantasan(this.gapCantasan);
        model.setGapGalakDahan(this.gapGalakDahan);
        model.setGapSenggara(this.gapSenggara);
        model.setGetahPkkHek(this.getahPkkHek);
        model.setHadirSendiri(this.hadirSendiri);
        model.setJarakLubang1(this.jarakLubang1);
        model.setJarakLubang2(this.jarakLubang2);
        model.setJarakLubang3(this.jarakLubang3);
        model.setJarakTanaman(this.jarakTanaman);
        model.setJawPemeriksa(this.jawPemeriksa);
        model.setJenisBenih(this.jenisBenih);
        model.setJenisCantasan(this.jenisCantasan);
        model.setJenisGalakDahan(this.jenisGalakDahan);
        model.setJenisLK(this.jenisLK);
        model.setKawalPerosak(this.kawalPerosak);
        model.setKenyataanLain(this.kenyataanLain);
        model.setKlonBenih(this.klonBenih);
        model.setKlonMutu(this.klonMutu);
        model.setKpbKontan(this.kpbKontan);
        model.setKpbKontanSelenggara(this.kpbKontanSelenggara);
        model.setKumpNoClaim(this.kumpNoClaim);
        model.setKumpulanId(this.kumpulanId);
        model.setLuasKontan(this.luasKontan);
        model.setLuasBerjaya(this.luasBerjaya);
        model.setLuasTanam(this.luasTanam);
        model.setMLawatMula(Util.convertTimeToMillisecond(this.mLawatMula));
        model.setMLawatTamat(Util.convertTimeToMillisecond(this.mLawatTamat));
        model.setModulOwner(this.modulOwner);
        model.setNoKpPpk(this.noKpPpk);
        model.setNoSiriLB(this.noSiriLB);
        model.setNoSiriTemujanji(this.noSiriTemujanji);
        model.setPegKemaskini(this.pegKemaskini);
        model.setPegLulus(this.pegLulus);
        model.setPegPemeriksa(this.pegPemeriksa);
        model.setPerimeter(this.perimeter);
        model.setPlanTanah(this.planTanah);
        model.setPtCode(this.ptCode);
        model.setResitBenih(this.resitBenih);
        model.setRumpaiSenggara(this.rumpaiSenggara);
        model.setRumpaiTdkSenggara(this.rumpaiTdkSenggara);
        model.setSahKlon(this.sahKlon);
        model.setStatusLulus(this.statusLulus);
        model.setStatusR7(this.statusR7);
        model.setStatusTolakKIV(this.statusTolakKIV);
        model.setSulaman(this.sulaman);
        model.setTapakSemaian(this.tapakSemaian);
        model.setTebangJentera1(this.tebangJentera1);
        model.setTebangJentera2(this.tebangJentera2);
        model.setTkhBerhasil(Util.convertDateTimeToMiliSeconds(this.tkhBerhasil));
        model.setTkhKemaskini(Util.convertDateTimeToMiliSeconds(this.tkhKemaskini));
        model.setTkhLawat(Util.convertDateTimeToMiliSeconds(this.tkhLawat));
        model.setTkhLulus(Util.convertDateTimeToMiliSeconds(this.tkhLulus));
        model.setTkhTanam(Util.convertDateTimeToMiliSeconds(this.tkhTanam));
        model.setTlPkkHidup(this.tlPkkHidup);
        model.setTsMohonId(this.tsMohonId);
        model.setTsMohonLBSiri(this.tsMohonLBSiri);
        model.setUkurLilit(this.ukurLilit);
        model.setSmartmapLink(this.smartmapLink);

        if (this.additional != null) {
            model.setSignPegawai(this.additional.getSignPegawai());
            model.setSignPerakuan(this.additional.getSignPerakuan());
            model.setNamaPerakuan(this.additional.getNamaPerakuan());
            model.setNoKpPerakuan(this.additional.getNoKpPerakuan());
            model.setHadir(this.additional.getHadir());
            model.setLatitude(this.additional.getLatitude());
            model.setLongitude(this.additional.getLongitude());
//            System.out.println("No Siri LB:" + this.additional.getNoSiriLB());
        }

        if (this.kumpulanTS != null) {
            model.setNamaKumpulan(this.kumpulanTS.getNamaKumpulan());
        }

        if (this.activities != null) {

            List<LawatBayar18AAktivitiModel> activitiesModel = this.activities.stream()
                    .map(a -> a.convertEntityToModel())
                    .collect(Collectors.toList());
            model.setActivities(activitiesModel);
        }

        model.setTsApplication(this.tsApplication.convertEntityToModel());

        return model;
    }
}
