package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SP_Dun", schema = "UtilityDict")
@Data
public class SpDun implements Serializable {

    @Id
    @Column(name = "U_Dun_ID")
    String dunId;

    @Column(name = "Dun")
    String dun;

    @Column(name = "Kod_Parlimen")
    String kodParlimen;

    @Column(name = "Kod_Dun2")
    String kodDun2;
}
