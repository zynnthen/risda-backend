package com.map2u.ipower.ipowerdb.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "DC_KodKerja", schema = "UtilityDict")
@Data
public class KodKerja {

    @Id
    @Column(name = "KodKerja_ID")
    String kodKerjaId;

    @Column(name = "KodKerja_Desc")
    String kodKerjaDesc;

    @Column(name = "KodKerja_Status")
    Character kodKerjaStatus;

    @Column(name = "Peg_Kemaskini")
    String pegKemaskini;

    @Column(name = "Tkh_Kemaskini")
    LocalDateTime tkhKemaskini;
}
