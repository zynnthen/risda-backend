package com.map2u.ipower.ipowerdb.entity;

import com.map2u.ipower.config.converter.KatKerjaDBValueConverter;
import com.map2u.ipower.enums.KatKerja;
import com.map2u.ipower.model.form.LawatBayar18AAktivitiModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "LawatBayar18A_Aktiviti", schema = "TanamSemula")
@Data
public class LawatBayar18AAktiviti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    long id;

    @Column(name = "NoSiri_Temujanji")
    String noSiriTemujanji;

    @Column(name = "NoSiri_LB")
    String noSiriLB;

    @Column(name = "KatKerja")
    @Convert(converter = KatKerjaDBValueConverter.class)
    KatKerja katKerja;

    @Column(name = "KerjaStok_Kod")
    String kerjaStokKod;

    @Column(name = "Stok_Item")
    String stokItem;

    @Column(name = "Status")
    Integer status;

    @Column(name = "Jenis_Caj")
    String jenisCaj;

    @OneToOne
    @JoinColumn(name = "NoSiri_LB", insertable = false, updatable = false)
    LawatBayar18A lawatBayar18A;

    public LawatBayar18AAktivitiModel convertEntityToModel() {
        LawatBayar18AAktivitiModel model = new LawatBayar18AAktivitiModel();

        model.setId(this.id);
        if (this.jenisCaj != null) {
            model.setJenisCaj(Integer.parseInt(this.jenisCaj));
        }
        model.setKatKerja(this.katKerja);
        model.setKerjaStokKod(this.kerjaStokKod);
        model.setNoSiriLB(this.noSiriLB);
        model.setNoSiriTemujanji(this.noSiriTemujanji);
        model.setStatus(this.status);
        model.setStokItem(this.stokItem);
        return model;
    }
}
