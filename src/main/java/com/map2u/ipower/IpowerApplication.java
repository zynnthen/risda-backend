package com.map2u.ipower;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableSpringDataWebSupport
public class IpowerApplication {

	public static void main(String[] args) {
		SpringApplication.run(IpowerApplication.class, args);
	}

}
