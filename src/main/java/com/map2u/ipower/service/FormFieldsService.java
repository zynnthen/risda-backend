package com.map2u.ipower.service;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18A;
import com.map2u.ipower.ipowerdb.entity.LawatBayar18AAdd;
import com.map2u.ipower.ipowerdb.entity.LawatKebun18;
import com.map2u.ipower.ipowerdb.entity.LawatKebun18Add;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.Column;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
@Slf4j
public class FormFieldsService {

    List<String> ts18AFields;

    List<String> ts18Fields;


    public FormFieldsService() {
    }

    @PostConstruct
    public void initialize() {
        log.info("Initialize form fields");
        initializeTS18A();
        initializeTS18();
    }

    public void initializeTS18() {
        this.ts18Fields = Arrays.stream(LawatKebun18.class.getDeclaredFields())
                .filter(f -> f.getAnnotation(Column.class) != null)
                .map(f -> {
                    Column column = f.getAnnotation(Column.class);
                    return column.name();
                }).collect(Collectors.toList());

        List<String> ts18AddFields = Arrays.stream(LawatKebun18Add.class.getDeclaredFields())
                .filter(f -> f.getAnnotation(Column.class) != null)
                .map(f -> {
                    Column column = f.getAnnotation(Column.class);
                    return column.name();
                }).collect(Collectors.toList());

        this.ts18Fields.addAll(ts18AddFields);
    }

    public void initializeTS18A() {
        this.ts18AFields = Arrays.stream(LawatBayar18A.class.getDeclaredFields())
                .filter(f -> f.getAnnotation(Column.class) != null)
                .map(f -> {
                    Column column = f.getAnnotation(Column.class);
                    return column.name();
                }).collect(Collectors.toList());

        List<String> ts18AAddFields = Arrays.stream(LawatBayar18AAdd.class.getDeclaredFields())
                .filter(f -> f.getAnnotation(Column.class) != null)
                .map(f -> {
                    Column column = f.getAnnotation(Column.class);
                    return column.name();
                }).collect(Collectors.toList());

        this.ts18AFields.addAll(ts18AAddFields);
    }
}
