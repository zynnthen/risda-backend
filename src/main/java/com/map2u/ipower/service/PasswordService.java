package com.map2u.ipower.service;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PasswordService {

    private PasswordEncoder encoder = null;


    private PasswordEncoder getEncoder() {
        if (this.encoder == null) {
            this.encoder = new BCryptPasswordEncoder();
        }

        return this.encoder;
    }

    public boolean matchHashPassword(String rawPassword, String encodedPassword) {
        return this.getEncoder().matches(rawPassword, encodedPassword);
    }
}
