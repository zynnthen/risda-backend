package com.map2u.ipower.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.map2u.ipower.config.JwtSetting;
import com.map2u.ipower.enums.TokenType;
import com.map2u.ipower.exception.UnauthorizedException;
import com.map2u.ipower.model.user.UserDetailsModel;
import com.map2u.ipower.model.user.UserRoleModel;
import com.map2u.ipower.util.Util;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
@Slf4j
public class JWTService {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    JwtSetting jwtSetting;

    private JWSSigner jwtSigner = null;
    private String sharedSecret = null;
    private static final String ROLE_PREFIX = "ROLE_";
    private static final String EMPTY_PWD = "";
    private static final String BEARER_PREFIX = "Bearer ";

    public String getRolePrefix(){
        return ROLE_PREFIX;
    }

    public String getAuthorizationHeader() {
        return jwtSetting.getAuthHeader();
    }

    public boolean isAuthEnabled() {
        return jwtSetting.isAuthEnable();
    }

    public boolean isAclEnabled() {
        return jwtSetting.isAclEnable();
    }

    private String getFixedSecret() {
        if (this.sharedSecret == null || this.sharedSecret.isEmpty()) {
            this.sharedSecret = jwtSetting.getSecret();
        }

        return this.sharedSecret;
    }

    private JWSSigner getJwtSigner() {
        if (this.jwtSigner == null) {
            // Create HMAC signer
            try {
                this.jwtSigner = new MACSigner(this.getFixedSecret());
            } catch (KeyLengthException ex) {
                log.error("Error while creating HMAC signer: {}", ex.getMessage());
            }
        }

        return this.jwtSigner;
    }

    private boolean isIssuedCorrectly(JWTClaimsSet claimsSet) {
        return claimsSet.getIssuer().equals(jwtSetting.getIssuer());
    }

    private String stripBearerToken(String token) {
        return token.startsWith(BEARER_PREFIX) ? token.substring(BEARER_PREFIX.length()) : token;
    }

    private boolean compareTokenType(TokenType tokenType, JWTClaimsSet claimsSet) {
        return claimsSet.getClaim(jwtSetting.getTypeField()).equals(tokenType.toString());
    }

    private String generateJWT(TokenType type, UserDetailsModel userDetails) throws Exception {
        Date now = new Date();

//        log.info("User Authorities: {}",user.getAuthorities());
        // Prepare JWT with claims set
        JWTClaimsSet.Builder jwtBuilder = new JWTClaimsSet.Builder()
                .issuer(jwtSetting.getIssuer())
                .issueTime(now)
                .subject(jwtSetting.getSubject())
                .jwtID(String.valueOf(userDetails.getId()))
                .notBeforeTime(now)
//                .expirationTime(new Date(now.getTime() + (PropsUtil.getJwtTokenIdExpiry() * 1000)))
                .claim(jwtSetting.getUserField(), new ObjectMapper().writeValueAsString(userDetails))
                .claim(jwtSetting.getTypeField(), type.toString());

        if (type.equals(TokenType.REFRESH)) {
            jwtBuilder = jwtBuilder.expirationTime(new Date(now.getTime() + (jwtSetting.getRefreshTokenExpTime() * 1000)));
        } else {
            jwtBuilder = jwtBuilder.expirationTime(new Date(now.getTime() + (jwtSetting.getTokenExpirationTime() * 1000)));
        }

        JWTClaimsSet claimsSet = jwtBuilder.build();

        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet);

        // Apply the HMAC protection
        signedJWT.sign(getJwtSigner());

        // Serialize to compact form, produces something like
        // eyJhbGciOiJIUzI1NiJ9.SGVsbG8sIHdvcmxkIQ.onO9Ihudz3WkiauDO2Uhyuz0Y18UASXlSc1eS0NkWyA
        return signedJWT.serialize();
    }

    private JWTClaimsSet verifyJWT(TokenType type, String token) throws Exception {
        SignedJWT signedJWT = SignedJWT.parse(stripBearerToken(token));
        JWSVerifier verifier = new MACVerifier(this.getFixedSecret());
        boolean isVerified = signedJWT.verify(verifier);

        JWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();

//        log.info("User: {}", claimsSet.getClaim(PropsUtil.getJwtTokenFieldUser()));
//        log.info("Is verified: {}", isVerified);
        if (isVerified) {
            if (!isIssuedCorrectly(claimsSet)) {
//                throw new UnauthorizedException(String.format("Issuer %s in JWT token doesn't match %s", claimsSet.getIssuer(), jwtSetting.getIssuer()));
                throw new UnauthorizedException("Issuer in JWT token doesn't match");
            }

            if (!compareTokenType(type, claimsSet)) {
                throw new UnauthorizedException(String.format("JWT Token doesn't seem to be an %s Token", type.toString()));
            }

            return claimsSet;
        }

        throw new UnauthorizedException(String.format("Invalid token given"));
    }

    public JWTClaimsSet verifyIdToken(String token) throws Exception {
        return verifyJWT(TokenType.ID, token);
    }

    public JWTClaimsSet verifyRefreshToken(String token) throws Exception {
        return verifyJWT(TokenType.REFRESH, token);
    }

    public UserDetailsModel getUserDetailsFromJWTClaimSet(JWTClaimsSet claimsSet) throws Exception {
        Object userClaim = claimsSet.getClaims().get(jwtSetting.getUserField());
        if (userClaim != null) {
            return objectMapper.readValue(userClaim.toString(), UserDetailsModel.class);
        }

        return null;
    }

    public JWTClaimsSet getClaimsSet(TokenType tokenType, String token) throws Exception {
        return verifyJWT(tokenType, token);
    }

    public Authentication getAuthentication(HttpServletRequest request) throws Exception {
        String idToken = request.getHeader(jwtSetting.getAuthHeader());
        if (idToken != null) {
            JWTClaimsSet claimsSet = verifyIdToken(idToken);

            UserDetailsModel userDetails = objectMapper.readValue(
                    claimsSet.getClaims().get(jwtSetting.getUserField()).toString(), UserDetailsModel.class);


            log.info("User from token {}", userDetails.getEmail());
            Long userId = userDetails.getId();

            if (userId != null) {
                List<GrantedAuthority> grantedAuthorities = Util.convertList(new HashSet<UserRoleModel>() {{
                                                                                 add(userDetails.getUserRole());
                                                                             }},
                        u -> new SimpleGrantedAuthority(ROLE_PREFIX + u.getId()));
//                List<GrantedAuthority> grantedAuthorities = convertList(
//                        userDetails.getAuthorities(),
//                        a -> new SimpleGrantedAuthority(ROLE_PREFIX + constructAuthorityNameBySiteIdAndAccessLevelName(a.getSiteId(), a.getAccessLevelName())));

//                User user = new User(userId.toString(), EMPTY_PWD, new ArrayList<>());
                User user = new User(userId.toString(), EMPTY_PWD, grantedAuthorities);
                return new UsernamePasswordAuthenticationToken(user, claimsSet, grantedAuthorities);
            }
            log.trace("No uuid found from id token");
        }

        log.trace("No idToken found in HTTP Header");
        return null;
    }

    public String generateIdToken(UserDetailsModel userDetails) throws Exception {
        return generateJWT(TokenType.ID, userDetails);
    }

    public String generateRefreshToken(UserDetailsModel userDetails) throws Exception {
        return generateJWT(TokenType.REFRESH, userDetails);
    }

}
