package com.map2u.ipower.service;

import com.map2u.ipower.exception.SourceNotFoundException;
import com.map2u.ipower.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.ZoneId;

@Service
@Slf4j
public class FileStorageService {

    @Value("${file.storage.path}")
    String storagePath;


    public String storeFile(MultipartFile file) {

        String originalFileName = file.getOriginalFilename();

        String fileId = constructFileId(originalFileName);
//        String fileName = fileId + originalFileName.substring(originalFileName.lastIndexOf("."));
//        System.out.println(fileName);
        log.info("Storing file to {}, file ID: {}", storagePath, fileId);
        try {
            Path location = Paths.get(storagePath + fileId);
            Files.copy(file.getInputStream(), location, StandardCopyOption.REPLACE_EXISTING);
            log.info("File stored");
            return fileId;
        } catch (Exception e) {
            log.error("Error while storing file", e);
        }
        return null;
    }

    public boolean updateFile(String fileName, MultipartFile file) {
        log.info("Updating file: {}", fileName);
        try {
            Path location = Paths.get(storagePath + fileName);
            Files.copy(file.getInputStream(), location, StandardCopyOption.REPLACE_EXISTING);
            log.info("File updated");
            return true;
        } catch (Exception e) {
            log.error("Error while storing file", e);
        }

        return false;
    }

    public Resource getFile(String fileName) {
        log.info("Retrieving file {} from dir {} ", fileName, storagePath);
        Path location = Paths.get(storagePath).resolve(fileName).normalize();
        try {

            Resource resource = new UrlResource(location.toUri());
            if (resource.exists()) {
                return resource;
            }
            throw new SourceNotFoundException(String.format("File not found for %s ", fileName));
        } catch (MalformedURLException ex) {
            throw new SourceNotFoundException(String.format("File not found for %s ", fileName));
        }
    }

    public boolean deleteFile(String fileName) {
        Path location = Paths.get(storagePath).resolve(fileName).normalize();

        try {
            boolean isDeleted = Files.deleteIfExists(location);
            if (isDeleted) {
                log.info(String.format("File %s is deleted.", fileName));
                return true;
            } else {
                log.info(String.format("File %s delete unsuccessful.", fileName));
            }
        } catch (Exception e) {
            log.error("Error while deleting file", e);
        }

        return false;
    }

    private String constructFileId(String fileName) {
        return Math.abs(fileName.hashCode()) + "_" + Util.getMiliseconds(null);
    }
}
