package com.map2u.ipower.service;

import com.map2u.ipower.config.UserRoleConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class AuthService {

    @Autowired
    JWTService jwtService;

    @Autowired
    UserRoleConfig userRoleConfig;

    Map<String,Integer> userCode;

    public boolean isAuthEnabled() {
        return jwtService.isAuthEnabled();
    }

    public boolean isAclEnabled(){
        return jwtService.isAclEnabled();
    }

    public Authentication getAuthentication(HttpServletRequest request) throws Exception {
        return jwtService.getAuthentication(request);
    }

    public String getAuthorizationHeader(){
        return jwtService.getAuthorizationHeader();
    }

    public UserRoleConfig getUserRoleConfig(){
        return this.userRoleConfig;
    }

    public String getUserRolePrefix(){
        return this.jwtService.getRolePrefix();
    }
}
