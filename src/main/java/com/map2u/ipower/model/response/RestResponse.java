package com.map2u.ipower.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestResponse<T> {
    public static final String STATUS_OK = "OK";
    public static final String STATUS_FAILED = "NOK";
    public static final String MESSAGE_SUCCESSFUL="Successful";
    public static final String MESSAGE_FAIL="Fail";

    private String result = STATUS_OK;

    private String message;

    private T data;

    public RestResponse() {

    }

    public RestResponse(String result) {
        this.result = result;
    }

    public RestResponse(String result, String message) {
        this.result = result;
        this.message = message;
    }

    public RestResponse(String result, String message, T data) {
        this.result = result;
        this.message = message;
        this.data = data;
    }

    public RestResponse(String result, T data) {
        this.result = result;
        this.message = this.MESSAGE_SUCCESSFUL;
        this.data = data;
    }

}