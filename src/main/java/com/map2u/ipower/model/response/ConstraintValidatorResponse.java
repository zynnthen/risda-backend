package com.map2u.ipower.model.response;

import lombok.Data;

@Data
public class ConstraintValidatorResponse {
    long timestamp;

    int status;

    String error;

    String message;

    String path;
}
