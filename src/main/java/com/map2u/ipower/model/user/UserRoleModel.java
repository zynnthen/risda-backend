package com.map2u.ipower.model.user;

import lombok.Data;


@Data
public class UserRoleModel {

    long id;

    String role;

    long categoryId;
}
