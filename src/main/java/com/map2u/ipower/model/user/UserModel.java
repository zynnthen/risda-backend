package com.map2u.ipower.model.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UserModel {

    @NotBlank(message = "Email should not be blank")
    @Email(message = "Invalid email address given")
    String email;

    @NotBlank(message = "Name should not be blank")
    String name;

}
