package com.map2u.ipower.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Set;
import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDetailsModel {

    Long id;

    String staffNo;

    String identity;

    String name;

    String password;

    String email;

    Integer category;

    String ptCode;

    String ptCodeNew;

    String designation;

    String contact;

    UserRoleModel userRole;

//    Set<AuthorityModel> authorities;

//    @Override
//    public void setName(String name) {
//        super.setName(name);
//    }
//
//    @Override
//    public void setEmail(String email) {
//        super.setEmail(email);
//    }
}
