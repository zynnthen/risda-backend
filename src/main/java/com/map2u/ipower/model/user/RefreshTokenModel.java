package com.map2u.ipower.model.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RefreshTokenModel {

    @NotBlank(message = "Refresh token should not be blank")
    String refreshToken;
}
