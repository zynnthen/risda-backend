package com.map2u.ipower.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthenticatedUserModel {

    UserDetailsModel user;

    TokenModel tokens;

    public AuthenticatedUserModel() {
    }

    public AuthenticatedUserModel(UserDetailsModel user, TokenModel tokens) {
        this.user = user;
        this.tokens = tokens;
    }
}
