package com.map2u.ipower.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenModel {

    String idToken;

    Integer accessCode;

    String refreshToken;

    public TokenModel() {
    }

    public TokenModel(String idToken) {
        this.idToken = idToken;
    }

    public TokenModel(String idToken, String refreshToken) {
        this.idToken = idToken;
        this.refreshToken = refreshToken;
    }

    public TokenModel(String idToken, String refreshToken, Integer accessCode) {
        this.idToken = idToken;
        this.accessCode = accessCode;
        this.refreshToken = refreshToken;
    }
}
