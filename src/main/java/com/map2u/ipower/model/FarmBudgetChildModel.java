package com.map2u.ipower.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;

@Data
public class FarmBudgetChildModel {

    String ptCode;

    long noSiriFBC;

    String noSiriFBM;

    String ansuranNo;

    String katKerja;

    String kerjaStokKod;

    String stokItem;

    Integer jumlahUnit;

    Double hargaUnit;

    Integer unitHektar;

    Double hargaKos;

    String statusByr;

    String perihalStokItem;
}
