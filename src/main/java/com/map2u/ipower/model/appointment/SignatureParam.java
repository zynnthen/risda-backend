package com.map2u.ipower.model.appointment;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class SignatureParam {

        MultipartFile signPeg;

        MultipartFile signOwner;

}
