package com.map2u.ipower.model.appointment;

import com.map2u.ipower.model.FarmBudgetModel;
import com.map2u.ipower.model.form.TSForm;
import lombok.Data;

import java.util.List;

@Data
public class AppointmentModel {

    String noSiriTemujanji;

    String ptCode;

    String moduleOwner;

    String tsPendekatan;

    String jenisLK;

    String tsMohonId;

    String kumpulanId;

    String kumpulanClaim;

    Long tkhTJanji;

    Long masaTJanji;

    String tempatTJanji;

    String statusTJanji;

    String pegKemaskini;

    Long tkhKemaskini;

    String ansuranNo;

    TSApplicationModel tsApplication;

    List<TSForm> forms;

    List<ParticipantAppointmentModel> participants;

//    FarmBudgetModel farmBudget;

    List<FarmBudgetItem> farmBudgetItems;
}
