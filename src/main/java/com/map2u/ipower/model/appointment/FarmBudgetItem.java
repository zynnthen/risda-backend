package com.map2u.ipower.model.appointment;

import lombok.Data;

@Data
public class FarmBudgetItem {
    String ptCode;
    String ansuranNo;
    String katKerja;
    String kerjaStokKod;
    String stokItem;
//    From DC_StockItem
    String jenisStok;
    String perihalStokItem;
    String ukuran;

//    From DC_KodKerja;
    String kodKerjaId;
    String kodKerjaDesc;


}
