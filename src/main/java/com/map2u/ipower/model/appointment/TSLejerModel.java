package com.map2u.ipower.model.appointment;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class TSLejerModel {
    String ptCode;

    String noLejer;

    Long tkhLejer;

    String tsMohonId;

    String noRisda7;

    Double amaunJumlah;

    String pegKemaskini;

    Long tkhKemaskini;

    String rujukan;

    String rpkMohonId;

    List<TSLejerChildModel> child;

}
