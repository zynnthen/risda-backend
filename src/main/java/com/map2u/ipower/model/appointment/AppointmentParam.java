package com.map2u.ipower.model.appointment;

import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class AppointmentParam {

    String tsMohonId;

    String pegKemaskini;

    String ptCode;

    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @ApiParam(value = "dd-MM-yyyy")
    Date date;

    Long start;

    Long end;

    String noKpPemohon;

    String namaPemohon;

    String noLot;

    String noGeran;

    String statusTJanji;

    @ApiParam(value = "default is ascending order")
    Sort.Direction tkhTJanji = Sort.Direction.ASC;

}
