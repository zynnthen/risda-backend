package com.map2u.ipower.model.appointment;

import lombok.Data;

@Data
public class TSLejerChildModel {
    String ptCode;

    long noLejerChild;

    String noLejer;

    Long noBaris;

    String transaksiDesc;

    String ansuranNo;

    String kerjaStokKod;

    String kerjaStokDesc;

    Double luasBayar;

    String jenisStok;

    Integer kuantitiStok;

    Double kerjaStokHrg;

    Double kerjaStokAmt;

}
