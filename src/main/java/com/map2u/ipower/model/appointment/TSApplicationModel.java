package com.map2u.ipower.model.appointment;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class TSApplicationModel {

    String ptCode;

    String tsMohonNo;

    String tsJenis;

    String pemilikLot;

    String idESpek;

    String icNoPemohon;

    String namaPemohon;

    Long DOB_Pmohon;

    String sexPemohon;

    String racePemohon;

    String address1;

    String address2;

    String address3;

    String poskod;

    String bandar;

    String negeri;

    String daerah;

    String mukim;

    String seksyen;

    String kampung;

    String dun;

    String parlimen;

    String noPhoneMobile;

    String noPhoneHouse;

    String noPhoneOffice;

    String email;

    String bankAccNo;

    String bankName;

    String bankSCode;

    String tsPendekatan;

    String tsAgensi;

    String tsAktiviti;

    String tsProgram;

    String tsRMK;

    String asalPemohon;

    String noSmbTso;

    Long tkhTerima;

    Long tkhLulus;

    String pegLulus;

    String ptrimaBantuan;

    String statusPmohon;

    String statusAlasan;

    String statusAktif;

    String statusPerluTS18;

    String kodPASemasa;

    String kodObjSTunai;

    String kodObjSStok;

    String kodPaBerturut;

    String kodObjBTunai;

    String kodObjBStok;

    String pegKemaskini;

    Long tkhKemaskini;

    Long noSiriTs;

    String dtSiriTs;

    String warganegara;

    String bayarKepada;

    String swkStatus;

    String swkKod;

    String tsKategori;

    String kumpulanId;

    String tsMohonId;

    List<LotOwnerModel> lotDimiliki;
}
