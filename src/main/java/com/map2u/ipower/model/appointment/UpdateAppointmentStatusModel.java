package com.map2u.ipower.model.appointment;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdateAppointmentStatusModel {

    @NotNull(message = "No siri temujanji should not be null")
    String noSiriTemujanji;

    @NotNull(message = "Status should not be null")
    String status;
}
