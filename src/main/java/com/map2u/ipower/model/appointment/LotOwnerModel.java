package com.map2u.ipower.model.appointment;

import lombok.Data;


@Data
public class LotOwnerModel {

    String lotMilikId;

    String ptCode;

    String tsMohonId;

    String icNoMilik;

    String noLithosheet;

    String noGeran;

    String noGeranLama;

    String noLot;

    String noLotLama;

    Float bhgnMilikan;

    Double luasDiambil;

    Long tkhDiambil;

    String syaratKhas;

    String butirPajak;

    String butirKaveat;

    String catatan;

    String negeri;

    String daerah;

    String mukim;

    String seksyen;

    String kampung;

    String dun;

    String parlimen;

    String replant;

    String syaratNyata;

    String tsCrop;

    String tsCropSub;

    String tsBulan;

    String tsTahun;

    Double luasLot;

    Double luasTs;

    String pegKemaskini;

    Long tkhKKemaskini;

    String carianRasmi;

    Long tarikhPecahan;

    String sumber;

    String logUpi;

    String kodUpi;

    Long tarafTanah;
}
