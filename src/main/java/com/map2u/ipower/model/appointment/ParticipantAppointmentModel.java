package com.map2u.ipower.model.appointment;

import lombok.Data;

import java.util.List;

@Data
public class ParticipantAppointmentModel {
    long temujanjiPID;

    String noSiriTemujanji;

    String tSmohonID;

    String kumpulanID;

    String status;

    KumpulanTSModel kumpulanTS;

    TSApplicationModel tsApplication;

    List<String> lejer;
}
