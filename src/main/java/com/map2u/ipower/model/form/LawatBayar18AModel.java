package com.map2u.ipower.model.form;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18A;
import com.map2u.ipower.ipowerdb.entity.LawatBayar18AAdd;
import com.map2u.ipower.ipowerdb.entity.LawatBayar18AAktiviti;
import com.map2u.ipower.model.appointment.TSApplicationModel;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Data
public class LawatBayar18AModel extends TSForm {

//    String ptCode;
//    String noSiriTemujanji;
//    String jenisLK;
//    String tsMohonId;
//    String pegPemeriksa;
//    String noKpPpk;
//    String jawPemeriksa;
//    Long tkhLawat;
//    String pegKemaskini;
//    Long tkhKemaskini;
//    String statusLulus;
//    String pegLulus;
//    Long tkhLulus;
//    Integer bilLawatan;
//    String catatan;


    @Size(max = 12, message = "noSiriLB should less than {max} characters ")
    String noSiriLB;

    @Size(max = 3, message = "moduleOwner should less than {max} characters ")
    String modulOwner;

    @Size(max = 2, message = "tsMohonLBSiri should less than {max} characters ")
    String tsMohonLBSiri;

    @Size(max = 2, message = "ansuranNo should less than {max} characters ")
    String ansuranNo;

    @Size(max = 5, message = "perimeter should less than {max} characters ")
    String perimeter;

    Double tebangJentera1;

    Double tebangJentera2;

    Double jarakLubang1;

    Double jarakLubang2;

    Double jarakLubang3;

    Double luasTanam;

    Long tkhTanam;

    @Size(max = 30, message = "jenisBenih should less than {max} characters ")
    String jenisBenih;

    @Size(max = 30, message = "klonBenih should less than {max} characters ")
    String klonBenih;

    @Size(max = 5, message = "benihLuar should less than {max} characters ")
    String benihLuar;

    @Size(max = 5, message = "sahKlon should less than {max} characters ")
    String sahKlon;

    @Size(max = 5, message = "gapSenggara should less than {max} characters ")
    String gapSenggara;

    @Size(max = 5, message = "gapBaja should less than {max} characters ")
    String gapBaja;

    @Size(max = 5, message = "gapCantasan should less than {max} characters ")
    String gapCantasan;

    @Size(max = 15, message = "jenisCantasan should less than {max} characters ")
    String jenisCantasan;

    @Size(max = 5, message = "gapGalakDahan should less than {max} characters ")
    String gapGalakDahan;

    @Size(max = 15, message = "jenisGalakDahan should less than {max} characters ")
    String jenisGalakDahan;

    @Size(max = 5, message = "kpbKontan should less than {max} characters ")
    String kpbKontan;

    @Size(max = 15, message = "kpbKontanSelenggara should less than {max} characters ")
    String kpbKontanSelenggara;

    @Digits(integer = 4, fraction = 0, message = "getahPkkHek should less than {integer} digits")
    Integer getahPkkHek;

    @Digits(integer = 4, fraction = 0, message = "tlPkkHidup should less than {integer} digits")
    Integer tlPkkHidup;

    @Digits(integer = 4, fraction = 0, message = "sulaman should less than {integer} digits")
    Integer sulaman;

    Double ukurLilit;

    @Size(max = 5, message = "kawalPerosak should less than {max} characters ")
    String kawalPerosak;

    Double rumpaiSenggara;

    Double rumpaiTdkSenggara;

    Long tkhBerhasil;

    String kenyataanLain;

    //    time only
    Long mLawatMula;

    //time only
    Long mLawatTamat;

    String planTanah;

    @Size(max = 5, message = "hadirSendiri should less than {max} characters ")
    String hadirSendiri;

    Double luasBerjaya;

    String statusTolakKIV;

    @Size(max = 10, message = "kumpulanId should less than {max} characters ")
//    @NotBlank(message = "kumpulanId should not be blank")
    String kumpulanId;

    String namaKumpulan;

    @Size(max = 10, message = "kumpNoClaim should less than {max} characters ")
    String kumpNoClaim;

    @Size(max = 3, message = "jarakTanaman should less than {max} characters ")
    String jarakTanaman;

    @Size(max = 5, message = "bekalAwal should less than {max} characters ")
    String bekalAwal;

    Character statusR7;

    @Size(max = 10, message = "klonMutu should less than {max} characters ")
    String klonMutu;

    @Size(max = 10, message = "resitBenih should less than {max} characters ")
    String resitBenih;

    @Size(max = 10, message = "tapakSemaian should less than {max} characters ")
    String tapakSemaian;

    @Size(max = 50, message = "benihSawit should less than {max} characters ")
    String benihSawit;

    Double luasKontan;

    String signPegawai;

    String signPerakuan;

    String namaPerakuan;

    String noKpPerakuan;

    String hadir;

    Double latitude;

    Double longitude;

    String smartmapLink;

    TSApplicationModel tsApplication;

    List<LawatKebunDocumentModel> documents;

    @Valid
    List<LawatBayar18AAktivitiModel> activities;

    LawatBayar18EModel ts18e;

    public LawatBayar18A convertModelToEntity() {
        LawatBayar18A entity = new LawatBayar18A();
        entity.setAnsuranNo(this.ansuranNo);
        if(this.bekalAwal != null && !this.bekalAwal.isEmpty()) {
            entity.setBekalAwal(this.bekalAwal);
        }
        if(this.benihLuar != null && !this.benihLuar.isEmpty()) {
            entity.setBenihLuar(this.benihLuar);
        }
        if(this.benihSawit != null && !this.benihSawit.isEmpty()) {
            entity.setBenihSawit(this.benihSawit);
        }
        entity.setBilLawatan(this.bilLawatan);
        if(this.catatan != null && !this.catatan.isEmpty()) {
            entity.setCatatan(this.catatan);
        }
        if(this.gapBaja != null && !this.gapBaja.isEmpty()) {
            entity.setGapBaja(this.gapBaja);
        }
        if(this.gapCantasan != null && !this.gapCantasan.isEmpty()) {
            entity.setGapCantasan(this.gapCantasan);
        }
        if(this.gapGalakDahan != null && !this.gapGalakDahan.isEmpty()) {
            entity.setGapGalakDahan(this.gapGalakDahan);
        }
        if(this.gapSenggara != null && !this.gapSenggara.isEmpty()) {
            entity.setGapSenggara(this.gapSenggara);
        }
        entity.setGetahPkkHek(this.getahPkkHek);
        if(this.hadirSendiri != null && !this.hadirSendiri.isEmpty()) {
            entity.setHadirSendiri(this.hadirSendiri);
        }
        entity.setJarakLubang1(this.jarakLubang1);
        entity.setJarakLubang2(this.jarakLubang2);
        entity.setJarakLubang3(this.jarakLubang3);
        if(this.jarakTanaman != null && !this.jarakTanaman.isEmpty()) {
            entity.setJarakTanaman(this.jarakTanaman);
        }
        if(this.jawPemeriksa != null && !this.jawPemeriksa.isEmpty()) {
            entity.setJawPemeriksa(this.jawPemeriksa);
        }
        if(this.jenisBenih != null && !this.jenisBenih.isEmpty()) {
            entity.setJenisBenih(this.jenisBenih);
        }
        if(this.jenisCantasan != null && !this.jenisCantasan.isEmpty()) {
            entity.setJenisCantasan(this.jenisCantasan);
        }
        if(this.jenisGalakDahan != null && !this.jenisGalakDahan.isEmpty()) {
            entity.setJenisGalakDahan(this.jenisGalakDahan);
        }
        if(this.jenisLK != null && !this.jenisLK.isEmpty()) {
            entity.setJenisLK(this.jenisLK);
        }
        if(this.kawalPerosak != null && !this.kawalPerosak.isEmpty()) {
            entity.setKawalPerosak(this.kawalPerosak);
        }
        if(this.kenyataanLain != null && !this.kenyataanLain.isEmpty()) {
            entity.setKenyataanLain(this.kenyataanLain);
        }
        if(this.klonBenih != null && !this.klonBenih.isEmpty()) {
            entity.setKlonBenih(this.klonBenih);
        }
        if(this.klonMutu != null && !this.klonMutu.isEmpty()) {
            entity.setKlonMutu(this.klonMutu);
        }
        if(this.kpbKontan != null && !this.kpbKontan.isEmpty()) {
            entity.setKpbKontan(this.kpbKontan);
        }
        if(this.kpbKontanSelenggara != null && !this.kpbKontanSelenggara.isEmpty()) {
            entity.setKpbKontanSelenggara(this.kpbKontanSelenggara);
        }
        if(this.kumpNoClaim != null && !this.kumpNoClaim.isEmpty()) {
            entity.setKumpNoClaim(this.kumpNoClaim);
        }
        if(this.kumpulanId != null && !this.kumpulanId.isEmpty()) {
            entity.setKumpulanId(this.kumpulanId);
        }
        entity.setLuasBerjaya(this.luasBerjaya);
        entity.setLuasKontan(this.luasKontan);
        entity.setLuasTanam(this.luasTanam);
        if(this.smartmapLink != null && !this.smartmapLink.isEmpty()) {
            entity.setSmartmapLink(this.smartmapLink);
        }
        if (this.mLawatMula != null) {
            entity.setMLawatMula(new Date(this.tkhLawat + this.mLawatMula));
        }
        if (this.mLawatTamat != null) {
            entity.setMLawatTamat(new Date(this.tkhLawat + this.mLawatTamat));
        }

        if(this.modulOwner != null && !this.modulOwner.isEmpty()) {
            entity.setModulOwner(this.modulOwner);
        }
        if(this.noKpPpk != null && !this.noKpPpk.isEmpty()) {
            entity.setNoKpPpk(this.noKpPpk);
        }
        if(this.noSiriLB != null && !this.noSiriLB.isEmpty()) {
            entity.setNoSiriLB(this.noSiriLB);
        }
        if(this.noSiriTemujanji != null && !this.noSiriTemujanji.isEmpty()) {
            entity.setNoSiriTemujanji(this.noSiriTemujanji);
        }
        if(this.pegKemaskini != null && !this.pegKemaskini.isEmpty()) {
            entity.setPegKemaskini(this.pegKemaskini);
        }
        if(this.pegLulus != null && !this.pegLulus.isEmpty()) {
            entity.setPegLulus(this.pegLulus);
        }
        if(this.perimeter != null && !this.perimeter.isEmpty()) {
            entity.setPerimeter(this.perimeter);
        }
        if(this.pegPemeriksa != null && !this.pegPemeriksa.isEmpty()) {
            entity.setPegPemeriksa(this.pegPemeriksa);
        }
        if(this.planTanah != null && !this.planTanah.isEmpty()) {
            entity.setPlanTanah(this.planTanah);
        }
        if(this.ptCode != null && !this.ptCode.isEmpty()) {
            entity.setPtCode(this.ptCode);
        }
        if(this.resitBenih != null && !this.resitBenih.isEmpty()) {
            entity.setResitBenih(this.resitBenih);
        }
        if(this.rumpaiTdkSenggara != null) {
            entity.setRumpaiTdkSenggara(this.rumpaiTdkSenggara);
        }
        if(this.rumpaiSenggara != null) {
            entity.setRumpaiSenggara(this.rumpaiSenggara);
        }
        if(this.sahKlon != null && !this.sahKlon.isEmpty()) {
            entity.setSahKlon(this.sahKlon);
        }
        if(this.statusLulus != null && !this.statusLulus.isEmpty()) {
            entity.setStatusLulus(this.statusLulus);
        }
        if(this.pegLulus != null && !this.pegLulus.isEmpty()) {
            entity.setPegLulus(this.pegLulus);
        }
        entity.setStatusR7(this.statusR7);
        if(this.statusTolakKIV != null && !this.statusTolakKIV.isEmpty()) {
            entity.setStatusTolakKIV(this.statusTolakKIV);
        }
        if(this.sulaman != null) {
            entity.setSulaman(this.sulaman);
        }
        if(this.tapakSemaian != null && !this.tapakSemaian.isEmpty()) {
            entity.setTapakSemaian(this.tapakSemaian);
        }
        entity.setTebangJentera1(this.tebangJentera1);
        entity.setTebangJentera2(this.tebangJentera2);

        if (this.tkhBerhasil != null) {
            entity.setTkhBerhasil(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhBerhasil), ZoneId.systemDefault()));
        }

        if (this.tkhKemaskini != null) {
            entity.setTkhKemaskini(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhKemaskini), ZoneId.systemDefault()));
        }

        if (this.tkhLawat != null) {
            entity.setTkhLawat(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhLawat), ZoneId.systemDefault()));
        }

        if (this.tkhLulus != null) {
            entity.setTkhLulus(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhLulus), ZoneId.systemDefault()));
        }

        if (this.tkhTanam != null) {
            entity.setTkhTanam(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhTanam), ZoneId.systemDefault()));
        }

        if(this.tlPkkHidup != null) {
            entity.setTlPkkHidup(this.tlPkkHidup);
        }
        if(this.tsMohonId != null) {
            entity.setTsMohonId(this.tsMohonId);
        }

        if(this.tsMohonLBSiri != null && !this.tsMohonLBSiri.isEmpty()) {
            entity.setTsMohonLBSiri(this.tsMohonLBSiri);
        }
        if(this.ukurLilit != null) {
            entity.setUkurLilit(this.ukurLilit);
        }

        if (this.signPegawai != null || this.signPerakuan != null || this.namaPerakuan != null || this.noKpPerakuan != null) {
            LawatBayar18AAdd formAdd = new LawatBayar18AAdd();
            formAdd.setNamaPerakuan(this.namaPerakuan);
            formAdd.setNoKpPerakuan(this.noKpPerakuan);
            formAdd.setNoSiriLB(this.noSiriLB);
            formAdd.setSignPegawai(this.signPegawai);
            formAdd.setSignPerakuan(this.signPerakuan);
            formAdd.setHadir(this.hadir);
            formAdd.setLatitude(this.latitude);
            formAdd.setLongitude(this.longitude);
//            formAdd.setForm(entity);
            entity.setAdditional(formAdd);
        }

        if (this.activities != null) {
            List<LawatBayar18AAktiviti> activityEntities = this.activities.stream()
                    .map(a -> {
                        LawatBayar18AAktiviti activity = a.convertModelToEntity();
                        activity.setNoSiriLB(this.noSiriLB);
                        return activity;
                    })
                    .collect(Collectors.toList());
            entity.setActivities(activityEntities);


        }

        return entity;
    }
}
