package com.map2u.ipower.model.form;

import com.map2u.ipower.ipowerdb.entity.LawatKebun18;
import com.map2u.ipower.ipowerdb.entity.LawatKebun18Add;
import com.map2u.ipower.model.appointment.TSApplicationModel;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Data
public class LawatKebun18Model extends TSForm {

//    String ptCode;
//    String noSiriTemujanji;

//    String jenisLK;

//    String tsMohonId;
//    Long tkhLulus;
//    Integer bilLawatan;
//    String catatan;
//    String pegPemeriksa;
//    String noKpPpk;
//    String jawPemeriksa;
//    Long tkhLawat;
//    String pegKemaskini;
//    Long tkhKemaskini;
//    String statusLulus;
//    String pegLulus;

    @Size(max = 12, message = "noSiriLK should less than {max} characters ")
    String noSiriLK;

    @Size(max = 5, message = "bantuanLain should less than {max} characters ")
    String bantuanLain;

    @Size(max = 10, message = "blAgensi should less than {max} characters ")
    String blAgensi;

    @Size(max = 10, message = "blCrop should less than {max} characters ")
    String blCrop;

    @Size(max = 10, message = "blNoTs should less than {max} characters ")
    String blNoTs;

    Double blLuasLulus;

    Double blLuasHabisByr;

    Double luasGetahTua;

    @Size(max = 10, message = "tunggulGetah should less than {max} characters ")
    String tunggulGetah;

    @Size(max = 10, message = "lesenGetah should less than {max} characters ")
    String lesenGetah;

    @Size(max = 10, message = "bukuLesen should less than {max} characters ")
    String bukuLesen;

    @Size(max = 10, message = "suratAgensi should less than {max} characters ")
    String suratAgensi;

    @Size(max = 10, message = "gambarSatelit should less than {max} characters ")
    String gambarSatelit;

    @Size(max = 10, message = "buktiLot should less than {max} characters ")
    String buktiLot;

    @Size(max = 10, message = "umurPokok should less than {max} characters ")
    String umurPokok;

    @Size(max = 15, message = "getahTua should less than {max} characters ")
    String getahTua;

    @Size(max = 5, message = "pokokK300 should less than {max} characters ")
    String pokokK300;

    @Size(max = 5, message = "hasilK500 should less than {max} characters ")
    String hasilK500;

    @Size(max = 10, message = "kulitTorehan should less than {max} characters ")
    String kulitTorehan;

    @Size(max = 50, message = "pelanTanah should less than {max} characters ")
    String pelanTanah;

    @Size(max = 5, message = "pkHadir should less than {max} characters ")
    String pkHadir;

    @Size(max = 50, message = "mLawatMula should less than {max} characters ")
    String mLawatMula;

    @Size(max = 50, message = "mLawatTamat should less than {max} characters ")
    String mLawatTamat;

    @Size(max = 40, message = "alasanLulus should less than {max} characters ")
    String alasanLulus;

    @Digits(integer = 4, fraction = 0, message = "alasanNo should less than {integer} digits")
    Integer alasanNo;

    String catatanLulus;

    Double luasLulus;

    String jawPegLulus;

    Double luasLain;

    @Size(max = 10, message = "buktiLotTs should less than {max} characters ")
    String buktiLotTs;

    @Size(max = 10, message = "sebabLainPRN should less than {max} characters ")
    String sebabLainPRN;

    Double luasTanamanLain;

    Double luasHutan;

    Double luasRumah;

    String lainLain;

    Double luasLainLain;

    String signPegawai;

    String signPerakuan;

    String namaPerakuan;

    String noKpPerakuan;

    String hadir;

    Double latitude;

    Double longitude;

    String smartmapLink;

    List<LawatKebunDocumentModel> documents;

    LawatBayar18EModel ts18e;

    TSApplicationModel tsApplication;

    public LawatKebun18 convertModelToEntity() {
        LawatKebun18 entity = new LawatKebun18();

        entity.setAlasanLulus(this.alasanLulus);
        entity.setAlasanNo(this.alasanNo);
        if(this.bantuanLain != null && !this.bantuanLain.isEmpty()) {
            entity.setBantuanLain(this.bantuanLain);
        }
        entity.setBilLawatan(this.bilLawatan);
        if(this.blAgensi != null && !this.blAgensi.isEmpty()) {
            entity.setBlAgensi(this.blAgensi);
        }
        if(this.blCrop != null && !this.blCrop.isEmpty()) {
            entity.setBlCrop(this.blCrop);
        }
        entity.setBlLuasHabisByr(this.blLuasHabisByr);
        entity.setBlLuasLulus(this.blLuasLulus);
        if(this.blNoTs != null && !this.blNoTs.isEmpty()) {
            entity.setBlNoTs(this.blNoTs);
        }
        entity.setBuktiLot(this.buktiLot);
        entity.setBuktiLotTs(this.buktiLotTs);
        entity.setBukuLesen(this.bukuLesen);
        if(this.catatan != null && !this.catatan.isEmpty()) {
            entity.setCatatan(this.catatan);
        }
        if(this.catatanLulus != null && !this.catatanLulus.isEmpty()) {
            entity.setCatatanLulus(this.catatanLulus);
        }
        entity.setGambarSatelit(this.gambarSatelit);
        entity.setGetahTua(this.getahTua);
        entity.setHasilK500(this.hasilK500);
        entity.setJawPemeriksa(this.jawPemeriksa);
        entity.setJenisLK(this.jenisLK);
        entity.setKulitTorehan(this.kulitTorehan);
        entity.setLainLain(this.lainLain);
        entity.setLesenGetah(this.lesenGetah);
        entity.setLuasGetahTua(this.luasGetahTua);
        entity.setLuasHutan(this.luasHutan);
        entity.setLuasLain(this.luasLain);
        entity.setLuasLainLain(this.luasLainLain);
        entity.setLuasLulus(this.luasLulus);
        entity.setLuasRumah(this.luasRumah);
        entity.setLuasTanamanLain(this.luasTanamanLain);
        entity.setMLawatMula(this.mLawatMula);
        entity.setMLawatTamat(this.mLawatTamat);
        entity.setNoKpPpk(this.noKpPpk);
        entity.setNoSiriLK(this.noSiriLK);
        entity.setNoSiriTemujanji(this.noSiriTemujanji);
        entity.setPegKemaskini(this.pegKemaskini);
        if(this.pegLulus != null && !this.pegLulus.isEmpty()) {
            entity.setPegLulus(this.pegLulus);
        }
        entity.setPegPemeriksa(this.pegPemeriksa);
        entity.setPelanTanah(this.pelanTanah);
        entity.setPkHadir(this.pkHadir);
        entity.setPokokK300(this.pokokK300);
        entity.setPtCode(this.ptCode);
        entity.setSebabLainPRN(this.sebabLainPRN);
        entity.setSuratAgensi(this.suratAgensi);
        if(this.statusLulus != null && !this.statusLulus.isEmpty()) {
            entity.setStatusLulus(this.statusLulus);
        }
        entity.setSmartmapLink(this.smartmapLink);

        if (this.tkhKemaskini != null) {
            entity.setTkhKemaskini(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhKemaskini), ZoneId.systemDefault()));
        }

        if (this.tkhLawat != null) {
            entity.setTkhLawat(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhLawat), ZoneId.systemDefault()));
        }

        entity.setTsMohonId(this.tsMohonId);
        entity.setTunggulGetah(this.tunggulGetah);
        entity.setUmurPokok(this.umurPokok);

        if (this.signPegawai != null || this.signPerakuan != null || this.namaPerakuan != null || this.noKpPerakuan != null) {
            LawatKebun18Add formAdd = new LawatKebun18Add();
            formAdd.setNamaPerakuan(this.namaPerakuan);
            formAdd.setNoKpPerakuan(this.noKpPerakuan);
            formAdd.setNoSiriLK(this.noSiriLK);
            formAdd.setSignPegawai(this.signPegawai);
            formAdd.setSignPerakuan(this.signPerakuan);
            formAdd.setHadir(this.hadir);
            formAdd.setLatitude(this.latitude);
            formAdd.setLongitude(this.longitude);
//            formAdd.setForm(entity);
            entity.setAdditional(formAdd);
        }

        return entity;
    }
}
