package com.map2u.ipower.model.form;

import lombok.Data;

@Data
public class FormParam {

    String noSiriTemujanji;

    String tsMohonId;

    String ptCode;

    Long tkhLawat;
}
