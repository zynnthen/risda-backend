package com.map2u.ipower.model.form;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18EAdd;
import com.map2u.ipower.ipowerdb.entity.LawatBayar18EChild;
import com.map2u.ipower.ipowerdb.entity.LawatBayar18EMast;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class LawatBayar18EModel {

    @Size(max = 12, message = "noSiriLE should less than {max} characters ")
    @ApiModelProperty(value = "Don't need for new form")
    String noSiriLE;

    @Size(max = 100, message = "catatan should less than {max} characters ")
    String catatan;

    @NotNull(message = "PT code should not be null")
    @NotBlank(message = "PT code should not be blank")
    @Size(max = 6, message = "ptCode should less than {max} characters ")
    String ptCode;

    @Size(max = 12, message = "noSiriLawatan should less than {max} characters ")
    @NotBlank(message = "noSiriLawatan code should not be empty")
    @ApiModelProperty(value = "No Siri LK/LB")
    String noSiriLawatan;

    @NotNull(message = "jenisLE should not be null")
    @NotBlank(message = "jenisLE should not be blank")
    @Size(max = 5, message = "jenisLE should less than {max} characters ")
    String jenisLE;

    @Size(max = 16, message = "tsMohonId should less than {max} characters ")
    String tsMohonId;

    Double luasLulus;

    @Size(max = 5, message = "statusLulus should less than {max} characters ")
    String statusLulus;

    @Size(max = 15, message = "pegLulus should less than {max} characters ")
    String pegLulus;

    String signPegawai;

    Double latitude;

    Double longitude;

    Long tkhLulus;

    Long tkhLawat;

    Long mLawatMula;

    Long mLawatTamat;

    @Size(max = 10, message = "imgPlan should less than {max} characters ")
    String imgPlan;

    String smartmapLink;

    @Valid
    List<LawatBayar18EChildModel> items;

    @ApiModelProperty(value = "Don't need while saving form")
    List<LawatKebunDocumentModel> documents;

    public LawatBayar18EMast convertEntityToModel() {
        LawatBayar18EMast entity = new LawatBayar18EMast();
        entity.setCatatan(this.catatan);
        entity.setImgPlan(this.imgPlan);
        entity.setJenisLE(this.jenisLE);
        entity.setLuasLulus(this.luasLulus);
        entity.setSmartmapLink(this.smartmapLink);

        if (this.mLawatMula != null) {
            entity.setMLawatMula(new Date(this.tkhLawat + this.mLawatMula));
        }

        if (this.mLawatTamat != null) {
            entity.setMLawatTamat(new Date(this.tkhLawat + this.mLawatTamat));
        }

        entity.setNoSiriLawatan(this.noSiriLawatan);
        entity.setNoSiriLE(this.noSiriLE);
        entity.setPegLulus(this.pegLulus);
        entity.setPtCode(this.ptCode);
        entity.setStatusLulus(this.statusLulus);

        if (this.tkhLawat != null) {
            entity.setTkhLawat(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhLawat), ZoneId.systemDefault()));
        }

        if (this.tkhLulus != null) {
            entity.setTkhLulus(LocalDateTime.ofInstant(Instant.ofEpochMilli(this.tkhLulus), ZoneId.systemDefault()));
        }

        entity.setTsMohonId(this.tsMohonId);

        if (this.items != null) {
            List<LawatBayar18EChild> itemEntities = this.items.stream().map(i -> i.convertModelToEntity(this.noSiriLE)).collect(Collectors.toList());
            entity.setItems(itemEntities);
        }

        if (this.signPegawai != null || this.latitude != null || this.longitude != null) {
            LawatBayar18EAdd formAdd = new LawatBayar18EAdd();
            formAdd.setSignPegawai(this.signPegawai);
            formAdd.setLatitude(this.latitude);
            formAdd.setLongitude(this.longitude);
            formAdd.setNoSiriLE(this.noSiriLE);
            entity.setAdditional(formAdd);
        }

        return entity;
    }
}
