package com.map2u.ipower.model.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class LawatBayar18AParam extends FormParam {
    //    @NotNull(message = "noSiriTemujanji should not be null.")
//    String noSiriTemujanji;

//    String tsMohonId;

    String kumpulanId;

    String noSiriLB;

//    String ptCode;

//    Long tkhLawat;
}
