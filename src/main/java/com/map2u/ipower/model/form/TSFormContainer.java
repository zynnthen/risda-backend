package com.map2u.ipower.model.form;

import lombok.Data;

import java.util.List;

@Data
public class TSFormContainer {

    long count;

    List<TSForm> forms;
}
