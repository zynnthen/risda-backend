package com.map2u.ipower.model.form;

import com.map2u.ipower.enums.KatKerja;
import com.map2u.ipower.ipowerdb.entity.LawatBayar18AAktiviti;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Data
public class LawatBayar18AAktivitiModel {

    @ApiModelProperty(value = "Don't need for new")
    Long id;

    @NotNull(message = "noSiriTemujanji should not be null")
    String noSiriTemujanji;

//    @NotBlank(message = "noSiriLB should not be blank")
    String noSiriLB;

    @ApiModelProperty(value = "Kerja Utama / Input Pertanian")
    @NotNull(message = "katKerja should be either 'Kerja Utama' or 'Input Pertanian'")
    KatKerja katKerja;

    @ApiModelProperty(value = "For kerja utama kerja stok id")
    String kerjaStokKod;

    @ApiModelProperty(value = "For input pertanian stok item id")
    String stokItem;

    @ApiModelProperty(value = "Always 1 if have")
    @Min(value = 1, message = "Status should be always 1")
    @Max(value = 1, message = "Status should be always 1")
    Integer status = 1;

    @ApiModelProperty(value = "Kerja Utama -> 'Caj' = 0, 'Laksana' = 1, 'tidak'= not saved \n" +
            "Input Pertanian -> 'Ya' = 1, 'tidak' = Not saved")
    @Min(value = 0, message = "jenisCaj should be either 0 or 1")
    @Max(value = 1, message = "jenisCaj should be either 0 or 1")
    @NotNull(message = "jenisCaj should not be null")
    Integer jenisCaj;

    public LawatBayar18AAktiviti convertModelToEntity() {
        LawatBayar18AAktiviti entity = new LawatBayar18AAktiviti();

        entity.setJenisCaj(this.jenisCaj.toString());
        entity.setKatKerja(this.katKerja);
        entity.setKerjaStokKod(this.kerjaStokKod);
        entity.setNoSiriLB(this.noSiriLB);
        entity.setNoSiriTemujanji(this.noSiriTemujanji);
        entity.setStatus(this.status);
        entity.setStokItem(this.stokItem);

        return entity;
    }

}
