package com.map2u.ipower.model.form;

import com.map2u.ipower.ipowerdb.entity.LawatBayar18EChild;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LawatBayar18EChildModel {
    Long id;

    @ApiModelProperty(value = "Don't need for new form")
    String noSiriLE;

    String item;

    String catatan;

    public LawatBayar18EChild convertModelToEntity() {
        LawatBayar18EChild entity = new LawatBayar18EChild();

        entity.setCatatan(this.catatan);
        entity.setId(this.id);
        entity.setItem(this.item);
        entity.setNoSiriLE(this.noSiriLE);

        return entity;
    }

    public LawatBayar18EChild convertModelToEntity(String noSiriLE) {
        LawatBayar18EChild entity = new LawatBayar18EChild();

        entity.setCatatan(this.catatan);
        entity.setId(this.id);
        entity.setItem(this.item);
        entity.setNoSiriLE(noSiriLE);

        return entity;
    }

}
