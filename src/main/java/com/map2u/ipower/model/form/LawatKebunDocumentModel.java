package com.map2u.ipower.model.form;

import lombok.Data;

@Data
public class LawatKebunDocumentModel {

    private String id;

    private String noSiri;

    private String docType;

    private String docName;
}
