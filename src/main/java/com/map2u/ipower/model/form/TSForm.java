package com.map2u.ipower.model.form;

import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class TSForm {

    @NotNull(message = "PT code should not be null")
    @Size(max = 6,message = "ptCode should less than {max} characters ")
    String ptCode;

    @NotBlank(message = "tsMohonId should not be blank")
    String tsMohonId;

    @Size(max = 5,message = "statusLulus should less than {max} characters ")
    String statusLulus;

    String pegPemeriksa;

    @Size(max = 15,message = "pegLulus should less than {max} characters ")
    String pegLulus;

    @Size(max = 15,message = "pegKemaskini should less than {max} characters ")
    String pegKemaskini;

    @NotBlank(message = "noSiriTemujanji should not be blank")
    String noSiriTemujanji;

    @Size(max = 13,message = "noKpPpk should less than {max} characters ")
    String noKpPpk;

    @NotNull(message = "Jenis LK should not be null")
    @Size(max = 5,message = "jenisLK should less than {max} characters ")
    String jenisLK;

    String jawPemeriksa;

    String catatan;

    Long tkhLulus;

    Long tkhLawat;

    Long tkhKemaskini;

    @Digits(integer = 4,fraction = 0, message = "bilLawatan should less than {integer} digits")
    Integer bilLawatan;

}
