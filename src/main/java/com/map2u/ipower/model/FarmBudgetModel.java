package com.map2u.ipower.model;

import lombok.Data;

import java.util.List;

@Data
public class FarmBudgetModel {

    String ptCode;

    String noSiriFBM;

    String tujuanFBM;

    String noTSBRPK;

    Double jumlahKos;

    String status;

    String noPindaan;

    String pegInput;

    Long tkhInput;

    String pegLulus;

    Long tkhLulus;

    List<FarmBudgetChildModel> child;
}
