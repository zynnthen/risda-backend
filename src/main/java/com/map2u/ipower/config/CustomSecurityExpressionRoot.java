package com.map2u.ipower.config;

import com.map2u.ipower.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Slf4j
public class CustomSecurityExpressionRoot implements MethodSecurityExpressionOperations {

    //    @Value("${rest.api.auth.enable}")
    private boolean restApiAuthEnabled;
    private boolean restApiAuthACLEnabled;
    private UserRoleConfig userRoleConfig;

    protected final Authentication authentication;
    private AuthenticationTrustResolver trustResolver;
    private RoleHierarchy roleHierarchy;
    private Set<String> roles;
    private String defaultRolePrefix = "ROLE_";

    public final boolean permitAll = true;
    public final boolean denyAll = false;
    private PermissionEvaluator permissionEvaluator;
    public final String read = "read";
    public final String write = "write";
    public final String create = "create";
    public final String delete = "delete";
    public final String admin = "administration";
    private Object filterObject;
    private Object returnObject;


    public CustomSecurityExpressionRoot(Authentication authentication, boolean authEnabled, boolean aclEnabled) {
        if (authentication == null) {
            throw new IllegalArgumentException("Authentication object cannot be null");
        }

        this.restApiAuthEnabled = authEnabled;
        this.restApiAuthACLEnabled = aclEnabled;
        this.authentication = authentication;
    }

    public CustomSecurityExpressionRoot(Authentication authentication, boolean authEnabled, boolean aclEnabled, UserRoleConfig userRoleConfig) {
        if (authentication == null) {
            throw new IllegalArgumentException("Authentication object cannot be null");
        }

        this.restApiAuthEnabled = authEnabled;
        this.restApiAuthACLEnabled = aclEnabled;
        this.authentication = authentication;
        this.userRoleConfig = userRoleConfig;
    }

    @Override
    public void setFilterObject(Object o) {
        this.filterObject = o;
    }

    @Override
    public Object getFilterObject() {
        return this.filterObject;
    }

    @Override
    public void setReturnObject(Object o) {
        this.returnObject = o;
    }

    @Override
    public Object getReturnObject() {
        return this.returnObject;
    }

    @Override
    public Object getThis() {
        return this;
    }

    @Override
    public Authentication getAuthentication() {
        return this.authentication;
    }

    @Override
    public boolean hasAuthority(String s) {
        return hasAnyAuthority(s);
    }

    @Override
    public boolean hasAnyAuthority(String... strings) {
        if (restApiAuthEnabled && restApiAuthACLEnabled) {
            return hasAnyAuthorityName(null, strings);
        }
        return true;
    }

    @Override
    public boolean hasRole(String s) {
        return hasAnyRole(s);
    }

    @Override
    public boolean hasAnyRole(String... strings) {
        if (restApiAuthEnabled && restApiAuthACLEnabled) {
            return hasAnyAuthorityName(defaultRolePrefix, strings);
        }
        return true;
    }

    private boolean hasAnyAuthorityName(String prefix, String... roles) {
        final Set<String> roleSet = getAuthoritySet();
//        System.out.println(roleSet);
        for (final String role : roles) {
            final String defaultedRole = getRoleWithDefaultPrefix(prefix, role);
            if (roleSet.contains(defaultedRole)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean permitAll() {
        return true;
    }

    @Override
    public boolean denyAll() {
        return false;
    }

    @Override
    public boolean isAnonymous() {
        return trustResolver.isAnonymous(authentication);
    }

    @Override
    public boolean isAuthenticated() {
        return !isAnonymous();
    }

    @Override
    public boolean isRememberMe() {
        return trustResolver.isRememberMe(authentication);
    }

    @Override
    public boolean isFullyAuthenticated() {
        return !trustResolver.isAnonymous(authentication) && !trustResolver.isRememberMe(authentication);
    }

    public String getUserRole(String key) {
        if (this.userRoleConfig != null) {
            return this.userRoleConfig.getUserRoleCode(key).toString();
        }

        return null;
    }

    public Object getPrincipal() {
        return authentication.getPrincipal();
    }

    public void setTrustResolver(AuthenticationTrustResolver trustResolver) {
        this.trustResolver = trustResolver;
    }

    public void setRoleHierarchy(RoleHierarchy roleHierarchy) {
        this.roleHierarchy = roleHierarchy;
    }

    public void setDefaultRolePrefix(String defaultRolePrefix) {
        this.defaultRolePrefix = defaultRolePrefix;
    }

    private Set<String> getAuthoritySet() {
        if (roles == null) {
            roles = new HashSet<String>();
            Collection<? extends GrantedAuthority> userAuthorities = authentication.getAuthorities();
            if (roleHierarchy != null) {
                userAuthorities = roleHierarchy.getReachableGrantedAuthorities(userAuthorities);
            }

            roles = AuthorityUtils.authorityListToSet(userAuthorities);
        }

        return roles;
    }

    @Override
    public boolean hasPermission(Object o, Object o1) {
        if (restApiAuthEnabled && restApiAuthACLEnabled) {
            return permissionEvaluator.hasPermission(authentication, o, o1);
        }
        return true;
    }

    @Override
    public boolean hasPermission(Object o, String s, Object o1) {
        if (restApiAuthEnabled && restApiAuthACLEnabled) {
            return permissionEvaluator.hasPermission(authentication, (Serializable) o, s, o1);
        }
        return true;
    }

    public void setPermissionEvaluator(PermissionEvaluator permissionEvaluator) {
        this.permissionEvaluator = permissionEvaluator;
    }

    private static String getRoleWithDefaultPrefix(String defaultRolePrefix, String role) {
        if (role == null) {
            return role;
        }
        if ((defaultRolePrefix == null) || (defaultRolePrefix.length() == 0)) {
            return role;
        }
        if (role.startsWith(defaultRolePrefix)) {
            return role;
        }
        return defaultRolePrefix + role;
    }
}
