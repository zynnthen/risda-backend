package com.map2u.ipower.config.dozer;

public interface BeanConverter {


	/**
	 * Convert the source to the targetType described by the TypeDescriptor.
	 * @param source the source object to map (may be null)
	 * @param targetClass the class of the field we are converting to
	 * @return the converted object
	 */
	<T> T convert(Object source, Class<T> targetClass);

}