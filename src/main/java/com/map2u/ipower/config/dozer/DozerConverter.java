package com.map2u.ipower.config.dozer;

import java.util.Collections;
import java.util.Set;

import org.springframework.core.convert.converter.GenericConverter.ConvertiblePair;

import com.github.dozermapper.core.Mapper;

public class DozerConverter implements BeanConverter {

	private Mapper mapper;
	
	public Set<ConvertiblePair> getConvertibleTypes() {
		return Collections.singleton(new ConvertiblePair(Object.class, Object.class));
	}

	public <T> T convert(Object source, Class<T> targetClass) {
		return mapper.map(source, targetClass);
	}

	public Mapper getMapper() {
		return mapper;
	}

	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

}
