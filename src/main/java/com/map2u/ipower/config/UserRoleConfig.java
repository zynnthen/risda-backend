package com.map2u.ipower.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
@ConfigurationProperties(prefix = "user.role")
@Data
public class UserRoleConfig {

    private Map<String,Integer> code;

    public Integer getUserRoleCode(String key){
        return this.code.get(key);
    }

}
