package com.map2u.ipower.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "security.jwt")
@Data
public class JwtSetting {

    private boolean authEnable = false;

    private boolean aclEnable = false;

    private String secret;

    private String issuer;

    private String subject;

    private int tokenExpirationTime = 1800;

    private int refreshTokenExpTime = 2592000;

    private String userField = "user";

    private String typeField = "token_type";

    private String authHeader = "Authorization";
}
