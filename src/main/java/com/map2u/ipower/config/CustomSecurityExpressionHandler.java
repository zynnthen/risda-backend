package com.map2u.ipower.config;

import com.map2u.ipower.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;

@Configuration
@Slf4j
public class CustomSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {

    @Autowired
    AuthService authService;

    @Autowired
    UserRoleConfig userRoleConfig;

    private AuthenticationTrustResolver trustResolver =
            new AuthenticationTrustResolverImpl();

    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(Authentication authentication, MethodInvocation invocation) {
        final CustomSecurityExpressionRoot root = new CustomSecurityExpressionRoot(authentication, authService.isAuthEnabled(), authService.isAclEnabled(), userRoleConfig);
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        return root;
    }
}
