package com.map2u.ipower.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "document.type.id")
@Data
public class DocumentTypeID {

    long LK, LB, LE;
}
