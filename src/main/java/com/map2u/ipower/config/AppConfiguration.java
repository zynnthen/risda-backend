package com.map2u.ipower.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dozermapper.core.DozerBeanMapper;
import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.map2u.ipower.config.dozer.BeanConverter;
import com.map2u.ipower.config.dozer.DozerConverter;
import com.map2u.ipower.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfiguration {

    @Autowired
    AuthService authService;

//    @Bean
//    public FilterRegistrationBean unregister(WebAuthenticationFilter webAuthenticationFilter) {
//        FilterRegistrationBean registration = new FilterRegistrationBean<>(webAuthenticationFilter);
//        registration.setEnabled(false);
//        return registration;
//    }

    @Bean
    public WebAuthenticationFilter webAuthenticationFilter() {
        return new WebAuthenticationFilter(authService);
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        return mapper;
    }

    @Bean
    public MappingJackson2HttpMessageConverter mappingJacksonHttpMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(objectMapper());
        return converter;
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, mappingJacksonHttpMessageConverter());
        return restTemplate;
    }


    @Bean
    public BeanConverter beanConverter() {
        DozerConverter converter = new DozerConverter();
        converter.setMapper(dozerBeanMapper());
        return converter;
    }

    Mapper dozerBeanMapper() {
        return DozerBeanMapperBuilder.buildDefault();
    }

}
