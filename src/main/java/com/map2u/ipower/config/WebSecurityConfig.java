package com.map2u.ipower.config;

import com.map2u.ipower.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity(debug = false)
@Slf4j
public class WebSecurityConfig {

    @Configuration
    public static class ApiWebSecurityConfig extends WebSecurityConfigurerAdapter implements Ordered {

        private int order = 2;

        @Autowired
        WebAuthenticationFilter webAuthenticationFilter;

        @Autowired
        AuthService authService;

        @Override
        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers(
                    "/error",
                    "/static/**",
                    "/v2/api-docs",
                    "/v2/api-docs/**",
                    "/configuration/ui",
                    "/swagger-resources",
                    "/swagger-resources/**",
                    "/configuration/security",
                    "/swagger-ui.html",
                    "/webjars/**"
//                    ,"/**"
//                    "/user/auth/**"
            );
//            web.ignoring().antMatchers();
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            log.info("Is auth enabled {}", authService.isAuthEnabled());

            if (authService.isAuthEnabled()) {
                http.cors()
                        // allow x-frame-options from same origin
                        .and().csrf().disable()
                        .headers().frameOptions().sameOrigin().httpStrictTransportSecurity().disable()
                        .and()
                        .authorizeRequests()
                        .antMatchers(HttpMethod.OPTIONS, "/api/**").permitAll()
                        .antMatchers("/api/auth/**","/api/file/**").permitAll()
                        .anyRequest()
                        .authenticated()
                        .and()
                        .addFilterBefore(webAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);


            } else {
                http.cors()
                        // allow x-frame-options from same origin
                        .and().headers().frameOptions().sameOrigin().httpStrictTransportSecurity().disable()
                        .and().csrf().disable().authorizeRequests().anyRequest().permitAll();
            }
        }
    }
}
