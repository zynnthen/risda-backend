package com.map2u.ipower.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "smartmap")
@Data
public class SmartmapConfig {

    private String link;

}
