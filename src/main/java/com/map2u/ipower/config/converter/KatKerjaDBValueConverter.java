package com.map2u.ipower.config.converter;

import com.map2u.ipower.enums.KatKerja;

import javax.persistence.AttributeConverter;

public class KatKerjaDBValueConverter implements AttributeConverter<KatKerja, String> {

    @Override
    public String convertToDatabaseColumn(KatKerja katKerja) {
        if (katKerja != null) {
            return katKerja.toString();
        }

        return null;
    }

    @Override
    public KatKerja convertToEntityAttribute(String dbData) {
        if (dbData != null) {
            return KatKerja.findByValue(dbData);
        }

        return null;
    }
}
