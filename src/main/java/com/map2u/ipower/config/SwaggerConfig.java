package com.map2u.ipower.config;


import com.google.common.collect.Lists;
import com.map2u.ipower.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Autowired
    AuthService authService;

    @Value("${swagger.title:RISDA IPOWER}")
    String swaggerTitle;

    public static final String DEFAULT_INCLUDE_PATTERN = "/api/**";


    @Bean
    public Docket apiApi() {
//        ArrayList<Parameter> globalParams = new ArrayList<>();

        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .groupName("Shared Api")
//                .host(apiHost)
                .apiInfo(apiInfo())
//                .globalOperationParameters(globalParams)
                .select()
//                .apis(RequestHandlerSelectors.basePackage("com.tanand.shellsgweb.controller"))
                .paths(PathSelectors.ant(DEFAULT_INCLUDE_PATTERN))

                .build();

        if (authService.isAuthEnabled()) {
            docket
                    .securityContexts(Lists.newArrayList(securityContext()))
                    .securitySchemes(Lists.newArrayList(apiKey()))
                    .enableUrlTemplating(false);
        }

        return docket;
    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", authService.getAuthorizationHeader(), "header");
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
//                .forPaths(PathSelectors.regex("^.*\\/api\\/((?!auth\\b).)*$"))
                .forPaths(PathSelectors.regex("^.*\\/api\\/((?!auth|file\\b).*)\\/*$"))
                .build();
    }

    List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(
                new SecurityReference("JWT", authorizationScopes));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerTitle)
                .description("REST Api document")
                .termsOfServiceUrl("")
//                .version("0.1")
                .build();
    }


}
