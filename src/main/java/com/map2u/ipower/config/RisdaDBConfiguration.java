package com.map2u.ipower.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "risdaEntityManagerFactory",
        transactionManagerRef = "risdaTransactionManager",
        basePackages = {"com.map2u.ipower.risdadb.repository"}
)
public class RisdaDBConfiguration {
    @Bean(name = "risdaDataSource")
    @ConfigurationProperties(prefix = "spring.second.datasource")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "risdaEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    risdaEntityManagerFactory(
            EntityManagerFactoryBuilder builder,
            @Qualifier("risdaDataSource") DataSource dataSource
    ) {
        return builder
                .dataSource(dataSource)
                .packages("com.map2u.ipower.risdadb.entity")
                .persistenceUnit("risda")
                .properties(new HashMap<String, String>() {{
                    put("hibernate.hbm2ddl.auto", "none");
                }})
                .build();
    }

    @Primary
    @Bean(name = "risdaTransactionManager")
    public PlatformTransactionManager risdaTransactionManager(
            @Qualifier("risdaEntityManagerFactory") EntityManagerFactory
                    risdaEntityManagerFactory
    ) {
        return new JpaTransactionManager(risdaEntityManagerFactory);
    }
}
