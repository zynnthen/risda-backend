package com.map2u.ipower.config.interfaces;

import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Documented
@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface NullableOrBlank {

    boolean nullable = true;
    boolean blankable = true;

    String message() default "nullable is {nullable} and blankable is {blankable}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
