package com.map2u.ipower.config.constraints;

import com.map2u.ipower.config.interfaces.NullableOrBlank;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NullableOrBlankValidator implements ConstraintValidator<NullableOrBlank, String> {

    private boolean blankable;

    private boolean nullable;

    @Override
    public void initialize(NullableOrBlank constraintAnnotation) {
        this.blankable = constraintAnnotation.blankable;
        this.nullable = constraintAnnotation.nullable;
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {


        return false;
    }
}
